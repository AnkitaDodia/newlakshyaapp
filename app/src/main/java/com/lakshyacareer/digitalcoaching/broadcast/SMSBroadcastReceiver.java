package com.lakshyacareer.digitalcoaching.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;

public class SMSBroadcastReceiver extends BroadcastReceiver {

    OTPReceiveListener mOTPReceiveListener = null;

    public void InitOTPReceiveListener(OTPReceiveListener otpreceivelistener){

        this.mOTPReceiveListener = otpreceivelistener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            Bundle extras = intent.getExtras();
            Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);

            switch(status.getStatusCode()) {
                case CommonStatusCodes.SUCCESS:
                    // Get SMS message contents
                    String message = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
                    // Extract one-time code from the message and complete verification
                    // by sending the code back to your server.

                    // Extract one-time code from the message and complete verification
                    // by sending the code back to your server for SMS authenticity.
                    // But here we are just passing it to MainActivity

//                    <#> DMMT: Your OTP code is - 5439 JFtmKhTEVWQ

                    Log.e("VERIFY","Full Message : "+message);

                    if (mOTPReceiveListener != null) {
                        String[] temp = message.split("-");
                        String[] msg = temp[1].split(" ");
                        message = msg[0];


                        Log.e("VERIFY","message : "+message);
                        mOTPReceiveListener.onOTPReceived(message);
                    }

                    break;
                case CommonStatusCodes.TIMEOUT:
                    // Waiting for SMS timed out (5 minutes)
                    // Handle the error ...
                    mOTPReceiveListener.onOTPTimeOut();
                    break;
            }
        }
    }

    public interface OTPReceiveListener {
       public void onOTPReceived(String otp);
       public void onOTPTimeOut();
    }
}