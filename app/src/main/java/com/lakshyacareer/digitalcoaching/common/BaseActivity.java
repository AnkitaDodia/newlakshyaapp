package com.lakshyacareer.digitalcoaching.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.models.CurrentAffairsData;
import com.lakshyacareer.digitalcoaching.models.FAQData;
import com.lakshyacareer.digitalcoaching.models.LatestNewsData;
import com.lakshyacareer.digitalcoaching.models.LiveVideo;
import com.lakshyacareer.digitalcoaching.models.LiveVideoData;
import com.lakshyacareer.digitalcoaching.models.UserData;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;
import com.lakshyacareer.digitalcoaching.utility.TypeFaces;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class BaseActivity extends AppCompatActivity {

    ProgressDialog mProgressDialog;

    //if this variable has 0 value then nothing to change if it's fresh install then 1
    public static int isFreshInstall = 1;

//    public static String[] states = {"આંધ્રપ્રદેશ", "અરુણાચલ પ્રદેશ", "આસામ", "બિહાર", "ગોવા", "ગુજરાત", "હરિયાણા",
//            "હિમાચલ પ્રદેશ", "જમ્મુ અને કાશ્મીર", "કર્ણાટક", "કેરળ", "મધ્યપ્રદેશ", "મહારાષ્ટ્ર", "મણિપુર", "મેઘાલય", "મિઝોરમ", "નાગાલેન્ડ",
//            "ઓરિસ્સા", "પંજાબ", "રાજસ્થાન", "સિક્કીમ", "તમિલનાડુ", "ત્રિપુરા", "ઉત્તર પ્રદેશ", "પશ્ચિમ બંગાળ",
//            "છત્તીસગઢ", "ઉત્તરાખંડ", "ઝારખંડ", "તેલંગણા"};

    public static String[] states = {"Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Goa", "Gujarat", "Haryana", "Himachal Pradesh",
            "Jammu & Kashmir", "Karnataka", "Kerala", "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland",
            "Orissa", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Tripura", "Uttar Pradesh", "West Bengal", "Chhattisgarh", "Uttarakhand",
            "Jharkhand", "Telangana"};

    public static int FROM_LEGAL = 0;
    public static String mText = null;
    public static int LATEST_NEWS_POSITION= 0;
    public static int CURRENT_AFFAIRS_POSITION= 0;


    public static String PLAYLIST_ID ;
    public static String VIDEO_ID ;


    public static String SUBJECT_ID = null;
    public static String PACKAGE_ID = null;

    public static int WRONG_ANSWERS = 0;
    public static int RIGHT_ANSWERS = 0;
    public static int NOT_ATTEMPTED_ANSWERS = 0;
    public static int TOTAL_QUESTIONS =  0;
    public static String SELECTED_ANSWER = null;

    public static int isFromSubject = 0;

    public static JSONArray questions = new JSONArray();

    //public static ArrayList<FAQData> mFAQitems = new ArrayList<>();
    public static ArrayList<LatestNewsData> mLatestNewsList = new ArrayList<>();
    public static ArrayList<CurrentAffairsData> mCurrentAffairsDataList = new ArrayList<>();

    public static boolean CheckInternet(Context mContext) {

        return new InternetStatus().isInternetOn(mContext);
    }

    // Code for get and set login
    public void setLogin(int i) {
        SharedPreferences sp = getSharedPreferences("LOGIN", MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putInt("Login", i);
        spe.apply();
    }

    public int getLogin() {
        SharedPreferences sp = getSharedPreferences("LOGIN", MODE_PRIVATE);
        int i = sp.getInt("Login", 0);
        return i;
    }

    // Code for get and set login
    public Typeface getGujaratiFonts(Context mContext) {
        Typeface font = TypeFaces.getTypeFace(mContext, "SHRUTI.TTF");
        return font;
    }

    public void overrideFonts(final View v, Context ctx) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(child, ctx);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(getGujaratiFonts(ctx));
            } else if (v instanceof Button) {
                ((Button) v).setTypeface(getGujaratiFonts(ctx));
            } else if (v instanceof EditText) {
                ((EditText) v).setTypeface(getGujaratiFonts(ctx));
            } else if (v instanceof TextInputLayout) {
                ((TextInputLayout) v).setTypeface(getGujaratiFonts(ctx));
            }
        } catch (Exception e) {
        }
    }

    public void showWaitIndicator(boolean state) {
        if (state) {
            showProgressDialog();
        } else {
            dismissProgressDialog();
        }
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            initProgressDialog();
        }
        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
//            mProgressDialog.setMessage("LOADING...");
            mProgressDialog.show();
            ProgressBar progressbar = (ProgressBar) mProgressDialog.findViewById(android.R.id.progress);
            progressbar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#FFDF284D"), android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }

    private void initProgressDialog() {
        Log.d("ProgressBar", "initProgressDialog()");
        mProgressDialog = new ProgressDialog(this, R.style.TransparentProgressDialog);
//        mProgressDialog.setMessage(LOADING);
        mProgressDialog.setCancelable(false);
//        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    public void dismissProgressDialog() {
        Log.d("ProgressBar", "dismissProgressDialog()");
        try {
            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public boolean isAlphaNumeric(String s) {
        boolean isAlphaNumericValue = false;
        boolean alpha = false;
        boolean numeric = false;
        boolean accepted = true;
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (Character.isDigit(c)) {
                numeric = true;
            } else if (Character.isLetter(c)) {
                alpha = true;
            } else {
                accepted = false;
                break;
            }
        }

        if (accepted && alpha && numeric) {
            isAlphaNumericValue = true;
        } else {
            isAlphaNumericValue = false;
        }

        return isAlphaNumericValue;
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public String getIMEINumber() {
        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String IMEINumber = mngr.getDeviceId();
//        String IMEINumber = "0000000000000000";
        return IMEINumber;
    }

    public void saveLoginDetail(String id, String pwd) {
        SharedPreferences sp = getSharedPreferences("LOGIN_Detail", MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("ID", id);
        spe.putString("PWD", pwd);
        spe.commit();
    }

    public String getLoginId() {
        SharedPreferences sp = getSharedPreferences("LOGIN_Detail", MODE_PRIVATE);
        String id = sp.getString("ID", "");
        return id;
    }

    public String getLoginPwd() {
        SharedPreferences sp = getSharedPreferences("LOGIN_Detail", MODE_PRIVATE);
        String pwd = sp.getString("PWD", "");
        return pwd;
    }

    public void saveRememberMe(boolean flag) {
        SharedPreferences sp = getSharedPreferences("REMEMBER", MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putBoolean("REMEMBER_ME", flag);
        spe.commit();
    }

    public void saveLoginPwd(String pwd) {
        SharedPreferences sp = getSharedPreferences("LOGIN_Detail", MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("PWD", pwd);
        spe.commit();
    }

    public boolean getRemember() {
        SharedPreferences sp = getSharedPreferences("REMEMBER", MODE_PRIVATE);
        boolean flag = sp.getBoolean("REMEMBER_ME", false);
        return flag;
    }

    public void updateUser(UserData user) {
        SettingsPreferences.storeConsumer(this, user);
    }

    public UserData getUserDetails() {
        return SettingsPreferences.getConsumer(this);
    }
}
