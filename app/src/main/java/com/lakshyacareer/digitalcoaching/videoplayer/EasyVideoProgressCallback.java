package com.lakshyacareer.digitalcoaching.videoplayer;

/** @author Aidan Follestad (afollestad) */
public interface EasyVideoProgressCallback {

  void onVideoProgressUpdate(int position, int duration);
}
