package com.lakshyacareer.digitalcoaching.restinterface;

import com.lakshyacareer.digitalcoaching.models.AboutUs;
import com.lakshyacareer.digitalcoaching.models.Answers;
import com.lakshyacareer.digitalcoaching.models.Banner;
import com.lakshyacareer.digitalcoaching.models.ChangePassword;
import com.lakshyacareer.digitalcoaching.models.ContactUs;
import com.lakshyacareer.digitalcoaching.models.CurrentAffairs;
import com.lakshyacareer.digitalcoaching.models.FAQ;
import com.lakshyacareer.digitalcoaching.models.Favourite;
import com.lakshyacareer.digitalcoaching.models.ForgotPassword;
import com.lakshyacareer.digitalcoaching.models.LatestNews;
import com.lakshyacareer.digitalcoaching.models.LiveVideo;
import com.lakshyacareer.digitalcoaching.models.MySubscription;
import com.lakshyacareer.digitalcoaching.models.Packages;
import com.lakshyacareer.digitalcoaching.models.PaymentInfo;
import com.lakshyacareer.digitalcoaching.models.Questions;
import com.lakshyacareer.digitalcoaching.models.ResendOTP;
import com.lakshyacareer.digitalcoaching.models.Sample;
import com.lakshyacareer.digitalcoaching.models.StatusMessages;
import com.lakshyacareer.digitalcoaching.models.Subjects;
import com.lakshyacareer.digitalcoaching.models.Topics;
import com.lakshyacareer.digitalcoaching.models.User;
import com.lakshyacareer.digitalcoaching.models.YTPlaylist;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


/**
 * Created by codfidea on 10/12/2016.
 */

public interface RestInterface {

    public static String API_BASE_URL = "http://api.lakshyacareer.in";

    public static String GOOGLE_API_BASE_URL = "https://www.googleapis.com/youtube/v3/";
//    https://www.googleapis.com/youtube/v3/playlists?part=snippet,contentDetails&channelId=UCqUDtr7LxA_g2loE0GvMD4A&key=AIzaSyCZZ5Vu9X-ahW7k8Decyl3d9j00HeJr5zs&maxResults=50


    String GET_PLAYLIST = "playlists?";
    String GET_PLAYLIST_VIDEO = "playlistItems?";

    String SAMPLE_TUTORIALS = "/topics/samplevideolist";

    String LOGIN = "/users/login";
    String LOGIN_OTP = "/users/loginusingotp";
    String REGISTER = "/users/useradd";
    String EDIT_PROFILE = "/users/useredit";
    String MATCH_OTP = "/users/matchotp";
    String RESEND_OTP = "users/resendotp";

    String FORGOT_PASSWORD = "/users/forgotpasswordotp";
    String CHANGE_PASSWORD = "/users/changepassword";
    String FORGOT_MATCH_OTP = "/users/forgotpasswordnew";

    String PACKAGES_LIST = "/packages/packagelist";
    String SUBJECTS_LIST = "/packages/PackageSubject";
    String TOPICS_LIST = "/topics/topicWithMaterials";

    String LATEST_NEWS = "/news/newslist";
    String CURRENT_AFFAIRS = "/currentAffairs/currentAffairList";
    String LIVE_VIDEO = "/learningVideo/videoList";
    String QUESTION_LIST = "/questions/questionsList";
    String ANSWERS_LIST = "/answers/answersList";

    String BANNER_IMAGES = "banners/bannerList";

    String ABOUTUS = "/home/aboutus";
    String FAQ_LIST = "/users/faqlist";
    String CONTACT_US = "/users/contactusadd";

    String PAYMENTINFO = "/packages/subscribePackage";
    String MY_SUBSCRIPTION = "/packages/PackageNameByUser";

    String ADD_FAVOURITE = "/materials/addMaterialToFavourite";
    String REMOVE_FAVOURITE = "/materials/removeMaterialFromFavourite";
    String FAVOURITE_LIST = "/materials/materialFavouriteList";

    String ADD_DOWNLOADS = "/materials/addMaterialToDownload";
    String REMOVE_DOWNLOADS = "/materials/removeMaterialFromDownload";

    //LogIn
    @FormUrlEncoded
    @POST(LOGIN)
    Call<User> sendLoginRequest(@Field("email") String email, @Field("password") String password,
                                @Field("isFreshInstall") int isFreshInstall, @Field("IMEI_number") String IMEI_number);

    //LogInWithOTP
    @FormUrlEncoded
    @POST(LOGIN_OTP)
    Call<User> sendLoginWithOTPRequest(@Field("mobile_number") String mobile_number, @Field("IMEI_number") String IMEI_number);

    @FormUrlEncoded
    @POST(MATCH_OTP)
    Call<User> sendMatchOTPRequest(@Field("id_user") String id_user, @Field("otp") String otp, @Field("islogin") String islogin);



    @FormUrlEncoded
    @POST(FORGOT_MATCH_OTP)
    Call<StatusMessages> sendForgotMatchOTPRequest(@Field("mobile_number") String mobile_number, @Field("otp") String otp);

    @FormUrlEncoded
    @POST(RESEND_OTP)
    Call<ResendOTP> sendResendOTPRequest(@Field("mobile_number") String mobile_number, @Field("type") String type);

    //Registration
    @FormUrlEncoded
    @POST(REGISTER)
    Call<User> sendRegisterRequest(@Field("first_name") String first_name, @Field("last_name") String last_name,
                                   @Field("dob") String dtae_of_birth, @Field("email") String email, @Field("mobile_number") String mobile_number,
                                   @Field("password") String password, @Field("confirm_password") String confirm_password,
                                   @Field("state") String state, @Field("city") String city,
                                   @Field("IMEI_number") String IMEI_number);

    @FormUrlEncoded
    @POST(EDIT_PROFILE)
    Call<User> sendEditProfileRequest(@Field("id_user") String user_id,
                                      @Field("first_name") String first_name, @Field("last_name") String last_name,
                                      @Field("dob") String dtae_of_birth,
                                      @Field("email") String email, @Field("mobile_number") String mobile_number,
                                      @Field("password") String password, @Field("confirm_password") String confirm_password,
                                      @Field("state") String state, @Field("city") String city,
                                      @Field("payment_mode") String payment_mode, @Field("IMEI_number") String IMEI_number);

    //Forgot Password
    @FormUrlEncoded
    @POST(FORGOT_PASSWORD)
    Call<ForgotPassword> sendForgotPasswordRequest(@Field("mobile_number") String mobile_number);



    @FormUrlEncoded
    @POST(CHANGE_PASSWORD)
    Call<ChangePassword> sendChangePasswordRequest(@Field("id_user") String user_id,
                                                   @Field("password") String password,
                                                   @Field("new_password") String new_password,
                                                   @Field("confirm_password") String confirm_password);

    @GET(PACKAGES_LIST)
    Call<Packages> sendPackagesListRequest();

    @FormUrlEncoded
    @POST(SUBJECTS_LIST)
    Call<Subjects> sendSubjectsListRequest(@Field("id_package") String id_package, @Field("id_user") String id_user);

    @FormUrlEncoded
    @POST(TOPICS_LIST)
    Call<Topics> sendTopicsRequest(@Field("id_subject") String id_subject, @Field("id_user") String id_user);

    @FormUrlEncoded
    @POST(ADD_FAVOURITE)
    Call<StatusMessages> sendAddToFavouriteRequest(@Field("id_user") String id_user, @Field("id_material") String id_material);

    @FormUrlEncoded
    @POST(REMOVE_FAVOURITE)
    Call<StatusMessages> sendRemoveFavouriteRequest(@Field("id_user") String id_user, @Field("id_material") String id_material);

    @FormUrlEncoded
    @POST(FAVOURITE_LIST)
    Call<Favourite> sendFavouriteListRequest(@Field("id_user") String id_user);

    @FormUrlEncoded
    @POST(ADD_DOWNLOADS)
    Call<StatusMessages> sendAddToDownloadsRequest(@Field("id_user") String id_user, @Field("id_material") String id_material);

    @FormUrlEncoded
    @POST(REMOVE_DOWNLOADS)
    Call<StatusMessages> sendRemoveDownloadsRequest(@Field("id_user") String id_user, @Field("id_material") String id_material);

    @FormUrlEncoded
    @POST(MY_SUBSCRIPTION)
    Call<MySubscription> sendMySubscriptionRequest(@Field("id_user") String id_user);


    @GET(LATEST_NEWS)
    Call<LatestNews> sendLatestNewsRequest();

    @GET(CURRENT_AFFAIRS)
    Call<CurrentAffairs> sendCurrentAffairsRequest();


    @GET(QUESTION_LIST)
    Call<Questions> sendQuestionsRequest();

    @FormUrlEncoded
    @POST(ANSWERS_LIST)
    Call<Answers> sendAnswersRequest(@Field("id_user") String user_id,
                                     @Field("answers") String answers,
                                     @Field("right_answers") int right_answers,
                                     @Field("wrong_answers") int wrong_answers,
                                     @Field("not_attempted_answers") int not_attempted_answers,
                                     @Field("mResult") String mResult);

    @GET(ABOUTUS)
    Call<AboutUs> sendAboutUsRequest();

    @GET(FAQ_LIST)
    Call<FAQ> sendFAQRequest();

    //ContactUs
    @FormUrlEncoded
    @POST(CONTACT_US)
    Call<ContactUs> sendContactUsRequest(@Field("name") String name,
                                         @Field("email") String email,
                                         @Field("city") String city,
                                         @Field("state") String state,
                                         @Field("message") String message,
                                         @Field("mobile_number") String mobile_number);

    @GET(SAMPLE_TUTORIALS)
    Call<Sample> getSampleTutorialsRequest();

    @GET(LIVE_VIDEO)
    Call<LiveVideo> getLiveVideoRequest();

    @GET(BANNER_IMAGES)
    Call<Banner> getBannerRequest();

    @FormUrlEncoded
    @POST(PAYMENTINFO)
    Call<PaymentInfo> sendPaymentInfoRequest(@Field("id_user") String id_user,
                                             @Field("payment_status") String payment_status,
                                             @Field("payment_date") String payment_date,
                                             @Field("payment_mode") String payment_mode,
                                             @Field("mAmount") String payed_amount,
                                             @Field("mPackageId") String package_id,
                                             @Field("mDuration") String package_duration);

    @GET(GET_PLAYLIST)
    Call<YTPlaylist> GetYTPlayListRequest(@Query("key") String key,
                                           @Query("channelId") String channelId,
                                           @Query("part") String part,
                                           @Query("maxResults") String maxResults);

    @GET(GET_PLAYLIST_VIDEO)
    Call<YTPlaylist> GetYTPlayListVideosRequest(@Query("key") String key,
                                                @Query("playlistId") String playlistId,
                                                @Query("part") String part,
                                                @Query("maxResults") String maxResults);

}
