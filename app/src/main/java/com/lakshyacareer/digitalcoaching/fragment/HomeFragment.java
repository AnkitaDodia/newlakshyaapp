package com.lakshyacareer.digitalcoaching.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.activity.CurrentAffairActivity;
import com.lakshyacareer.digitalcoaching.activity.DashBoardActivity;
import com.lakshyacareer.digitalcoaching.activity.LatestNewsActivity;
import com.lakshyacareer.digitalcoaching.activity.LiveVideoActivity;
import com.lakshyacareer.digitalcoaching.activity.MaterialActivity;
import com.lakshyacareer.digitalcoaching.activity.PracticeActivity;
import com.lakshyacareer.digitalcoaching.adapters.BannerAdapter;
import com.lakshyacareer.digitalcoaching.models.Banner;
import com.lakshyacareer.digitalcoaching.models.BannerData;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.lakshyacareer.digitalcoaching.common.BaseActivity.CheckInternet;


public class HomeFragment extends Fragment {

    DashBoardActivity mContext;
    CardView card_latest_news, card_current_affair, card_materials, card_practice, card_live_video;
    private ViewPager vp_banner;
    public ArrayList<BannerData> mBannerData = new ArrayList<>();

    private LinearLayout ll_parent_home, intro_pager_dots;
    private int dotscount;


    private ImageView[] ivArrayDotsPager;
    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 100;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 4000; // time in milliseconds between successive task executions.

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (DashBoardActivity) getActivity();

        InitViews(view);
        SetClickListners();

        if (CheckInternet(mContext)) {
            getBannerRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void InitViews(View view){

        vp_banner = view.findViewById(R.id.vp_banner);

        ll_parent_home = view.findViewById(R.id.ll_parent_home);

        intro_pager_dots = view.findViewById(R.id.intro_pager_dots);

        card_latest_news = view.findViewById(R.id.card_latest_news);
        card_current_affair = view.findViewById(R.id.card_current_affair);
        card_materials = view.findViewById(R.id.card_materials);
        card_practice = view.findViewById(R.id.card_practice);
        card_live_video = view.findViewById(R.id.card_live_video);

        mContext.overrideFonts(ll_parent_home, mContext);
    }

    private void SetClickListners() {

        card_latest_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, LatestNewsActivity.class);
                startActivity(i);

            }
        });

        card_current_affair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, CurrentAffairActivity.class);
                startActivity(i);
            }
        });

        card_materials.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, MaterialActivity.class);
                startActivity(i);
            }
        });

        card_practice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, PracticeActivity.class);
                startActivity(i);

            }
        });

        card_live_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, LiveVideoActivity.class);
                startActivity(i);

            }
        });
    }

    private void getBannerRequest() {

        mContext.showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<Banner> call = service.getBannerRequest();

        call.enqueue(new Callback<Banner>() {
            @Override
            public void onResponse(Call<Banner> call, Response<Banner> response) {

                mContext.showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e("BANNER", "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));

                        if(response.body().getStatus() == 1){

                            mBannerData = response.body().getData() ;
                            InitializeViewPager();

                        }else {

                        }
                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    mContext.showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Banner> call, Throwable t) {
                mContext.showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void InitializeViewPager() {

        BannerAdapter mBannerAdapter = new BannerAdapter(mContext, mBannerData);
        vp_banner.setAdapter(mBannerAdapter);

        dotscount = mBannerAdapter.getCount();
        ivArrayDotsPager = new ImageView[dotscount];

        for (int i = 0; i < dotscount; i++) {

            ivArrayDotsPager[i] = new ImageView(mContext);
            ivArrayDotsPager[i].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.page_indicator_unselected));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            intro_pager_dots.addView(ivArrayDotsPager[i], params);

        }

        ivArrayDotsPager[0].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.page_indicator_selected));

        vp_banner.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < dotscount; i++) {
                    ivArrayDotsPager[i].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.page_indicator_unselected));
                }

                ivArrayDotsPager[position].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.page_indicator_selected));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        /*After setting the adapter use the timer */
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == dotscount) {
                    currentPage = 0;
                }
                vp_banner.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer .schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
    }

}
