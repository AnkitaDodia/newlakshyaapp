package com.lakshyacareer.digitalcoaching.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.activity.DashBoardActivity;
import com.lakshyacareer.digitalcoaching.activity.VideoPlayerActivity;
import com.lakshyacareer.digitalcoaching.models.StatusMessages;
import com.lakshyacareer.digitalcoaching.models.VideosDownloads;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.DatabaseHelper;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by abc on 29-10-2017.
 */

public class VideoDownloadedFragment extends Fragment {

    DashBoardActivity mContext;

    TextView txt_empty_downloads;
    RecyclerView rv_downloads;

    private ArrayList<VideosDownloads> mVideosDownloads = new ArrayList<>();


    DatabaseHelper mDbHelper, mDBAdapters;
    static int version_val = 2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_downloads, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (DashBoardActivity) getActivity();


        initView(view);
        setTypeFace();

        try {
            mDbHelper = new DatabaseHelper(mContext, "LakshyCareer.sqlite", null,version_val);
            mDBAdapters = DatabaseHelper.getDBAdapterInstance(mContext);
            mDBAdapters.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }

        setListData();


    }

    private void initView(View view) {

        txt_empty_downloads = view.findViewById(R.id.txt_empty_downloads);
        rv_downloads = view.findViewById(R.id.rv_downloads);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rv_downloads.setLayoutManager(mLayoutManager);
        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.rv_divider));
        rv_downloads.addItemDecoration(itemDecorator);

    }

    private void setTypeFace()
    {
        txt_empty_downloads.setTypeface(mContext.getGujaratiFonts(mContext));
    }


    private void setListData()
    {
        try {
            mDBAdapters.openDataBase();
            mVideosDownloads = mDBAdapters.GetVideos();
            mDBAdapters.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("SIZE",""+mVideosDownloads.size());

        if(mVideosDownloads.size() > 0)
        {
            txt_empty_downloads.setVisibility(View.GONE);
            rv_downloads.setVisibility(View.VISIBLE);


            DownloadListAdapter mDownloadListAdapter= new DownloadListAdapter(mContext, mVideosDownloads);
            rv_downloads.setAdapter(mDownloadListAdapter);
        }
        else
        {
            txt_empty_downloads.setVisibility(View.VISIBLE);
            rv_downloads.setVisibility(View.GONE);
            txt_empty_downloads.setText("You haven't downloaded anything!!");
        }
    }

    public class DownloadListAdapter extends RecyclerView.Adapter<DownloadListAdapter.MyDownloadHolder>
    {
        ArrayList<VideosDownloads> mVideosDownloadsList = new ArrayList<>();

        DashBoardActivity mContext;

        DownloadListAdapter.MyDownloadHolder holder;

        public DownloadListAdapter(DashBoardActivity context, ArrayList<VideosDownloads> mVideosDownloads)
        {
            mContext = context;
            mVideosDownloadsList = mVideosDownloads;
        }

        @Override
        public DownloadListAdapter.MyDownloadHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_downloads, null);

            return new DownloadListAdapter.MyDownloadHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final DownloadListAdapter.MyDownloadHolder holder, final int position) {

            final VideosDownloads mVideosDownloads = mVideosDownloadsList.get(position);

            LinearLayout.LayoutParams params = new
                    LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            holder.ll_parent_downloads.setLayoutParams(params);


            holder.txt_download_number.setTypeface(mContext.getGujaratiFonts(mContext));
            holder.txt_download_name.setTypeface(mContext.getGujaratiFonts(mContext));
            holder.txt_download_duration.setTypeface(mContext.getGujaratiFonts(mContext));
            holder.txt_download_size.setTypeface(mContext.getGujaratiFonts(mContext));

            int pos = position + 1;
            holder.txt_download_number.setText(String.valueOf(pos));
            holder.txt_download_name.setText(mVideosDownloads.getSubjectName());
            holder.txt_download_duration.setText("duration : "+mVideosDownloads.getVideoDuraion());
            holder.txt_download_size.setText("size : "+mVideosDownloads.getVideoSize());

            holder.lay_download_open.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent videoIntent = new Intent(mContext, VideoPlayerActivity.class);
                    videoIntent.putExtra("video_path",mVideosDownloads.getVideoPath());
                    mContext.startActivity(videoIntent);
                }
            });

            holder.lay_download_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                    builder.setTitle("Delete")
                            .setMessage("Are you sure you want to delete this video?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    try {
                                        mDBAdapters.openDataBase();
                                        mDBAdapters.DeleteVideo(mVideosDownloadsList.get(position).getMaterialId());
                                        mDBAdapters.close();

                                        File fdelete = new File(mVideosDownloadsList.get(position).getVideoPath());
                                        if (fdelete.exists()) {
                                            if (fdelete.delete()) {
                                                System.out.println("file Deleted :" + mVideosDownloadsList.get(position).getVideoPath());
                                            } else {
                                                System.out.println("file not Deleted :" + mVideosDownloadsList.get(position).getVideoPath());
                                            }
                                        }
                                        notifyDataSetChanged();
                                        setListData();
                                        sendRemoveDownloadsRequest(mVideosDownloadsList.get(position).getMaterialId());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    dialog.dismiss();

                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return mVideosDownloadsList.size();
        }

        public class MyDownloadHolder extends RecyclerView.ViewHolder {

            TextView txt_download_number, txt_download_name, txt_download_duration, txt_download_size;
            ImageView img_download_open, img_download_delete;
            LinearLayout ll_parent_downloads, lay_download_open, lay_download_delete;

            public MyDownloadHolder(View convertView)
            {
                super(convertView);

                ll_parent_downloads = (LinearLayout) convertView.findViewById(R.id.ll_parent_downloads);
                txt_download_number = (TextView) convertView.findViewById(R.id.txt_download_number);
                txt_download_name = (TextView) convertView.findViewById(R.id.txt_download_name);
                txt_download_duration = (TextView) convertView.findViewById(R.id.txt_download_duration);
                txt_download_size = (TextView) convertView.findViewById(R.id.txt_download_size);

                img_download_open = (ImageView) convertView.findViewById(R.id.img_download_open);
                lay_download_open = (LinearLayout) convertView.findViewById(R.id.lay_download_open);
                img_download_delete = (ImageView) convertView.findViewById(R.id.img_download_delete);
                lay_download_delete = (LinearLayout) convertView.findViewById(R.id.lay_download_delete);

            }
        }

    }

    private void sendRemoveDownloadsRequest(String id_material)
    {
        try {
            mContext.showWaitIndicator(true);

            String id_user = SettingsPreferences.getConsumer(mContext).getIdUser();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            Call<StatusMessages> call = service.sendRemoveDownloadsRequest(id_user, id_material);

            call.enqueue(new Callback<StatusMessages>() {
                @Override
                public void onResponse(Call<StatusMessages> call, Response<StatusMessages> response) {

                    mContext.showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {

                            if(response.body().getStatus() == 1){

                                setListData();
                                Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            }else {

                                Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            }

                        }
                    } catch (Exception e) {
                        mContext.showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<StatusMessages> call, Throwable t) {
                    mContext.showWaitIndicator(false);
                    Log.d("OnFailure","onFailure"+t.toString());
                    Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


}
