package com.lakshyacareer.digitalcoaching.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.activity.DashBoardActivity;
import com.lakshyacareer.digitalcoaching.adapters.MyDownloadsPagerAdapter;
import com.lakshyacareer.digitalcoaching.ui.SlidingTabLayout;

public class MyDownloadsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    DashBoardActivity mContext;

    ViewPager vp_downloads;
    SlidingTabLayout svp_downloads;
    MyDownloadsPagerAdapter adapter;
    CharSequence Titles[]={"Videos","Materials"};
    int Numboftabs =2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mydownloads, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (DashBoardActivity) getActivity();

        setHasOptionsMenu(true);

        inItView(view);
    }


    private void inItView(View view) {

        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new MyDownloadsPagerAdapter(mContext.getSupportFragmentManager(), Titles, Numboftabs);

        // Assigning ViewPager View and setting the adapter
        vp_downloads = view.findViewById(R.id.vp_my_downloads);
        vp_downloads.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        svp_downloads = view.findViewById(R.id.svp_my_downloads);
        svp_downloads.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        svp_downloads.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        svp_downloads.setViewPager(vp_downloads);
    }


    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();


    }
}
