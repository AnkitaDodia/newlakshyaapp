package com.lakshyacareer.digitalcoaching.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.activity.ChangePasswordActivity;
import com.lakshyacareer.digitalcoaching.activity.DashBoardActivity;
import com.lakshyacareer.digitalcoaching.activity.EditProfileActivity;
import com.lakshyacareer.digitalcoaching.activity.MySubscriptionActivity;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;

public class MyAccountFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    DashBoardActivity mContext;

    LinearLayout lay_edit_profile, lay_change_password, lay_mysubscription;
    TextView txt_account_email, txt_account_phone, txt_account_type, txt_edit_profile, txt_change_password, txt_my_subscription;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_account, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (DashBoardActivity) getActivity();

        setHasOptionsMenu(true);

//        mContext.toolbar.setNavigationIcon(R.drawable.ic_back);
//        mContext.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().finish();
//                mContext.startActivity(new Intent(mContext,NavigationDrawerActivity.class));
//            }
//        });

        inItView(view);
        setTypeFace();
        setClickListener();
    }


    private void inItView(View view) {

        lay_edit_profile = (LinearLayout) view.findViewById(R.id.lay_edit_profile);
        lay_change_password = (LinearLayout) view.findViewById(R.id.lay_change_password);
        lay_mysubscription = (LinearLayout) view.findViewById(R.id.lay_mysubscription);

        txt_account_email = (TextView) view.findViewById(R.id.txt_account_email);
        txt_account_phone = (TextView) view.findViewById(R.id.txt_account_phone);
        txt_account_type = (TextView) view.findViewById(R.id.txt_account_type);
        txt_edit_profile = (TextView) view.findViewById(R.id.txt_edit_profile);
        txt_change_password = (TextView) view.findViewById(R.id.txt_change_password);
        txt_my_subscription = (TextView) view.findViewById(R.id.txt_my_subscription);

    }

    private void setTypeFace()
    {
        txt_account_email.setTypeface(mContext.getGujaratiFonts(mContext));
        txt_account_phone.setTypeface(mContext.getGujaratiFonts(mContext));
        txt_account_type.setTypeface(mContext.getGujaratiFonts(mContext));
        txt_edit_profile.setTypeface(mContext.getGujaratiFonts(mContext));
        txt_change_password.setTypeface(mContext.getGujaratiFonts(mContext));
        txt_my_subscription.setTypeface(mContext.getGujaratiFonts(mContext));


        txt_account_email.setText(SettingsPreferences.getConsumer(mContext).getEmail());
        txt_account_phone.setText(SettingsPreferences.getConsumer(mContext).getMobileNumber());
//        txt_account_type.setText(SettingsPreferences.getConsumer(mContext).getEmail());
    }

    private void setClickListener() {

        lay_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext, EditProfileActivity.class));
            }
        });

        lay_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext, ChangePasswordActivity.class));
            }
        });

        lay_mysubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext, MySubscriptionActivity.class));
            }
        });

    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

//        getView().setFocusableInTouchMode(true);
//        getView().requestFocus();
//        getView().setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK)
//                {
//                    getActivity().finish();
//                    mContext.startActivity(new Intent(mContext,DashBoardActivity.class));
//                    return true;
//                }
//                return false;
//            }
//        });
//
//        txt_account_email.setText(SettingsPreferences.getConsumer(mContext).getEmail());
//        txt_account_phone.setText(SettingsPreferences.getConsumer(mContext).getMobileNumber());
    }
}
