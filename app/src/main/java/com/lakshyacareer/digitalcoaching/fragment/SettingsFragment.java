package com.lakshyacareer.digitalcoaching.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.activity.AboutUsActivity;
import com.lakshyacareer.digitalcoaching.activity.ContactUsActivity;
import com.lakshyacareer.digitalcoaching.activity.DashBoardActivity;
import com.lakshyacareer.digitalcoaching.activity.FAQActivity;
import com.lakshyacareer.digitalcoaching.activity.LegalActivity;
import com.lakshyacareer.digitalcoaching.activity.LoginActivity;
import com.lakshyacareer.digitalcoaching.activity.SubscriptionProcessActivity;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;

public class SettingsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match

    DashBoardActivity mContext;

    LinearLayout lay_faq, lay_aboutus, lay_contactus, lay_subscription_process, lay_legal, lay_logout;

    TextView txt_faq,txt_aboutus,txt_contact_us,txt_subscription_process,txt_legal, txt_logout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (DashBoardActivity) getActivity();

        setHasOptionsMenu(true);

//        mContext.toolbar.setNavigationIcon(R.drawable.ic_back);
//        mContext.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().finish();
//                mContext.startActivity(new Intent(mContext,NavigationDrawerActivity.class));
//            }
//        });

        inItView(view);
        setTypeFace();
        setClickListener();
    }

    private void inItView(View view) {

        lay_contactus = (LinearLayout) view.findViewById(R.id.lay_contactus);
        lay_aboutus = (LinearLayout) view.findViewById(R.id.lay_aboutus);
        lay_legal = (LinearLayout) view.findViewById(R.id.lay_legal);
        lay_faq = (LinearLayout) view.findViewById(R.id.lay_faq);
        lay_subscription_process = (LinearLayout) view.findViewById(R.id.lay_subscription_process);
        lay_logout = (LinearLayout) view.findViewById(R.id.lay_logout);

        txt_faq = (TextView) view.findViewById(R.id.txt_faq);
        txt_aboutus = (TextView) view.findViewById(R.id.txt_aboutus);
        txt_contact_us = (TextView) view.findViewById(R.id.txt_contact_us);
        txt_subscription_process = (TextView) view.findViewById(R.id.txt_subscription_process);
        txt_legal = (TextView) view.findViewById(R.id.txt_legal);
        txt_logout = (TextView) view.findViewById(R.id.txt_logout);
    }

    private void setTypeFace()
    {
        txt_faq.setTypeface(mContext.getGujaratiFonts(mContext));
        txt_aboutus.setTypeface(mContext.getGujaratiFonts(mContext));
        txt_contact_us.setTypeface(mContext.getGujaratiFonts(mContext));
        txt_subscription_process.setTypeface(mContext.getGujaratiFonts(mContext));
        txt_legal.setTypeface(mContext.getGujaratiFonts(mContext));
        txt_logout.setTypeface(mContext.getGujaratiFonts(mContext));
    }

    private void setClickListener() {
        lay_subscription_process.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext, SubscriptionProcessActivity.class));
            }
        });

        lay_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext, FAQActivity.class));
            }
        });

        lay_contactus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext, ContactUsActivity.class));
            }
        });

        lay_aboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext, AboutUsActivity.class));
            }
        });

        lay_legal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext, LegalActivity.class));
            }
        });

        lay_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SettingsPreferences.clearDB(mContext);

                mContext.setLogin(0);
//
//                isFreshInstall = 0;
//                isFromTopicListing = 0;

                startActivity(new Intent(mContext, LoginActivity.class));
                mContext.finish();

            }
        });
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

//        getView().setFocusableInTouchMode(true);
//        getView().requestFocus();
//        getView().setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
//                    getActivity().finish();
//                    mContext.startActivity(new Intent(mContext,NavigationDrawerActivity.class));
//                    return true;
//                }
//                return false;
//            }
//        });
    }
}
