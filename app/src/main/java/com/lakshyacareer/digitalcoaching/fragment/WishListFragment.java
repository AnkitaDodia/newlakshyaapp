package com.lakshyacareer.digitalcoaching.fragment;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.activity.DashBoardActivity;
import com.lakshyacareer.digitalcoaching.activity.TopicsActivity;
import com.lakshyacareer.digitalcoaching.adapters.WishlistAdapter;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.Favourite;
import com.lakshyacareer.digitalcoaching.models.FavouriteData;
import com.lakshyacareer.digitalcoaching.models.MaterialsDownloads;
import com.lakshyacareer.digitalcoaching.models.StatusMessages;
import com.lakshyacareer.digitalcoaching.models.TopicsData;
import com.lakshyacareer.digitalcoaching.models.VideosDownloads;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.DatabaseHelper;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadManager;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.RetryPolicy;
import com.thin.downloadmanager.ThinDownloadManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by abc on 29-10-2017.
 */

public class WishListFragment extends Fragment {

    private String TAG = "WISHLIST";
    DashBoardActivity mContext;

    TextView txt_empty_downloads;
    RecyclerView rv_downloads;

    private ArrayList<FavouriteData> mFavouriteDataList = new ArrayList<>();
    FavouriteData mFavouriteData;

    Uri mDestinationUri;
    private ThinDownloadManager mThinDownloadManager;
    private static final int DOWNLOAD_THREAD_POOL_SIZE = 4;

    MyDownloadDownloadStatusListenerV1 mMyDownloadStatusListener = new MyDownloadDownloadStatusListenerV1();

    DownloadRequest downloadRequest;
    int downloadId;

    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mNotificationBuilder;
    int id = 1;
    String ChannelId = "my_channel_02";


    public String APP_PATH_SD_CARD ;
    private boolean isVideo = false;


    //    //Code for database
    DatabaseHelper mDbHelper, mDBAdapters;
    static int version_val = 2;
    MaterialsDownloads mMaterialsDownloads;
    VideosDownloads mVideosDownloads;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_downloads, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (DashBoardActivity) getActivity();


        initView(view);
        setTypeFace();
        setupForDB();
        if (mContext.CheckInternet(mContext)) {
            sendFavouriteListRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }


    }

    private void initView(View view) {

        txt_empty_downloads = view.findViewById(R.id.txt_empty_downloads);
        rv_downloads = view.findViewById(R.id.rv_downloads);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rv_downloads.setLayoutManager(mLayoutManager);
        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.rv_divider));
        rv_downloads.addItemDecoration(itemDecorator);

    }

    private void setTypeFace()
    {
        txt_empty_downloads.setTypeface(mContext.getGujaratiFonts(mContext));
    }

    private void sendFavouriteListRequest() {
        try {
            mContext.showWaitIndicator(true);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);


            String id_user = SettingsPreferences.getConsumer(mContext).getIdUser();

            Call<Favourite> call = service.sendFavouriteListRequest(id_user);

            call.enqueue(new Callback<Favourite>() {
                @Override
                public void onResponse(Call<Favourite> call, Response<Favourite> response) {

                    mContext.showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {
                            Log.e(TAG, "RESPONSE : " + new Gson().toJson(response.body().getData()));

                            if (response.body().getStatus() == 1) {

                                rv_downloads.setVisibility(View.VISIBLE);
                                txt_empty_downloads.setVisibility(View.GONE);

                                mFavouriteDataList = response.body().getData();
                                setAdapter();

                            } else {
                                rv_downloads.setVisibility(View.GONE);
                                txt_empty_downloads.setVisibility(View.VISIBLE);
                                txt_empty_downloads.setText("You have not wish listed anything yet");
                            }

                        }
                    } catch (Exception e) {
                        mContext.showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Favourite> call, Throwable t) {
                    mContext.showWaitIndicator(false);
                    Log.d(TAG, "onFailure" + t.toString());
                    Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {

        WishlistAdapter mWishlistAdapter = new WishlistAdapter(mContext, mFavouriteDataList, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View v, int position) {

                mFavouriteData = mFavouriteDataList.get(position);

                if (v.getId() == R.id.lay_topic_video) {

                    if (mContext.CheckInternet(mContext)) {

                        isVideo = true;
                        APP_PATH_SD_CARD = "/Subjects/Topics/Videos";
                        String mMaterialName = CreateFileName(mFavouriteData.getVideo());
                        InitDownload(mFavouriteData.getVideoLink(), mFavouriteData, mMaterialName);

                    } else {

                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();

                    }

                } else if (v.getId() == R.id.lay_document_file) {

                    if (mContext.CheckInternet(mContext)) {

                        isVideo = false;
                        APP_PATH_SD_CARD = "/Subjects/Topics/Materials";
                        String mMaterialName = CreateFileName(mFavouriteData.getMaterial());//+"_"+mTopicsData.getMaterial()
                        InitDownload(mFavouriteData.getMaterialLink(), mFavouriteData, mMaterialName);

                    } else {

                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();

                    }
                } else if (v.getId() == R.id.lay_topic_favorite) {

                    if (mContext.CheckInternet(mContext))
                    {


                        switch (mFavouriteData.getFavourite())
                        {
                            case 0:
//                                sendAddToFavouriteRequest(mFavouriteData.getIdMaterial());
                                break;
                            case 1:
                                sendRemoveFavouriteRequest(mFavouriteData.getIdMaterial());
                                break;
                        }
                    }
                    else
                    {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }

                } else if (v.getId() == R.id.ll_parent_row_topics) {

//                    Toast.makeText(mContext, "Clicked on Parent ", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });
        rv_downloads.setAdapter(mWishlistAdapter);
    }

    private void sendRemoveFavouriteRequest(String id_material)
    {
        try {
            mContext.showWaitIndicator(true);

            String id_user = SettingsPreferences.getConsumer(mContext).getIdUser();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            Call<StatusMessages> call = service.sendRemoveFavouriteRequest(id_user, id_material);

            call.enqueue(new Callback<StatusMessages>() {
                @Override
                public void onResponse(Call<StatusMessages> call, Response<StatusMessages> response) {

                    mContext.showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {

                            if(response.body().getStatus() == 1){

                                if (mContext.CheckInternet(mContext)) {
                                    sendFavouriteListRequest();
                                } else {
                                    Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                                }
                                Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            }else {

                                Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            }

                        }
                    } catch (Exception e) {
                        mContext.showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<StatusMessages> call, Throwable t) {
                    mContext.showWaitIndicator(false);
                    Log.d(TAG,"onFailure"+t.toString());
                    Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setupForDB()
    {
        mVideosDownloads = new VideosDownloads();
        mMaterialsDownloads = new MaterialsDownloads();

        try {
            mDbHelper = new DatabaseHelper(mContext, "LakshyCareer.sqlite", null,version_val);
            mDBAdapters = DatabaseHelper.getDBAdapterInstance(mContext);
            mDBAdapters.createDataBase();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public String CreateFileName(String filename){
        return filename.replace(" ", "");
    }

    private void InitDownload(String DownloadURL, FavouriteData mfavouritedata, String DownloadfileName)
    {
        mThinDownloadManager = new ThinDownloadManager(DOWNLOAD_THREAD_POOL_SIZE);
        RetryPolicy retryPolicy = new DefaultRetryPolicy();

//        File filesDir = getExternalFilesDir("");
        File filesDir = mContext.getExternalFilesDir(APP_PATH_SD_CARD);

        String DestinationPath = filesDir+"/"+DownloadfileName;


        Log.e("DestinationPath","DestinationPath    :"+DestinationPath);


        Uri downloadUri = Uri.parse(DownloadURL);
        mDestinationUri = Uri.parse(DestinationPath);
        downloadRequest = new DownloadRequest(downloadUri)
                .setDestinationURI(mDestinationUri).setPriority(DownloadRequest.Priority.LOW)
                .setRetryPolicy(retryPolicy)
                .setDownloadContext("Download")
                .setStatusListener(mMyDownloadStatusListener);


        mNotifyManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel mChannel = new NotificationChannel(ChannelId, "channel_name",NotificationManager.IMPORTANCE_LOW);
            mChannel.setSound(null, null);
            mNotifyManager.createNotificationChannel(mChannel);
        }
//        build = new NotificationCompat.Builder(ctx);
        mNotificationBuilder = new NotificationCompat.Builder(mContext, ChannelId);
        mNotificationBuilder.setContentTitle(""+mfavouritedata.getSubjectName())
                .setContentText("Download in progress")
                .setSmallIcon(R.drawable.ic_lakshya_notification);

        Intent mPendingIntent = new Intent(mContext, DashBoardActivity.class);
        mPendingIntent.putExtra("PendingIntent", "PendingIntent");
        PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext, 0,mPendingIntent,0);

        mNotificationBuilder.setContentIntent(resultPendingIntent);
        mNotificationBuilder.setAutoCancel(true);

        if (mThinDownloadManager.query(downloadId) == DownloadManager.STATUS_NOT_FOUND) {
            downloadId = mThinDownloadManager.add(downloadRequest);
        }

        mNotificationBuilder.setContentText("Download Started");
        // Removes the progress bar
        mNotificationBuilder.setProgress(0, 0, false);
        mNotifyManager.notify(id, mNotificationBuilder.build());
    }

    class MyDownloadDownloadStatusListenerV1 implements DownloadStatusListenerV1 {

        @Override
        public void onDownloadComplete(DownloadRequest request) {
            final int id = request.getDownloadId();
            if (id == downloadId) {
//                mProgressTxt.setText(request.getDownloadContext() + "Download Completed");

                mNotificationBuilder.setContentText("Download Completed");
                // Removes the progress bar
                mNotificationBuilder.setProgress(100, 100, false);
                mNotifyManager.notify(id, mNotificationBuilder.build());

//                addDownloadRequest();
                AddToLocalDatabase();
            }
        }

        @Override
        public void onDownloadFailed(DownloadRequest request, int errorCode, String errorMessage) {
            final int id = request.getDownloadId();
            if (id == downloadId) {
//                mProgressTxt.setText("Download Failed");
//                mProgress.setProgress(0);

                mNotificationBuilder.setContentText("Download Failed");
                mNotifyManager.notify(id, mNotificationBuilder.build());

                Log.e("errorMessage", "errorMessage :"+errorMessage);
            }
        }

        @Override
        public void onProgress(DownloadRequest request, long totalBytes, long downloadedBytes, int progress) {
            int id = request.getDownloadId();

            System.out.println("######## onProgress ###### "+id+" : "+totalBytes+" : "+downloadedBytes+" : "+progress);
            if (id == downloadId) {
//                mProgressTxt.setText("Downloading...  "+", "+progress+"%"+"  "+getBytesDownloaded(progress,totalBytes));
//                mProgress.setProgress(progress);

                mNotificationBuilder.setContentText("Downloading... "+progress+"%"+"  "+getBytesDownloaded(progress,totalBytes));
                mNotificationBuilder.setProgress(100, progress, false);
                mNotifyManager.notify(id, mNotificationBuilder.build());

            }
        }

    }

    private void AddToLocalDatabase() {

        if(isVideo){
            try
            {
                mDBAdapters.openDataBase();
                Log.e("destinationUri",mDestinationUri.toString());

                mVideosDownloads.setSubjectName(mFavouriteData.getSubjectName());
                mVideosDownloads.setVideoName(mFavouriteData.getVideo());
                mVideosDownloads.setVideoSize(mFavouriteData.getVideoSize());
                mVideosDownloads.setVideoDuraion(mFavouriteData.getDuration());
                mVideosDownloads.setVideoPath(mDestinationUri.toString());

                mDBAdapters.InsertVideo(mVideosDownloads);

                mDBAdapters.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }else {

            try
            {
                mDBAdapters.openDataBase();
                Log.e("destinationUri",mDestinationUri.toString());

                mMaterialsDownloads.setSubjectName(mFavouriteData.getSubjectName());
                mMaterialsDownloads.setMaterialName(mFavouriteData.getMaterial());
                mMaterialsDownloads.setMaterialSize(mFavouriteData.getMaterialSize());
                mMaterialsDownloads.setMaterialPath(mDestinationUri.toString());

                mDBAdapters.InsertMaterial(mMaterialsDownloads);

                mDBAdapters.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        sendAddToDownloadsRequest(mFavouriteData.getIdMaterial());

    }

    private String getBytesDownloaded(int progress, long totalBytes) {

        //Greater than 1 MB
        long bytesCompleted = (progress * totalBytes)/100;
        if (totalBytes >= 1000000) {
            return (""+(String.format("%.1f", (float)bytesCompleted/1000000))+ "/"+ ( String.format("%.1f", (float)totalBytes/1000000)) + "MB");
        } if (totalBytes >= 1000) {
            return (""+(String.format("%.1f", (float)bytesCompleted/1000))+ "/"+ ( String.format("%.1f", (float)totalBytes/1000)) + "Kb");

        } else {
            return ( ""+bytesCompleted+"/"+totalBytes );
        }

    }

    private void sendAddToDownloadsRequest(String id_material)
    {
        try {
            mContext.showWaitIndicator(true);

            String id_user = SettingsPreferences.getConsumer(mContext).getIdUser();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            Call<StatusMessages> call = service.sendAddToDownloadsRequest(id_user, id_material);

            call.enqueue(new Callback<StatusMessages>() {
                @Override
                public void onResponse(Call<StatusMessages> call, Response<StatusMessages> response) {

                    mContext.showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {

                            if(response.body().getStatus() == 1){

                                if (mContext.CheckInternet(mContext)) {
                                    sendFavouriteListRequest();
                                } else {
                                    Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                                }
                                Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            }else {

                                Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            }

                        }
                    } catch (Exception e) {
                        mContext.showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<StatusMessages> call, Throwable t) {
                    mContext.showWaitIndicator(false);
                    Log.d(TAG,"onFailure"+t.toString());
                    Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



}
