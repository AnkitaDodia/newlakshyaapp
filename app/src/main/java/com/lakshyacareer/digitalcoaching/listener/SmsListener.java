package com.lakshyacareer.digitalcoaching.listener;

public interface SmsListener{
    public void onMessageReceived(String messageText);
}