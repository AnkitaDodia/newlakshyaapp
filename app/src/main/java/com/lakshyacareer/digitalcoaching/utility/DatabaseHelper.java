package com.lakshyacareer.digitalcoaching.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Environment;

//import com.lakshyacareer.digitalcoaching.model.MyDownloadsList;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.models.Cart;
import com.lakshyacareer.digitalcoaching.models.MaterialsDownloads;
import com.lakshyacareer.digitalcoaching.models.PackagePrice;
import com.lakshyacareer.digitalcoaching.models.PackagesData;
import com.lakshyacareer.digitalcoaching.models.VideosDownloads;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by My 7 on 11/11/2017.
 */
public class DatabaseHelper extends SQLiteOpenHelper
{
    private static String DB_PATH = "";
    private static final String DB_NAME = "LakshyCareer.sqlite";
    private SQLiteDatabase myDataBase;
    private final Context myContext;
    private static DatabaseHelper mDBConnection;
    static int version_val = 2;

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) throws IOException {
        super(context, name, factory, version);
        this.myContext = context;

        if (android.os.Build.VERSION.SDK_INT >= 17)
            DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        else
            DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";

        copyDataBase();

        this.getReadableDatabase();
    }

    public static synchronized DatabaseHelper getDBAdapterInstance(Context context) throws IOException {
        if (mDBConnection == null)
        {
            mDBConnection = new DatabaseHelper(context, DB_NAME, null,version_val);
        }
        return mDBConnection;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void createDataBase() throws IOException
    {
        boolean dbExist = checkDataBase();
        if (dbExist) {
            // do nothing - database already exist
        } else {
            this.getReadableDatabase();
            this.close();
            try {
                copyDataBase();
            } catch (Exception e) {
                throw new Error("Error copying database:" + e.toString());
            }
        }
    }

    public boolean databaseExist() {
        File dbFile = new File(DB_PATH + DB_NAME);
        return dbFile.exists();
    }

    private boolean checkDataBase() {
        File dbFile = new File(DB_PATH + DB_NAME);
        return dbFile.exists();
    }

    private void copyDataBase() {
        if (!checkDataBase()) {
            this.getReadableDatabase();
            this.close();
            try {
                copyDBFile();
            } catch (IOException mIOException) {
                throw new Error("ErrorCopyingDataBase");
            }
        }
    }

    private void copyDBFile() throws IOException {
        //InputStream mInput = mContext.getAssets().open(DB_NAME);
        InputStream mInput = myContext.getResources().getAssets().open("LakshyCareer.sqlite");
        OutputStream mOutput = new FileOutputStream(DB_PATH + DB_NAME);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer)) > 0)
            mOutput.write(mBuffer, 0, mLength);
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

//    private boolean checkDataBase() {
//
//        File dbFile = myContext.getDatabasePath(DB_PATH + DB_NAME);
//        return dbFile.exists();
//    }

//    private void copyDataBase() throws IOException {
//        InputStream myInput = myContext.getAssets().open(DB_NAME);
//        String outFileName = DB_PATH + DB_NAME;
//        OutputStream myOutput = new FileOutputStream(outFileName);
//        byte[] buffer = new byte[1024];
//        int length;
//        while ((length = myInput.read(buffer)) > 0) {
//            myOutput.write(buffer, 0, length);
//        }
//
//        myOutput.flush();
//        myOutput.close();
//        myInput.close();
//    }

    public void openDataBase() throws SQLException {
        String myPath;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            myDataBase = getReadableDatabase();
            myPath = myDataBase.getPath();

        } else {
            String DB_PATH = Environment.getDataDirectory() + "/data/com.lakshyacareer.digitalcoaching/databases/";
            myPath = DB_PATH + DB_NAME;
        }

//        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READWRITE);
    }

    @Override
    public synchronized void close() {
        if (myDataBase != null)
            myDataBase.close();
        super.close();
    }

    public long InsertPackage(String id,String packageName, String duration, String price, String position) {
        // TODO Auto-generated method stub
        long rawId = 0;
        try {
            ContentValues CV = new ContentValues();

            CV.put("package_id",id);
            CV.put("package_name",packageName);
            CV.put("package_duration",duration);
            CV.put("package_price",price);
            CV.put("spinner_position",position);

            rawId = myDataBase.insert("packages", null, CV);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return rawId;
    }

    public ArrayList<Cart> SelectPackage()
    {
        // TODO Auto-generated method stub
        ArrayList<Cart> list = new ArrayList<Cart>();
        Cursor cursor = myDataBase.rawQuery("SELECT * FROM packages", null);
        if (cursor.moveToFirst())
        {
            do
            {
                Cart bean = new Cart();
                bean.setId(cursor.getString(1));
                bean.setPackageName(cursor.getString(2));
                bean.setDuration(cursor.getString(3));
                bean.setPrice(cursor.getString(4));

                list.add(bean);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }

    public long UpdatePackage(String package_id, String id,String packageName, String duration, String price,String position) {
        long rawId = 0;
        try {
            ContentValues CV = new ContentValues();

            CV.put("package_id",id);
            CV.put("package_name",packageName);
            CV.put("package_duration",duration);
            CV.put("package_price",price);
            CV.put("spinner_position",position);

            rawId = myDataBase.update("packages", CV, "package_id=" + package_id,null);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return rawId;
    }

    public void DeletePackage(String package_id) {
        myDataBase.delete("packages", "package_id=" + package_id, null);
    }

    public String ValidateID(String spid) {
        Cursor cursor = myDataBase.rawQuery(
                "SELECT package_id FROM packages where package_id =" + spid + "",null);
        String result = "-1";
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    result = cursor.getString(0);
                } while (cursor.moveToNext());
            }

        } else {
            result = "-1";
        }
        cursor.close();

        return result;
    }

    public String getSpinnerPosition(String spid) {
        Cursor cursor = myDataBase.rawQuery(
                "SELECT spinner_position FROM packages where package_id =" + spid + "",null);
        String result = "-1";
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    result = cursor.getString(0);
                } while (cursor.moveToNext());
            }

        } else {
            result = "-1";
        }
        cursor.close();

        return result;
    }

    public void deletePackage() {
//        myDataBase.delete("packages", null, null);
        myDataBase.delete("packages", null, null);
    }

    public String ValidateVideoID(String spid) {
        Cursor cursor = myDataBase.rawQuery(
                "SELECT material_id FROM videos where material_id =" + spid + "",null);
        String result = "-1";
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    result = cursor.getString(0);
                } while (cursor.moveToNext());
            }

        } else {
            result = "-1";
        }
        cursor.close();

        return result;
    }

    public long InsertVideo(VideosDownloads model) {
    // TODO Auto-generated method stub
    long rawId = 0;
    try {
        ContentValues CV = new ContentValues();

        CV.put("material_id",model.getMaterialId());
        CV.put("material_topic_name",model.getMaterialTopicName());
        CV.put("subject_name",model.getSubjectName());
        CV.put("video_name",model.getVideoName());
        CV.put("video_path",model.getVideoPath());
        CV.put("video_size",model.getVideoSize());
        CV.put("video_duration",model.getVideoDuraion());

        rawId = myDataBase.insert("videos", null, CV);

    } catch (Exception e) {
        e.printStackTrace();
    }
    return rawId;
}

    public ArrayList<VideosDownloads> GetVideos()
    {
        // TODO Auto-generated method stub
        ArrayList<VideosDownloads> list = new ArrayList<VideosDownloads>();
        Cursor cursor = myDataBase.rawQuery("SELECT * FROM videos", null);
        if (cursor.moveToFirst())
        {
            do
            {
                VideosDownloads bean = new VideosDownloads();
                bean.setMaterialId(cursor.getString(3));
                bean.setMaterialTopicName(cursor.getString(4));
                bean.setSubjectName(cursor.getString(0));
                bean.setVideoName(cursor.getString(2));
                bean.setVideoSize(cursor.getString(1));
                bean.setVideoPath(cursor.getString(5));
                bean.setVideoDuraion(cursor.getString(6));

                list.add(bean);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }

    public void DeleteVideo(String material_id) {
        myDataBase.delete("videos", "material_id=" + material_id, null);
    }

    public String ValidateMaterialID(String spid) {
        Cursor cursor = myDataBase.rawQuery(
                "SELECT material_id FROM materials where material_id =" + spid + "",null);
        String result = "-1";
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    result = cursor.getString(0);
                } while (cursor.moveToNext());
            }

        } else {
            result = "-1";
        }
        cursor.close();

        return result;
    }

    public long InsertMaterial(MaterialsDownloads model) {
        // TODO Auto-generated method stub
        long rawId = 0;
        try {
            ContentValues CV = new ContentValues();

            CV.put("material_id",model.getMaterialId());
            CV.put("material_topic_name",model.getMaterialTopicName());
            CV.put("subject_name",model.getSubjectName());
            CV.put("material_name",model.getMaterialName());
            CV.put("material_path",model.getMaterialPath());
            CV.put("material_size",model.getMaterialSize());

            rawId = myDataBase.insert("materials", null, CV);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return rawId;
    }

    public ArrayList<MaterialsDownloads> GetMaterials()
    {
        // TODO Auto-generated method stub
        ArrayList<MaterialsDownloads> list = new ArrayList<MaterialsDownloads>();
        Cursor cursor = myDataBase.rawQuery("SELECT * FROM materials", null);
        if (cursor.moveToFirst())
        {
            do
            {
                MaterialsDownloads bean = new MaterialsDownloads();
                bean.setMaterialId(cursor.getString(5));
                bean.setMaterialTopicName(cursor.getString(2));
                bean.setSubjectName(cursor.getString(0));
                bean.setMaterialName(cursor.getString(1));
                bean.setMaterialSize(cursor.getString(4));
                bean.setMaterialPath(cursor.getString(3));

                list.add(bean);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }

    public void DeleteMaterial(String material_id) {
        myDataBase.delete("materials", "material_id=" + material_id, null);
    }
}
