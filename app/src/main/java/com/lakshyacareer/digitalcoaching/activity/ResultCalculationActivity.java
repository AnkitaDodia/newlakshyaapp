package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.Answers;
import com.lakshyacareer.digitalcoaching.models.User;
import com.lakshyacareer.digitalcoaching.models.UserData;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ResultCalculationActivity extends BaseActivity {

    String TAG = "RESULT";
    Context mContext;

    TextView txt_topbar_title, txt_right_answers, txt_wrong_answers, txt_not_attampt, txt_result_score;
    LinearLayout ll_parent_result, layout_back;
    private String mResult ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate_result);

        mContext = this;
        InitViews();
        SetClickListners();
        SetResultData();

    }

    private void InitViews() {
        ll_parent_result = findViewById(R.id.ll_parent_result);
        layout_back = findViewById(R.id.layout_back);
        txt_topbar_title = findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText(getResources().getString(R.string.txt_label_result));
        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));

        txt_right_answers = findViewById(R.id.txt_right_answers);
        txt_wrong_answers = findViewById(R.id.txt_wrong_answers);
        txt_not_attampt = findViewById(R.id.txt_not_attampt);
        txt_result_score = findViewById(R.id.txt_result_score);

        overrideFonts(ll_parent_result, mContext);

    }

    private void SetClickListners() {

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });
    }

    private void SetResultData() {

        Log.e("NOT_ATTEMPTED_ANSWERS", "" + BaseActivity.NOT_ATTEMPTED_ANSWERS);
        Log.e("RIGHT_ANSWERS", "" + BaseActivity.RIGHT_ANSWERS);
        Log.e("WRONG_ANSWERS", "" + BaseActivity.WRONG_ANSWERS);

        double mWrongAns = BaseActivity.WRONG_ANSWERS * 0.3;

        double score = BaseActivity.RIGHT_ANSWERS - mWrongAns;
        Log.e("mResult", "" + mResult);
        Log.e("questions", "" + questions);

        mResult = String.format("%.2f", score);
        txt_right_answers.setText(": "+BaseActivity.RIGHT_ANSWERS);
        txt_wrong_answers.setText(": "+BaseActivity.WRONG_ANSWERS);
        txt_not_attampt.setText(": "+BaseActivity.NOT_ATTEMPTED_ANSWERS);
        txt_result_score.setText(": "+mResult);

        if (BaseActivity.CheckInternet(mContext)) {
            sendAnswersRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void sendAnswersRequest()
    {
        try {

            String id_user = SettingsPreferences.getConsumer(this).getIdUser();
            showWaitIndicator(true);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            Call<Answers> call = service.sendAnswersRequest(id_user, questions.toString(), BaseActivity.RIGHT_ANSWERS, BaseActivity.WRONG_ANSWERS, BaseActivity.NOT_ATTEMPTED_ANSWERS, mResult);
            call.enqueue(new Callback<Answers>() {
                @Override
                public void onResponse(Call<Answers> call, Response<Answers> response) {

                    showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {
//                            Log.e(TAG, "RESPONSE : "+ new Gson().toJson(response.body().getData()));

                            if(response.body().getStatus() == 1)
                            {
//                                Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            }else {

//                                Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                    } catch (Exception e) {
                        showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Answers> call, Throwable t) {
                    showWaitIndicator(false);
                    Log.d(TAG,"onFailure"+t.toString());
                    Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
