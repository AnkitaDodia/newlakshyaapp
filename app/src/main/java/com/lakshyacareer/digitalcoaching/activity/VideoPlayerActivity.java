package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.videoplayer.EasyVideoCallback;
import com.lakshyacareer.digitalcoaching.videoplayer.EasyVideoPlayer;

/**
 * Created by abc on 11-11-2017.
 */

public class VideoPlayerActivity extends BaseActivity implements EasyVideoCallback {

    Context ctx;
    String TAG = "VideoPlayer";
    String vidAddress ;

    private EasyVideoPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        ctx = this;

        vidAddress = getIntent().getExtras().getString("video_path");

        Log.d("vidAddress", "PATH  :"+vidAddress);

//        vidAddress = "http://admin.lakshyacareer.in/uploads/AuDagYWrSh.mp4";
        player = (EasyVideoPlayer) findViewById(R.id.player);
        assert player != null;
        player.setCallback(this);

        player.setAutoPlay(true);

        player.setSource(Uri.parse(vidAddress));
    }

    @Override
    protected void onPause() {
        super.onPause();
        player.pause();
    }

    @Override
    public void onStarted(EasyVideoPlayer player) {}

    @Override
    public void onPaused(EasyVideoPlayer player) {}

    @Override
    public void onPreparing(EasyVideoPlayer player) {
        Log.d("EVP-Sample", "onPreparing()");
    }

    @Override
    public void onPrepared(EasyVideoPlayer player) {
        Log.d("EVP-Sample", "onPrepared()");
    }

    @Override
    public void onBuffering(int percent) {
        Log.d("EVP-Sample", "onBuffering(): " + percent + "%");
    }

    @Override
    public void onError(EasyVideoPlayer player, Exception e) {
        Log.d("EVP-Sample", "onError(): " + e.getMessage());
        /*new MaterialDialog.Builder(this)
                .title(R.string.error)
                .content(e.getMessage())
                .positiveText(android.R.string.ok)
                .show();*/
    }

    @Override
    public void onCompletion(EasyVideoPlayer player) {
        Log.d("EVP-Sample", "onCompletion()");
    }

    @Override
    public void onRetry(EasyVideoPlayer player, Uri source) {
//        Toast.makeText(this, "Retry", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSubmit(EasyVideoPlayer player, Uri source) {
//        Toast.makeText(this, "Submit", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClickVideoFrame(EasyVideoPlayer player) {
//        Toast.makeText(this, "Click video frame.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}
