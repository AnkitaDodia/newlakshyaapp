package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.CurrentAffairsData;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class DetailCurrentAffairsActivity extends BaseActivity {

    Context mContext;
    TextView txt_topbar_title, txt_current_affair_title, txt_current_affair_descriptions;
    LinearLayout layout_back, ll_previous, ll_next;
    ImageView img_details_current_affair;
    ProgressBar prgs_current_affair;
    private int mItemPosition = 0, mPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_current_affairs);

        mContext = this;
        InitViews();
        SetClickListners();
        mItemPosition = BaseActivity.CURRENT_AFFAIRS_POSITION;
        mPosition = BaseActivity.CURRENT_AFFAIRS_POSITION;
        LoadCurrentaffairsData(mItemPosition);

        NextPrevVisibility();
    }

    private void LoadCurrentaffairsData(int position) {

        CurrentAffairsData mCurrentAffairsData = mCurrentAffairsDataList.get(position);

        Glide.with(mContext).load(mCurrentAffairsData.getImage())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        prgs_current_affair.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                            holder.img_vendor_services.setImageDrawable(resource);
                        prgs_current_affair.setVisibility(View.GONE);
                        return false;
                    }
                }).into(img_details_current_affair);


        txt_current_affair_title.setText(mCurrentAffairsData.getTitle());
        txt_current_affair_descriptions.setText(mCurrentAffairsData.getDescription());
    }

    private void InitViews() {


        layout_back = findViewById(R.id.layout_back);
        txt_topbar_title = findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText(getResources().getString(R.string.txt_label_current_affair));
        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));

        txt_current_affair_title = findViewById(R.id.txt_current_affair_title);
        txt_current_affair_descriptions = findViewById(R.id.txt_current_affair_descriptions);
        img_details_current_affair = findViewById(R.id.img_details_current_affair);
        prgs_current_affair = findViewById(R.id.prgs_current_affair);

        ll_previous = findViewById(R.id.ll_previous);
        ll_next = findViewById(R.id.ll_next);


        txt_current_affair_title.setTypeface(getGujaratiFonts(mContext));
        txt_current_affair_descriptions.setTypeface(getGujaratiFonts(mContext));
    }

    private void SetClickListners(){

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ll_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPosition = mItemPosition +1;
                mItemPosition = mItemPosition+1;
                if(mItemPosition<mCurrentAffairsDataList.size()){

                    LoadCurrentaffairsData(mItemPosition);
                }
                else{
                    mItemPosition = mItemPosition-1;
                }

                NextPrevVisibility();
            }
        });

        ll_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mPosition = mItemPosition -1;
                mItemPosition=mItemPosition-1;
                if(mItemPosition>=0){
                    LoadCurrentaffairsData(mItemPosition);
                }
                else{
                    mItemPosition=mItemPosition+1;
                }
                NextPrevVisibility();
            }
        });


    }

    private void NextPrevVisibility(){

        Log.e("CheckNextLogic", "mItemPosition : "+mPosition);
        Log.e("CheckNextLogic", "mLatestNewsList.size() : "+mLatestNewsList.size());

        if(mPosition == mLatestNewsList.size() -1){
            ll_next.setVisibility(View.INVISIBLE);
            ll_previous.setVisibility(View.VISIBLE);
        }else if(mPosition == 0){
            ll_previous.setVisibility(View.INVISIBLE);
            ll_next.setVisibility(View.VISIBLE);
        }else {
            ll_next.setVisibility(View.VISIBLE);
            ll_previous.setVisibility(View.VISIBLE);
        }

    }
}
