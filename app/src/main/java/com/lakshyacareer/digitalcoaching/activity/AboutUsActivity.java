package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.AboutUs;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by abc on 31-10-2017.
 */

public class AboutUsActivity extends BaseActivity {

    Context mContext;
    TextView txt_topbar_title, txt_about_content;
    LinearLayout layout_back;

    WebView webview_about;
    String text = null;
    CardView card_aboutus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);

        mContext = this;

        initViews();
        setTypeFace();
        setClickListener();


        if(BaseActivity.mText != null)
        {
            text = BaseActivity.mText;
            card_aboutus.setVisibility(View.VISIBLE);
            webview_about.loadData("<font>" + text + "</font>",
                    "text/html; charset=UTF-8", null);
        }else{
            if (CheckInternet(mContext)) {
                sendAboutUsRequest();
            } else {
                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initViews() {
        layout_back = (LinearLayout) findViewById(R.id.layout_back);

        txt_topbar_title = (TextView)findViewById(R.id.txt_topbar_title);
        txt_about_content = (TextView)findViewById(R.id.txt_about_content);

        webview_about = (WebView) findViewById(R.id.webview_about);

        card_aboutus = (CardView)findViewById(R.id.card_aboutus);
        card_aboutus.setVisibility(View.GONE);
    }

    private void setTypeFace()
    {
        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));
        txt_about_content.setTypeface(getGujaratiFonts(mContext));

        txt_topbar_title.setText("About Us");
    }

    private void setClickListener()
    {
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void sendAboutUsRequest() {

        showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<AboutUs> call = service.sendAboutUsRequest();

        call.enqueue(new Callback<AboutUs>() {
            @Override
            public void onResponse(Call<AboutUs> call, Response<AboutUs> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        if (response.body().getStatus() == 1) {
                            try {
                                String about = response.body().getMessage();
                                String text = "<html><body style=\"color:#484848;font-family:file:///android_asset//ProximaNovaSemibold.ttf;font-size:20px;\"'background-color:transparent' >" + "<p align=\"justify\">" + about + "</p>" + "</body></html>";

                                BaseActivity.mText = text;

                                webview_about.loadData("<font>" + text + "</font>",
                                        "text/html; charset=UTF-8", null);

//                        Toast.makeText(ctx, "Tessstttt", Toast.LENGTH_LONG).show();
                                card_aboutus.setVisibility(View.VISIBLE);
                                showWaitIndicator(false);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AboutUs> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }


    @Override
    public void onBackPressed() {
        finish();
    }
}