package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.adapters.QuestionsAdapter;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.Questions;
import com.lakshyacareer.digitalcoaching.models.QuestionsData;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.ui.NonSwipeableViewPager;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PracticeActivity extends BaseActivity {

    String TAG = "PRACTICE";
    PracticeActivity mContext;

    TextView txt_topbar_title, txt_questions_number, txt_right_number, txt_wrong_number;
    LinearLayout layout_back;
    Button btn_next_questions;

    NonSwipeableViewPager vp_practice;
    private int currentPage = 0;
    int number = 1;

    ArrayList<QuestionsData> mQuestionsDataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice);

        mContext = this;
        InitViews();
        SetClickListners();

        if (CheckInternet(mContext)) {
            sendQuestionsRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }

        BaseActivity.RIGHT_ANSWERS = 0;
        BaseActivity.WRONG_ANSWERS = 0;
        BaseActivity.SELECTED_ANSWER = null;
    }

    private void InitViews() {

        layout_back = findViewById(R.id.layout_back);
        txt_topbar_title = findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText(getResources().getString(R.string.txt_label_practice));
        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));

        btn_next_questions = findViewById(R.id.btn_next_questions);
        vp_practice = findViewById(R.id.vp_practice);

        txt_right_number = findViewById(R.id.txt_right_number);
        txt_questions_number = findViewById(R.id.txt_questions_number);
        txt_wrong_number = findViewById(R.id.txt_wrong_number);

        txt_right_number.setTypeface(getGujaratiFonts(mContext));
        txt_questions_number.setTypeface(getGujaratiFonts(mContext));
        txt_wrong_number.setTypeface(getGujaratiFonts(mContext));
        btn_next_questions.setTypeface(getGujaratiFonts(mContext));
    }

    private void SetClickListners() {

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        btn_next_questions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (BaseActivity.SELECTED_ANSWER != null) {

                    Log.e("ANSWER", "Selected : " + BaseActivity.SELECTED_ANSWER + "  API : " + mQuestionsDataList.get(currentPage).getAnswer());
                    if(BaseActivity.SELECTED_ANSWER.equalsIgnoreCase("E")) {

                        BaseActivity.NOT_ATTEMPTED_ANSWERS = BaseActivity.NOT_ATTEMPTED_ANSWERS + 1;
                    } else if (BaseActivity.SELECTED_ANSWER.contains(mQuestionsDataList.get(currentPage).getAnswer().toString())) {

                        BaseActivity.RIGHT_ANSWERS = BaseActivity.RIGHT_ANSWERS + 1;
                    } else {
                        BaseActivity.WRONG_ANSWERS = BaseActivity.WRONG_ANSWERS + 1;
                    }

                    try {
                        JSONObject ans = new JSONObject();
                        ans.put("que_id",mQuestionsDataList.get(currentPage).getIdQuestion());
                        ans.put("user_ans",BaseActivity.SELECTED_ANSWER);
                        ans.put("real_ans",mQuestionsDataList.get(currentPage).getAnswer());

                        questions.put(ans);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    BaseActivity.SELECTED_ANSWER = null;
                    Log.e("ANSWER", "Right : " + BaseActivity.RIGHT_ANSWERS + "  wrong : " + BaseActivity.WRONG_ANSWERS);

                    txt_right_number.setText(""+BaseActivity.RIGHT_ANSWERS);
                    txt_wrong_number.setText("" + BaseActivity.WRONG_ANSWERS);
                    number = number + 1;
                    currentPage++;
                    if(currentPage != mQuestionsDataList.size()) {
                        txt_questions_number.setText(number + "/" + mQuestionsDataList.size());
                    }

                    vp_practice.setCurrentItem(currentPage, true);


                    if(currentPage == mQuestionsDataList.size() - 1) {
                        btn_next_questions.setText("Finish");
                    }

                    if(currentPage == mQuestionsDataList.size()) {
                        startActivity(new Intent(PracticeActivity.this,ResultCalculationActivity.class));
                        finish();
                    }

                } else {
                    Toast.makeText(mContext, "Please choose at least one answer.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void sendQuestionsRequest() {

        showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<Questions> call = service.sendQuestionsRequest();

        call.enqueue(new Callback<Questions>() {
            @Override
            public void onResponse(Call<Questions> call, Response<Questions> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e("QUESTION_LIST", "FULL RESPONSE : " + new Gson().toJson(response.body().getData()));

                        if (response.body().getStatus() == 1) {

                            mQuestionsDataList = response.body().getData();
                            BaseActivity.TOTAL_QUESTIONS = mQuestionsDataList.size();
                            txt_questions_number.setText(number + "/" + mQuestionsDataList.size());
                            QuestionsAdapter mQuestionsAdapter = new QuestionsAdapter(mContext, mQuestionsDataList);
                            vp_practice.setAdapter(mQuestionsAdapter);


                        } else {


                        }
                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Questions> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
