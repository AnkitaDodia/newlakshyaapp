package com.lakshyacareer.digitalcoaching.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.User;
import com.lakshyacareer.digitalcoaching.models.UserData;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.ui.UnderlineTextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends BaseActivity {

    private static final String TAG = "LOGIN";
    LoginActivity mContext;

    LinearLayout ll_parent_login, layout_login_otp;
    UnderlineTextView txt_forgot_password, txt_signup;
    EditText edt_email, edt_password;
    Button btn_login;

    CheckBox cb_remember_me;

    String IMEI_number;
    public static final int REQUEST_READ_PHONE_STATE = 1;
    boolean doubleBackToExitPressedOnce = false;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mContext = this;
        initView();
        setClickListeners();

        //Code for check run time permission
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
        } else {
            //TODO
            IMEI_number = getIMEINumber();
            Log.e("IMEI_NUMBER", IMEI_number);
        }
    }

    private void initView() {

        ll_parent_login = findViewById(R.id.ll_parent_login);
        layout_login_otp = findViewById(R.id.layout_login_otp);

        txt_forgot_password = findViewById(R.id.txt_forgot_password);

        txt_signup = findViewById(R.id.txt_signup);

        edt_email = findViewById(R.id.edt_email);
        edt_password = findViewById(R.id.edt_password);

        btn_login = findViewById(R.id.btn_login);

        cb_remember_me = findViewById(R.id.cb_remember_me);

        overrideFonts(ll_parent_login, mContext);

        if (getRemember()) {
            cb_remember_me.setChecked(true);
            edt_email.setText(getLoginId());
            edt_password.setText(getLoginPwd());
        }
    }

    private void setClickListeners() {

        ll_parent_login.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                View view = mContext.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                return false;
            }
        });

        btn_login.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                View view = mContext.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                return false;
            }
        });

        txt_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, ForgotPasswordActivity.class);
                startActivity(i);
            }
        });

        txt_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, RegistrationActivity.class);
                startActivity(i);
            }
        });

        cb_remember_me.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    saveRememberMe(true);
                    saveLoginDetail(edt_email.getText().toString(), edt_password.getText().toString());

                    Log.e("Remember", "true    :  " + getRemember());
                } else {
                    saveRememberMe(false);
                    Log.e("Remember", "false   :  " + getRemember());
                }
            }
        });

        layout_login_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent it = new Intent(mContext,VerificationActivity.class);
//                startActivity(it);
                getPhoneNumberDialog();
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edt_email.getText().toString().trim())) {
                    Toast.makeText(mContext, "Please enter email", Toast.LENGTH_LONG).show();
                } else if (!BaseActivity.isValidEmail(edt_email.getText().toString())) {
                    Toast.makeText(mContext, "Please enter valid email", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(edt_password.getText().toString().trim())) {
                    Toast.makeText(mContext, "Please enter valid password", Toast.LENGTH_LONG).show();
                } else {
                    if (!edt_email.getText().toString().equalsIgnoreCase(getLoginId()) ||
                            !edt_password.getText().toString().equalsIgnoreCase(getLoginPwd()) && getRemember()) {
                        saveLoginDetail(edt_email.getText().toString(), edt_password.getText().toString());
                    }
                    if (BaseActivity.CheckInternet(mContext)) {
                        sendLoginRequest();
                    } else {
                        Toast.makeText(mContext, getResources().getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void getPhoneNumberDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Lakshya Digital Coaching");

        View viewInflated = LayoutInflater.from(mContext).inflate(R.layout.dialog_login_otp, null, false);

        final EditText input = viewInflated.findViewById(R.id.input);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        builder.setView(viewInflated);

        // Set up the buttons
        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (input.getText().toString().length() == 0) {
                    Toast.makeText(mContext, "Please Enter phone number.", Toast.LENGTH_LONG).show();
                } else {
                    dialog.dismiss();

                    if (BaseActivity.CheckInternet(mContext)) {
                        sendLoginWithOtpRequest(input.getText().toString());
                    } else {
                        Toast.makeText(mContext, getResources().getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();
    }

    private void sendLoginWithOtpRequest(final String phone) {
        try {
            showWaitIndicator(true);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            Log.e("phone", "" + phone);
            Log.e("IMEI_number", "" + IMEI_number);

            Call<User> call = service.sendLoginWithOTPRequest(phone, IMEI_number);

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {

                    showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {
                            Log.e(TAG, "RESPONSE : " + new Gson().toJson(response.body().getData()));

                            if (response.body().getStatus() == 1) {
                                UserData data = response.body().getData();
                                updateUser(data);

                                Intent it = new Intent(mContext, VerificationActivity.class);
                                it.putExtra("PHONE", phone);
                                it.putExtra("USER_ID", data.getIdUser());
                                it.putExtra("TYPE", "login");
                                startActivity(it);
                            }

                            Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    showWaitIndicator(false);
                    Log.d(TAG, "onFailure" + t.toString());
                    Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendLoginRequest() {
        try {
            String email = edt_email.getText().toString();
            String password = edt_password.getText().toString();

            showWaitIndicator(true);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);
            Log.e(TAG, "IMEI_number : " + IMEI_number);

            Call<User> call = service.sendLoginRequest(email, password, isFreshInstall, IMEI_number);

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {

                    showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {
                            Log.e(TAG, "RESPONSE : " + new Gson().toJson(response.body().getData()));

                            if (response.body().getStatus() == 1) {
                                setLogin(1);
                                UserData data = response.body().getData();
                                updateUser(data);

                                //is chkIos checked?
                                if (getRemember() == true) {
                                    saveLoginDetail(edt_email.getText().toString(), edt_password.getText().toString());
                                }

                                startActivity(new Intent(mContext, DashBoardActivity.class));
                                finish();

                                Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    showWaitIndicator(false);
                    Log.d(TAG, "onFailure" + t.toString());
                    Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    IMEI_number = getIMEINumber();
                    Log.e("IMEI_NUMBER", IMEI_number);

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            System.exit(0);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(mContext, "Please click BACK again to exit", Toast.LENGTH_LONG).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}