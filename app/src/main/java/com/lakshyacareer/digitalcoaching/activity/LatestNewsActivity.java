package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.adapters.LatestNewsAdapter;
import com.lakshyacareer.digitalcoaching.adapters.PackagesAdapter;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.LatestNews;
import com.lakshyacareer.digitalcoaching.models.LatestNewsData;
import com.lakshyacareer.digitalcoaching.models.Packages;
import com.lakshyacareer.digitalcoaching.models.PackagesData;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LatestNewsActivity extends BaseActivity {

    LatestNewsActivity mContext;
    RecyclerView rv_latestnews;
    TextView txt_topbar_title, txt_empty_latestnews;
    LinearLayout layout_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_latest_news);

        mContext = this;
        InitViews();
        SetClickListners();

        if (CheckInternet(mContext)) {
            sendLatestNewsRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void InitViews() {

        rv_latestnews = findViewById(R.id.rv_latestnews);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rv_latestnews.setLayoutManager(mLayoutManager);
        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.rv_divider));
        rv_latestnews.addItemDecoration(itemDecorator);

        layout_back = findViewById(R.id.layout_back);
        txt_topbar_title = findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText(getResources().getString(R.string.txt_label_latest_news));
        txt_empty_latestnews = findViewById(R.id.txt_empty_latestnews);
        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));
    }

    private void SetClickListners(){

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rv_latestnews.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_latestnews, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                BaseActivity.LATEST_NEWS_POSITION = position;
                startActivity(new Intent(mContext, DetailNewsActivity.class));

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void sendLatestNewsRequest() {

        showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<LatestNews> call = service.sendLatestNewsRequest();

        call.enqueue(new Callback<LatestNews>() {
            @Override
            public void onResponse(Call<LatestNews> call, Response<LatestNews> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e("LATEST_NEWS", "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));

                        if(response.body().getStatus() == 1){

                            rv_latestnews.setVisibility(View.VISIBLE);
                            txt_empty_latestnews.setVisibility(View.GONE);

                            mLatestNewsList = response.body().getData();
                            Log.e("LATEST_NEWS", "Size of Array : "+mLatestNewsList.size());
                            setAdapter(mLatestNewsList);

                        }else {
                            rv_latestnews.setVisibility(View.GONE);
                            txt_empty_latestnews.setVisibility(View.VISIBLE);
                            txt_empty_latestnews.setText("No Packages Available");
                        }
                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<LatestNews> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void setAdapter(ArrayList<LatestNewsData> mLatestNewsList){

        LatestNewsAdapter mLatestNewsAdapter = new LatestNewsAdapter(mContext, mLatestNewsList);
        rv_latestnews.setAdapter(mLatestNewsAdapter);

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
