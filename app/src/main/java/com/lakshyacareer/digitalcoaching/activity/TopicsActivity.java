package com.lakshyacareer.digitalcoaching.activity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.adapters.TopicsAdapter;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.MaterialsDownloads;
import com.lakshyacareer.digitalcoaching.models.StatusMessages;
import com.lakshyacareer.digitalcoaching.models.Subjects;
import com.lakshyacareer.digitalcoaching.models.Topics;
import com.lakshyacareer.digitalcoaching.models.TopicsData;
import com.lakshyacareer.digitalcoaching.models.VideosDownloads;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.DatabaseHelper;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadManager;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.RetryPolicy;
import com.thin.downloadmanager.ThinDownloadManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TopicsActivity extends BaseActivity {

    private String TAG = "TOPICS";
    TopicsActivity mContext;
    RecyclerView rv_topics;
    TextView txt_topbar_title, txt_empty_topics;
    LinearLayout layout_back;
    public ArrayList<TopicsData> mTopicsList = new ArrayList<>();
    TopicsData mTopicsData;

    Uri mDestinationUri;
    private ThinDownloadManager mThinDownloadManager;
    private static final int DOWNLOAD_THREAD_POOL_SIZE = 4;

    MyDownloadDownloadStatusListenerV1 mMyDownloadStatusListener = new MyDownloadDownloadStatusListenerV1();

    DownloadRequest downloadRequest;
    int downloadId;

    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mNotificationBuilder;
    int id = 1;
    String ChannelId = "my_channel_02";


    public String APP_PATH_SD_CARD ;
    private boolean isVideo = false;


    //    //Code for database
    DatabaseHelper mDbHelper, mDBAdapters;
    static int version_val = 2;
    MaterialsDownloads mMaterialsDownloads;
    VideosDownloads mVideosDownloads;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topics);

        mContext = this;
        InitViews();
        SetClickListners();
        setupForDB();
        if (CheckInternet(mContext)) {
            sendTopicsRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }


    private void InitViews() {

        rv_topics = findViewById(R.id.rv_topics);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rv_topics.setLayoutManager(mLayoutManager);
//        rv_packagelist.addItemDecoration(new DividerItemDecoration(mContext,
//                DividerItemDecoration.VERTICAL));

        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.rv_divider));
        rv_topics.addItemDecoration(itemDecorator);

        layout_back = findViewById(R.id.layout_back);
        txt_topbar_title = findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText(getResources().getString(R.string.txt_label_topics));
        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));
        txt_empty_topics = findViewById(R.id.txt_empty_topics);
    }

    private void SetClickListners() {

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void sendTopicsRequest() {
        try {
            showWaitIndicator(true);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            String id_user = SettingsPreferences.getConsumer(this).getIdUser();
            Call<Topics> call = service.sendTopicsRequest(BaseActivity.SUBJECT_ID, id_user);

            call.enqueue(new Callback<Topics>() {
                @Override
                public void onResponse(Call<Topics> call, Response<Topics> response) {

                    showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {
                            Log.e(TAG, "RESPONSE : " + new Gson().toJson(response.body().getData()));

                            if (response.body().getStatus() == 1) {

                                rv_topics.setVisibility(View.VISIBLE);
                                txt_empty_topics.setVisibility(View.GONE);

                                mTopicsList = response.body().getData();
                                setAdapter();

                            } else {
                                rv_topics.setVisibility(View.GONE);
                                txt_empty_topics.setVisibility(View.VISIBLE);
                                txt_empty_topics.setText("No Subjects Available");
                            }

                        }
                    } catch (Exception e) {
                        showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Topics> call, Throwable t) {
                    showWaitIndicator(false);
                    Log.d(TAG, "onFailure" + t.toString());
                    Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {

        TopicsAdapter mTopicsAdapter = new TopicsAdapter(mContext, mTopicsList, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View v, int position) {

                mTopicsData = mTopicsList.get(position);

                if (v.getId() == R.id.lay_topic_video) {

                    if (CheckInternet(mContext)) {
                        try {
                            mDBAdapters.openDataBase();
                            String mId = mDBAdapters.ValidateVideoID(mTopicsData.getIdMaterial());
                            Log.e("mId", "From_db : "+mId);
                            mDBAdapters.close();

                            if(mId.equalsIgnoreCase("-1")){
                                isVideo = true;
                                APP_PATH_SD_CARD = "/Subjects/Topics/Videos";
                                String mMaterialName = CreateFileName(mTopicsData.getVideoName());
                                InitDownload(mTopicsData.getVideoUrl(), mTopicsData, mMaterialName);
                            }else {
                                Toast.makeText(mContext,"This video is already downloaded",Toast.LENGTH_LONG).show();
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    } else {

                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();

                    }

                } else if (v.getId() == R.id.lay_document_file) {

                    if (CheckInternet(mContext)) {

                        try {
                            mDBAdapters.openDataBase();
                            String mId = mDBAdapters.ValidateMaterialID(mTopicsData.getIdMaterial());
                            Log.e("mId", "From_db : "+mId);
                            mDBAdapters.close();

                            if(mId.equalsIgnoreCase("-1")){
                                isVideo = false;
                                APP_PATH_SD_CARD = "/Subjects/Topics/Materials";
                                String mMaterialName = CreateFileName(mTopicsData.getDocumentName());//+"_"+mTopicsData.getMaterial()
                                InitDownload(mTopicsData.getDocumentLink(), mTopicsData, mMaterialName);
                            }else {
                                Toast.makeText(mContext,"This material is already downloaded",Toast.LENGTH_LONG).show();
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    } else {

                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();

                    }
                } else if (v.getId() == R.id.lay_topic_favorite) {

                    if (CheckInternet(mContext))
                    {


                        switch (mTopicsData.getIsfavourite())
                        {
                            case 0:
                                sendAddToFavouriteRequest(mTopicsData.getIdMaterial());
                                break;
                            case 1:
                                sendRemoveFavouriteRequest(mTopicsData.getIdMaterial());
                                break;
                        }
                    }
                    else
                    {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }

                } else if (v.getId() == R.id.ll_parent_row_topics) {

//                    Toast.makeText(mContext, "Clicked on Parent ", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });
        rv_topics.setAdapter(mTopicsAdapter);
    }

    private void InitDownload(String DownloadURL, TopicsData mTopicsData, String DownloadfileName)
    {
        mThinDownloadManager = new ThinDownloadManager(DOWNLOAD_THREAD_POOL_SIZE);
        RetryPolicy retryPolicy = new DefaultRetryPolicy();

//        File filesDir = getExternalFilesDir("");
        File filesDir = getExternalFilesDir(APP_PATH_SD_CARD);

        String DestinationPath = filesDir+"/"+DownloadfileName;


        Log.e("DestinationPath","DestinationPath    :"+DestinationPath);


        Uri downloadUri = Uri.parse(DownloadURL);
        mDestinationUri = Uri.parse(DestinationPath);
        downloadRequest = new DownloadRequest(downloadUri)
                .setDestinationURI(mDestinationUri).setPriority(DownloadRequest.Priority.LOW)
                .setRetryPolicy(retryPolicy)
                .setDownloadContext("Download")
                .setStatusListener(mMyDownloadStatusListener);


        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel mChannel = new NotificationChannel(ChannelId, "channel_name",NotificationManager.IMPORTANCE_LOW);
            mChannel.setSound(null, null);
            mNotifyManager.createNotificationChannel(mChannel);
        }
//        build = new NotificationCompat.Builder(ctx);
        mNotificationBuilder = new NotificationCompat.Builder(mContext, ChannelId);
        mNotificationBuilder.setContentTitle(""+mTopicsData.getSubjectName())
                .setContentText("Download in progress")
                .setSmallIcon(R.drawable.ic_lakshya_notification);

        Intent mPendingIntent = new Intent(mContext, DashBoardActivity.class);
        mPendingIntent.putExtra("PendingIntent", "PendingIntent");
        PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext, 0,mPendingIntent,0);


        mNotificationBuilder.setContentIntent(resultPendingIntent);
        mNotificationBuilder.setAutoCancel(true);

        if (mThinDownloadManager.query(downloadId) == DownloadManager.STATUS_NOT_FOUND) {
            downloadId = mThinDownloadManager.add(downloadRequest);
        }

        mNotificationBuilder.setContentText("Download Started");
        // Removes the progress bar
        mNotificationBuilder.setProgress(0, 0, false);
        mNotifyManager.notify(id, mNotificationBuilder.build());
    }

    class MyDownloadDownloadStatusListenerV1 implements DownloadStatusListenerV1 {

        @Override
        public void onDownloadComplete(DownloadRequest request) {
            final int id = request.getDownloadId();
            if (id == downloadId) {
//                mProgressTxt.setText(request.getDownloadContext() + "Download Completed");

                mNotificationBuilder.setContentText("Download Completed");
                // Removes the progress bar
                mNotificationBuilder.setProgress(100, 100, false);
                mNotifyManager.notify(id, mNotificationBuilder.build());

//                addDownloadRequest();
                AddToLocalDatabase();
            }
        }

        @Override
        public void onDownloadFailed(DownloadRequest request, int errorCode, String errorMessage) {
            final int id = request.getDownloadId();
            if (id == downloadId) {
//                mProgressTxt.setText("Download Failed");
//                mProgress.setProgress(0);

                mNotificationBuilder.setContentText("Download Failed");
                mNotifyManager.notify(id, mNotificationBuilder.build());

                Log.e("errorMessage", "errorMessage :"+errorMessage);
            }
        }

        @Override
        public void onProgress(DownloadRequest request, long totalBytes, long downloadedBytes, int progress) {
            int id = request.getDownloadId();

            System.out.println("######## onProgress ###### "+id+" : "+totalBytes+" : "+downloadedBytes+" : "+progress);
            if (id == downloadId) {
//                mProgressTxt.setText("Downloading...  "+", "+progress+"%"+"  "+getBytesDownloaded(progress,totalBytes));
//                mProgress.setProgress(progress);

                mNotificationBuilder.setContentText("Downloading... "+progress+"%"+"  "+getBytesDownloaded(progress,totalBytes));
                mNotificationBuilder.setProgress(100, progress, false);
                mNotifyManager.notify(id, mNotificationBuilder.build());

            }
        }

    }

    private void AddToLocalDatabase() {

        if(isVideo){
            try
            {
                mDBAdapters.openDataBase();
                Log.e("destinationUri",mDestinationUri.toString());

                mVideosDownloads.setMaterialId(mTopicsData.getIdMaterial());
                mVideosDownloads.setSubjectName(mTopicsData.getTitle());
                mVideosDownloads.setVideoName(mTopicsData.getVideoName());
                mVideosDownloads.setVideoSize(mTopicsData.getVideoSize());
                mVideosDownloads.setVideoDuraion(mTopicsData.getVideoDuration());
                mVideosDownloads.setVideoPath(mDestinationUri.toString());

                mDBAdapters.InsertVideo(mVideosDownloads);

                mDBAdapters.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }else {

            try
            {
                mDBAdapters.openDataBase();
                Log.e("destinationUri",mDestinationUri.toString());
                mMaterialsDownloads.setMaterialId(mTopicsData.getIdMaterial());
                mMaterialsDownloads.setSubjectName(mTopicsData.getTitle());
                mMaterialsDownloads.setMaterialName(mTopicsData.getDocumentName());
                mMaterialsDownloads.setMaterialSize(mTopicsData.getDocumentSize());
                mMaterialsDownloads.setMaterialPath(mDestinationUri.toString());

                mDBAdapters.InsertMaterial(mMaterialsDownloads);

                mDBAdapters.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        sendAddToDownloadsRequest(mTopicsData.getIdMaterial());

    }

    private String getBytesDownloaded(int progress, long totalBytes) {

        //Greater than 1 MB
        long bytesCompleted = (progress * totalBytes)/100;
        if (totalBytes >= 1000000) {
            return (""+(String.format("%.1f", (float)bytesCompleted/1000000))+ "/"+ ( String.format("%.1f", (float)totalBytes/1000000)) + "MB");
        } if (totalBytes >= 1000) {
            return (""+(String.format("%.1f", (float)bytesCompleted/1000))+ "/"+ ( String.format("%.1f", (float)totalBytes/1000)) + "Kb");

        } else {
            return ( ""+bytesCompleted+"/"+totalBytes );
        }

    }

    public String CreateFileName(String filename){
        return filename.replace(" ", "");
    }


    private void setupForDB()
    {
        mVideosDownloads = new VideosDownloads();
        mMaterialsDownloads = new MaterialsDownloads();

        try {
            mDbHelper = new DatabaseHelper(this, "LakshyCareer.sqlite", null,version_val);
            mDBAdapters = DatabaseHelper.getDBAdapterInstance(this);
        } catch (IOException e) {
            e.printStackTrace();
        }


        try
        {
            mDBAdapters.createDataBase();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void sendAddToFavouriteRequest(String id_material)
    {
        try {
            showWaitIndicator(true);

            String id_user = SettingsPreferences.getConsumer(this).getIdUser();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            Call<StatusMessages> call = service.sendAddToFavouriteRequest(id_user, id_material);

            call.enqueue(new Callback<StatusMessages>() {
                @Override
                public void onResponse(Call<StatusMessages> call, Response<StatusMessages> response) {

                    showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {

                            if(response.body().getStatus() == 1){

                                if (CheckInternet(mContext)) {
                                    sendTopicsRequest();
                                } else {
                                    Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                                }
                                Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            }else {

                                Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            }

                        }
                    } catch (Exception e) {
                        showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<StatusMessages> call, Throwable t) {
                    showWaitIndicator(false);
                    Log.d(TAG,"onFailure"+t.toString());
                    Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void sendRemoveFavouriteRequest(String id_material)
    {
        try {
            showWaitIndicator(true);

            String id_user = SettingsPreferences.getConsumer(this).getIdUser();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            Call<StatusMessages> call = service.sendRemoveFavouriteRequest(id_user, id_material);

            call.enqueue(new Callback<StatusMessages>() {
                @Override
                public void onResponse(Call<StatusMessages> call, Response<StatusMessages> response) {

                    showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {

                            if(response.body().getStatus() == 1){

                                if (CheckInternet(mContext)) {
                                    sendTopicsRequest();
                                } else {
                                    Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                                }
                                Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            }else {

                                Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            }

                        }
                    } catch (Exception e) {
                        showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<StatusMessages> call, Throwable t) {
                    showWaitIndicator(false);
                    Log.d(TAG,"onFailure"+t.toString());
                    Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void sendAddToDownloadsRequest(String id_material)
    {
        try {
            showWaitIndicator(true);

            String id_user = SettingsPreferences.getConsumer(this).getIdUser();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            Call<StatusMessages> call = service.sendAddToDownloadsRequest(id_user, id_material);

            call.enqueue(new Callback<StatusMessages>() {
                @Override
                public void onResponse(Call<StatusMessages> call, Response<StatusMessages> response) {

                    showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {

                            if(response.body().getStatus() == 1){

                                if (CheckInternet(mContext)) {
                                    sendTopicsRequest();
                                } else {
                                    Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                                }
                                Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                Log.e("ADD",""+response.body().getMessage());

                            }else {
                                Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                Log.e("ADD",""+response.body().getMessage());
                            }

                        }
                    } catch (Exception e) {
                        showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<StatusMessages> call, Throwable t) {
                    showWaitIndicator(false);
                    Log.d(TAG,"onFailure"+t.toString());
                    Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


}
