package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.ContactUs;
import com.lakshyacareer.digitalcoaching.models.User;
import com.lakshyacareer.digitalcoaching.models.UserData;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ContactUsActivity extends BaseActivity {

    Context mContext;
    private static final String TAG = "CONTACT_US" ;
    private LinearLayout layout_back;
    private TextView txt_topbar_title;

    private TextInputLayout input_layout_name, input_layout_email, input_layout_mobile, input_layout_city, input_layout_subject, input_layout_message;
    private EditText edit_name, edit_email, edit_mobile, edit_city, edit_subject, edit_message;

    private Button btn_contactus_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_contact_us);

        mContext = this;

        initViews();
        setClickListener();
        getDetails();
    }

    private void initViews(){

        layout_back = (LinearLayout) findViewById(R.id.layout_back);

        txt_topbar_title = (TextView)findViewById(R.id.txt_topbar_title);

        input_layout_name = (TextInputLayout) findViewById(R.id.input_layout_name);
        input_layout_email = (TextInputLayout) findViewById(R.id.input_layout_email);
        input_layout_mobile = (TextInputLayout) findViewById(R.id.input_layout_mobile);
        input_layout_city = (TextInputLayout) findViewById(R.id.input_layout_city);
        input_layout_subject = (TextInputLayout) findViewById(R.id.input_layout_subject);
        input_layout_message = (TextInputLayout) findViewById(R.id.input_layout_message);

        edit_name = (EditText) findViewById(R.id.edit_name);
        edit_email= (EditText) findViewById(R.id.edit_email);
        edit_mobile = (EditText) findViewById(R.id.edit_mobile);
        edit_city = (EditText) findViewById(R.id.edit_city);
        edit_subject = (EditText) findViewById(R.id.edit_subject);
        edit_message = (EditText) findViewById(R.id.edit_message);

        input_layout_name.setTypeface(getGujaratiFonts(mContext));
        input_layout_email.setTypeface(getGujaratiFonts(mContext));
        input_layout_mobile.setTypeface(getGujaratiFonts(mContext));
        input_layout_city.setTypeface(getGujaratiFonts(mContext));
        input_layout_subject.setTypeface(getGujaratiFonts(mContext));
        input_layout_message.setTypeface(getGujaratiFonts(mContext));

        edit_name.setTypeface(getGujaratiFonts(mContext));
        edit_email.setTypeface(getGujaratiFonts(mContext));
        edit_mobile.setTypeface(getGujaratiFonts(mContext));
        edit_city.setTypeface(getGujaratiFonts(mContext));
        edit_subject.setTypeface(getGujaratiFonts(mContext));
        edit_message.setTypeface(getGujaratiFonts(mContext));

        btn_contactus_submit = (Button)findViewById(R.id.btn_contactus_submit);
        btn_contactus_submit.setTypeface(getGujaratiFonts(mContext));

        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));
        txt_topbar_title.setText("Contact Us");
    }

    private void getDetails()
    {

        UserData user = new UserData();

        user = getUserDetails();

        edit_name.setText(user.getFirstName()+" "+user.getLastName());
        edit_email.setText(user.getEmail());
        edit_mobile.setText(user.getMobileNumber());
        edit_city.setText(user.getCity());

        edit_email.setEnabled(false);
    }

    private void setClickListener()
    {
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btn_contactus_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(edit_name.getText().toString().length() == 0)
                {
                    edit_name.setError("Please enter name");
//                    Toast.makeText(ctx,"Please enter first name",Toast.LENGTH_LONG).show();
                }
                else if(edit_email.getText().toString().length() == 0)
                {
                    edit_email.setError("Please enter email");
//                    Toast.makeText(ctx,"Please enter email",Toast.LENGTH_LONG).show();

                }else if (!isValidEmail(edit_email.getText().toString())) {

                    edit_email.setError("Please enter valid email");
//                    Toast.makeText(ctx,"Please enter valid email",Toast.LENGTH_LONG).show();
                }
                else if(edit_mobile.getText().toString().length() == 0)
                {
                    edit_mobile.setError("Please enter mobile number");
//                    Toast.makeText(ctx,"Please enter mobile number",Toast.LENGTH_LONG).show();
                }
                else if(edit_city.getText().toString().length() == 0)
                {
                    edit_city.setError("Please define your city");
//                    Toast.makeText(ctx,"Please define your city",Toast.LENGTH_LONG).show();
                }
                else if(edit_subject.getText().toString().length() == 0)
                {
                    edit_subject.setError("Please enter subject");
//                    Toast.makeText(ctx,"Please enter subject",Toast.LENGTH_LONG).show();
                }
                else if(edit_message.getText().toString().length() == 0)
                {
                    edit_message.setError("Please enter message");
//                    Toast.makeText(ctx,"Please enter message",Toast.LENGTH_LONG).show();
                }
                else
                {
                    if (CheckInternet(mContext)) {
                        sendContactUsRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }


    /*private void sendContactUsRequest() {
        try {
            String name = edit_name.getText().toString();
            String email = edit_email.getText().toString();
            String mobile = edit_mobile.getText().toString();
            String city = edit_city.getText().toString();
            String subject = edit_subject.getText().toString();
            String message = edit_message.getText().toString();

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendContactUsRequest(name,email,mobile,city,subject,message
                    ,new Callback<ContactUs>() {
                        @Override
                        public void success(ContactUs model, Response response) {
                            showWaitIndicator(false);

                            if (response.getStatus() == 200) {
                                try {
                                    Log.e("CONTACT_US_RESPONSE", "" + new Gson().toJson(model));

                                    Toast.makeText(ctx,model.getMessage(),Toast.LENGTH_LONG).show();

                                } catch (Exception e) {

                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void failure(RetrofitError error)
                        {
                            showWaitIndicator(false);

                            Log.e("ERROR", "" + error.getMessage());
                        }
                    });

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }*/

    private void sendContactUsRequest()
    {
        try
        {
            String name = edit_name.getText().toString();
            String email = edit_email.getText().toString();
            String mobile = edit_mobile.getText().toString();
            String city = edit_city.getText().toString();
            String subject = edit_subject.getText().toString();
            String message = edit_message.getText().toString();

            showWaitIndicator(true);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            Call<ContactUs> call = service.sendContactUsRequest(name,email,mobile,city,subject,message);

            call.enqueue(new Callback<ContactUs>() {
                @Override
                public void onResponse(Call<ContactUs> call, Response<ContactUs> response) {

                    showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {
                            Log.e(TAG, "RESPONSE : "+ new Gson().toJson(response.body().getData()));

                            if(response.body().getStatus() == 1)
                            {

                                Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                finish();

                            }
                        }
                    } catch (Exception e) {
                        showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ContactUs> call, Throwable t) {
                    showWaitIndicator(false);
                    Log.d(TAG,"onFailure"+t.toString());
                    Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}