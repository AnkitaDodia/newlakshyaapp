package com.lakshyacareer.digitalcoaching.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.PaymentInfo;
import com.lakshyacareer.digitalcoaching.payumoney.AppEnvironment;
import com.lakshyacareer.digitalcoaching.payumoney.AppPreference;
import com.lakshyacareer.digitalcoaching.payumoney.BaseApplication;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.DatabaseHelper;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;
import com.payumoney.core.PayUmoneyConfig;
import com.payumoney.core.PayUmoneyConstants;
import com.payumoney.core.PayUmoneySdkInitializer;
import com.payumoney.core.entity.TransactionResponse;
import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;
import com.payumoney.sdkui.ui.utils.ResultModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by abc on 03-11-2017.
 */

public class PaymentOptionActivity extends BaseActivity {

    private String TAG = "PAYMENT_FLOW";
    PaymentOptionActivity ctx;
    TextView txt_pay_tag, txt_pay_rupee, txt_pay_tutorials;
    private RadioGroup mRadioGroup;
    private RadioButton rb_pay_payumoney;
    Button btn_payment_submit;

    LinearLayout layout_back;
    TextView txt_topbar_title;


    String TXN_AMOUNT = "00.00";
    String PHONE;
    String PRODUCTINFO = "product_info";
    String FIRSTNAME;
    String EMAIL;
    String mPackageId,mAmount,mDuration;

    public static String mPaymentMode = null;
    public static String PayedDate = null;
    public static String PaymentStatus = null;

    private PayUmoneySdkInitializer.PaymentParam mPaymentParams;

    private AppPreference mAppPreference;

    private SharedPreferences mSPSetting;
    private SharedPreferences.Editor editor;

    DatabaseHelper mDbHelper, mDBAdapters;
    static int version_val = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        ctx = this;

        initView();
        setTypeFace();
        setListners();
        setupForDB();

        mSPSetting = getSharedPreferences("settings", MODE_PRIVATE);

        PHONE = getUserDetails().getMobileNumber();
        EMAIL = getUserDetails().getEmail();
        FIRSTNAME = getUserDetails().getFirstName();
        TXN_AMOUNT = String.valueOf(getIntent().getIntExtra("TOTAL_AMOUNT", 0));
        mPackageId = getIntent().getStringExtra("PACKAGE_ID");
        mAmount = getIntent().getStringExtra("AMOUNT");
        mDuration = getIntent().getStringExtra("DURATION");

        Log.e("mPackageId",""+mPackageId);
        Log.e("mAmount",""+mAmount);
        Log.e("mDuration",""+mDuration);

        mAppPreference = new AppPreference();
        AppPreference.selectedTheme = R.style.AppTheme_Green;

        selectProdEnv();

        if (mSPSetting.getBoolean("is_prod_env", true)) {
            ((BaseApplication) getApplication()).setAppEnvironment(AppEnvironment.PRODUCTION);
//            radio_btn_production.setChecked(true);
        } else {
            ((BaseApplication) getApplication()).setAppEnvironment(AppEnvironment.SANDBOX);
//            radio_btn_sandbox.setChecked(true);
        }
//        setupCitrusConfigs();

//        String.valueOf(getIntent().getIntExtra("TOTAL_AMOUNT", 0));
//        getIntent().getIntExtra("TOTAL_AMOUNT", 0);
        String mPrice = ctx.getResources().getString(R.string.ruppee, String.valueOf(getIntent().getIntExtra("TOTAL_AMOUNT", 0)));
        txt_pay_rupee.setText(mPrice);

    }

    private void setListners() {

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(ctx, CartActivity.class);
                startActivity(mIntent);
                finish();
            }
        });

        btn_payment_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                startActivity(new Intent(ctx, LoginActivity.class));
//                finish();

                launchPayUMoneyFlow();

            }
        });


        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if(rb.getText().toString().equalsIgnoreCase("Card / NetBanking")){


                }else {

                    Toast.makeText(ctx, "Sorry, Currently UPI Payment is not available.", Toast.LENGTH_SHORT).show();
                    rb_pay_payumoney.setChecked(true);

                }
            }
        });
    }

    private void setupForDB()
    {
        try {
            mDbHelper = new DatabaseHelper(ctx, "LakshyCareer.sqlite", null,version_val);
            mDBAdapters = DatabaseHelper.getDBAdapterInstance(ctx);
        } catch (IOException e) {
            e.printStackTrace();
        }


        try
        {
            mDBAdapters.createDataBase();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void initView() {

        layout_back = (LinearLayout) findViewById(R.id.layout_back);

        txt_topbar_title = (TextView) findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText("Payment Option");

        btn_payment_submit = (Button) findViewById(R.id.btn_payment_submit);

        txt_pay_tag = (TextView) findViewById(R.id.txt_pay_tag);
        txt_pay_rupee = (TextView) findViewById(R.id.txt_pay_rupee);
        txt_pay_tutorials = (TextView) findViewById(R.id.txt_pay_tutorials);

        mRadioGroup = (RadioGroup) findViewById(R.id.radio);
        rb_pay_payumoney = findViewById(R.id.rb_pay_payumoney);
        rb_pay_payumoney.setChecked(true);
    }

    private void setTypeFace() {

        txt_topbar_title.setTypeface(getGujaratiFonts(ctx));

        btn_payment_submit.setTypeface(getGujaratiFonts(ctx));

        txt_pay_tag.setTypeface(getGujaratiFonts(ctx));
        txt_pay_rupee.setTypeface(getGujaratiFonts(ctx));
        txt_pay_tutorials.setTypeface(getGujaratiFonts(ctx));

    }


    /**
     * This function sets the mode to PRODUCTION in Shared Preference
     */
    private void selectProdEnv() {

        new Handler(getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                ((BaseApplication) getApplication()).setAppEnvironment(AppEnvironment.PRODUCTION);
                editor = mSPSetting.edit();
                editor.putBoolean("is_prod_env", true);
                editor.apply();

//                if (PayUmoneyFlowManager.isUserLoggedIn(getApplicationContext())) {
//                    logoutBtn.setVisibility(View.VISIBLE);
//                } else {
//                    logoutBtn.setVisibility(View.GONE);
//                }

//                setupCitrusConfigs();
            }
        }, AppPreference.MENU_DELAY);
    }

    /*private void setupCitrusConfigs() {
        AppEnvironment appEnvironment = ((BaseApplication) getApplication()).getAppEnvironment();
        if (appEnvironment == AppEnvironment.PRODUCTION) {
            Toast.makeText(ctx, "Environment Set to Production", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(ctx, "Environment Set to SandBox", Toast.LENGTH_SHORT).show();
        }
    }*/

    /**
     * This function prepares the data for payment and launches payumoney plug n play sdk
     */
    private void launchPayUMoneyFlow() {

        PayUmoneyConfig payUmoneyConfig = PayUmoneyConfig.getInstance();

        //Use this to set your custom text on result screen button
        payUmoneyConfig.setDoneButtonText("Finish");


        //Use this to set your custom title for the activity
        payUmoneyConfig.setPayUmoneyActivityTitle("PayUMoney");

        PayUmoneySdkInitializer.PaymentParam.Builder builder = new PayUmoneySdkInitializer.PaymentParam.Builder();

//        double amount = 0;
//        try {
//            amount = Double.parseDouble(TXN_AMOUNT);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        String txnId = System.currentTimeMillis() + "";
        String phone = PHONE;
        String productName = PRODUCTINFO;
        String firstName = FIRSTNAME;
        String email = EMAIL;
        String udf1 = "";
        String udf2 = "";
        String udf3 = "";
        String udf4 = "";
        String udf5 = "";
        String udf6 = "";
        String udf7 = "";
        String udf8 = "";
        String udf9 = "";
        String udf10 = "";

        AppEnvironment appEnvironment = ((BaseApplication) getApplication()).getAppEnvironment();
        builder.setAmount(TXN_AMOUNT)
                .setTxnId(txnId)
                .setPhone(phone)
                .setProductName(productName)
                .setFirstName(firstName)
                .setEmail(email)
                .setsUrl(appEnvironment.surl())
                .setfUrl(appEnvironment.furl())
                .setUdf1(udf1)
                .setUdf2(udf2)
                .setUdf3(udf3)
                .setUdf4(udf4)
                .setUdf5(udf5)
                .setUdf6(udf6)
                .setUdf7(udf7)
                .setUdf8(udf8)
                .setUdf9(udf9)
                .setUdf10(udf10)
                .setIsDebug(appEnvironment.debug())
                .setKey(appEnvironment.merchant_Key())
                .setMerchantId(appEnvironment.merchant_ID());

        try {
            mPaymentParams = builder.build();

            /*
             * Hash should always be generated from your server side.
             * */
            generateHashFromServer(mPaymentParams);

            /*            *//**
             * Do not use below code when going live
             * Below code is provided to generate hash from sdk.
             * It is recommended to generate hash from server side only.
             * *//*
            mPaymentParams = calculateServerSideHashAndInitiatePayment1(mPaymentParams);

           if (AppPreference.selectedTheme != -1) {
                PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams,MainActivity.this, AppPreference.selectedTheme,mAppPreference.isOverrideResultScreen());
            } else {
                PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams,MainActivity.this, R.style.AppTheme_default, mAppPreference.isOverrideResultScreen());
            }*/

        } catch (Exception e) {
            // some exception occurred
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
//            payNowButton.setEnabled(true);
        }
    }

    /**
     * This method generates hash from server.
     *
     * @param paymentParam payments params used for hash generation
     */
    public void generateHashFromServer(PayUmoneySdkInitializer.PaymentParam paymentParam) {
        //nextButton.setEnabled(false); // lets not allow the user to click the button again and again.

        HashMap<String, String> params = paymentParam.getParams();

        // lets create the post params
        StringBuffer postParamsBuffer = new StringBuffer();
        postParamsBuffer.append(concatParams(PayUmoneyConstants.KEY, params.get(PayUmoneyConstants.KEY)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.AMOUNT, params.get(PayUmoneyConstants.AMOUNT)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.TXNID, params.get(PayUmoneyConstants.TXNID)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.EMAIL, params.get(PayUmoneyConstants.EMAIL)));
        postParamsBuffer.append(concatParams("productinfo", params.get(PayUmoneyConstants.PRODUCT_INFO)));
        postParamsBuffer.append(concatParams("firstname", params.get(PayUmoneyConstants.FIRSTNAME)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF1, params.get(PayUmoneyConstants.UDF1)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF2, params.get(PayUmoneyConstants.UDF2)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF3, params.get(PayUmoneyConstants.UDF3)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF4, params.get(PayUmoneyConstants.UDF4)));
        postParamsBuffer.append(concatParams(PayUmoneyConstants.UDF5, params.get(PayUmoneyConstants.UDF5)));

        String postParams = postParamsBuffer.charAt(postParamsBuffer.length() - 1) == '&' ? postParamsBuffer.substring(0, postParamsBuffer.length() - 1).toString() : postParamsBuffer.toString();

        // lets make an api call
        GetHashesFromServerTask getHashesFromServerTask = new GetHashesFromServerTask();
        getHashesFromServerTask.execute(postParams);
    }

    protected String concatParams(String key, String value) {
        return key + "=" + value + "&";
    }


    /**
     * This AsyncTask generates hash from server.
     */
    private class GetHashesFromServerTask extends AsyncTask<String, String, String> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(PaymentOptionActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... postParams) {

            String merchantHash = "";
            try {
                //TODO Below url is just for testing purpose, merchant needs to replace this with their server side hash generation url
                URL url = new URL("http://api.lakshyacareer.in/ccavenue/generatehashkey");

                String postParam = postParams[0];

                byte[] postParamsByte = postParam.getBytes("UTF-8");

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postParamsByte);

                InputStream responseInputStream = conn.getInputStream();
                StringBuffer responseStringBuffer = new StringBuffer();
                byte[] byteContainer = new byte[1024];
                for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
                    responseStringBuffer.append(new String(byteContainer, 0, i));
                }

                JSONObject response = new JSONObject(responseStringBuffer.toString());

                Iterator<String> payuHashIterator = response.keys();
                while (payuHashIterator.hasNext()) {
                    String key = payuHashIterator.next();
                    switch (key) {
                        /**
                         * This hash is mandatory and needs to be generated from merchant's server side
                         *
                         */
                        case "payment_hash":
                            merchantHash = response.getString(key);
                            break;
                        default:
                            break;
                    }
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return merchantHash;
        }

        @Override
        protected void onPostExecute(String merchantHash) {
            super.onPostExecute(merchantHash);

            progressDialog.dismiss();

            if (merchantHash.isEmpty() || merchantHash.equals("")) {
                Toast.makeText(PaymentOptionActivity.this, "Could not generate hash", Toast.LENGTH_SHORT).show();
            } else {
                mPaymentParams.setMerchantHash(merchantHash);

                if (AppPreference.selectedTheme != -1) {
                    PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, PaymentOptionActivity.this, AppPreference.selectedTheme, mAppPreference.isOverrideResultScreen());
                } else {
                    PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, PaymentOptionActivity.this, R.style.AppTheme_default, mAppPreference.isOverrideResultScreen());
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result Code is -1 send from Payumoney activity
        Log.e("MainActivity", "request code " + requestCode + " resultcode " + resultCode);
        if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data !=
                null) {
            TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager
                    .INTENT_EXTRA_TRANSACTION_RESPONSE);

            ResultModel resultModel = data.getParcelableExtra(PayUmoneyFlowManager.ARG_RESULT);
            Log.e("MainActivity", "resultModel : " + new Gson().toJson(resultModel));

            // Check which object is non-null
            if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
                if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
                    //Success Transaction
                    Log.e("MainActivity","IFFFF");
                    sendPaymentInfoRequest();

                } else {
                    //Failure Transaction
//                    sendPaymentInfoRequest();
                    Log.e("MainActivity","ELSE");
                    Toast.makeText(ctx, "Transaction Failure", Toast.LENGTH_SHORT).show();
                }

                // Response from Payumoney
                String payuResponse = transactionResponse.getPayuResponse();
                Log.e("MainActivity", "payuResponse : " + payuResponse);

                // Response from SURl and FURL
                String merchantResponse = transactionResponse.getTransactionDetails();
                Log.e("MainActivity", "merchantResponse : " + merchantResponse);

//                new AlertDialog.Builder(this)
//                        .setCancelable(false)
//                        .setMessage("Payu's Data : " + payuResponse + "\n\n\n Merchant's Data: " + merchantResponse)
//                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int whichButton) {
//                                dialog.dismiss();
//                            }
//                        }).show();

            } else if (resultModel != null && resultModel.getError() != null) {
                Log.d("PayUmoney", "Error response : " + resultModel.getError().getTransactionResponse());
            } else {
                Log.d("PayUmoney", "Both objects are null!");
            }
        }
    }

    private void sendPaymentInfoRequest() {
        try {
            Log.e("MainActivity","API CALL");
            String userId = SettingsPreferences.getConsumer(ctx).getIdUser();
            PayedDate = GetTodayDate();
            PaymentStatus = "1";
            mPaymentMode = "1";

            showWaitIndicator(true);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            Call<PaymentInfo> call = service.sendPaymentInfoRequest(userId, PaymentStatus,
                    PayedDate, mPaymentMode,mAmount,mPackageId,mDuration);

            call.enqueue(new Callback<PaymentInfo>() {
                @Override
                public void onResponse(Call<PaymentInfo> call, Response<PaymentInfo> response) {

                    showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {
                            Log.e(TAG, "RESPONSE : " + new Gson().toJson(response.body().getData()));

                            if (response.body().getStatus() == 1) {
                                Toast.makeText(ctx, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                                try {
                                    mDbHelper.openDataBase();
                                    mDbHelper.deletePackage();
                                    mDbHelper.close();
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }

                                Log.e("MainActivity","Execute");
                                startActivity(new Intent(PaymentOptionActivity.this,DashBoardActivity.class));
                            }
                        }
                    } catch (Exception e) {
                        showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<PaymentInfo> call, Throwable t) {
                    showWaitIndicator(false);
                    Log.d(TAG, "onFailure" + t.toString());
                    Toast.makeText(ctx, " " + ctx.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String GetTodayDate() {

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());

        return formattedDate;
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}