package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.LatestNewsData;

public class DetailNewsActivity extends BaseActivity {

    Context mContext;
    TextView txt_topbar_title, txt_news_title, txt_news_descriptions;
    LinearLayout layout_back, ll_previous, ll_next;
    ImageView img_details_news;
    ProgressBar prgs_latest_news;
    private int mItemPosition = 0, mPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_news);

        mContext = this;
        InitViews();
        SetClickListners();
        mItemPosition = BaseActivity.LATEST_NEWS_POSITION;
        mPosition = BaseActivity.LATEST_NEWS_POSITION;
        LoadLatestNewsData(mItemPosition);

        NextPrevVisibility();
    }

    private void LoadLatestNewsData(int position) {

        LatestNewsData mLatestNewsData = mLatestNewsList.get(position);

        Glide.with(mContext).load(mLatestNewsData.getImage())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        prgs_latest_news.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                            holder.img_vendor_services.setImageDrawable(resource);
                        prgs_latest_news.setVisibility(View.GONE);
                        return false;
                    }
                }).into(img_details_news);


        txt_news_title.setText(mLatestNewsData.getTitle());
        txt_news_descriptions.setText(mLatestNewsData.getDescription());

    }

    private void InitViews() {

        layout_back = findViewById(R.id.layout_back);
        txt_topbar_title = findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText(getResources().getString(R.string.txt_label_latest_news));
        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));

        txt_news_title = findViewById(R.id.txt_news_title);
        txt_news_descriptions = findViewById(R.id.txt_news_descriptions);
        img_details_news = findViewById(R.id.img_details_news);
        prgs_latest_news = findViewById(R.id.prgs_latest_news);

        ll_previous = findViewById(R.id.ll_previous);
        ll_next = findViewById(R.id.ll_next);

        txt_news_title.setTypeface(getGujaratiFonts(mContext));
        txt_news_descriptions.setTypeface(getGujaratiFonts(mContext));


    }

    private void SetClickListners(){

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ll_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mPosition = mItemPosition +1;
                mItemPosition = mItemPosition + 1;
                if(mItemPosition<mLatestNewsList.size()){
                    LoadLatestNewsData(mItemPosition);
                }
                else{
                    mItemPosition = mItemPosition-1;
                }

                NextPrevVisibility();
            }
        });

        ll_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mPosition = mItemPosition -1;
                mItemPosition = mItemPosition - 1;

                if(mItemPosition>=0){
                    LoadLatestNewsData(mItemPosition);
                }
                else{
                    mItemPosition=mItemPosition+1;
                }

                NextPrevVisibility();
            }
        });

    }

    private void NextPrevVisibility(){

        Log.e("CheckNextLogic", "mItemPosition : "+mPosition);
        Log.e("CheckNextLogic", "mLatestNewsList.size() : "+mLatestNewsList.size());

        if(mPosition == mLatestNewsList.size() -1){
            ll_next.setVisibility(View.INVISIBLE);
            ll_previous.setVisibility(View.VISIBLE);
        }else if(mPosition == 0){
            ll_previous.setVisibility(View.INVISIBLE);
            ll_next.setVisibility(View.VISIBLE);
        }else {
            ll_next.setVisibility(View.VISIBLE);
            ll_previous.setVisibility(View.VISIBLE);
        }

    }
}
