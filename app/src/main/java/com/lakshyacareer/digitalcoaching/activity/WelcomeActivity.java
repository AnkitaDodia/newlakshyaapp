package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;

public class WelcomeActivity extends BaseActivity {

    private static int SPLASH_TIME_OUT = 3000;
    Context mContext;
    ProgressBar mPrg_splash;
    RelativeLayout ll_parent_welcome;
    TextView txt_developername, txt_developerby, txt_app_title, txt_way_to_tag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        mContext = this;
        InitView();

        mPrg_splash.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getLogin() == 1) {
                    Intent i = new Intent(mContext, DashBoardActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(mContext, SampleIntroActivity.class);//SampleIntroActivity
                    startActivity(i);
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    private void InitView() {
        ll_parent_welcome = findViewById(R.id.ll_parent_welcome);
        txt_developerby = (TextView) findViewById(R.id.txt_developerby);
        txt_developername = (TextView) findViewById(R.id.txt_developername);
        txt_way_to_tag = (TextView) findViewById(R.id.txt_way_to_tag);
        txt_app_title = (TextView) findViewById(R.id.txt_app_title);

        mPrg_splash = (ProgressBar) findViewById(R.id.progressBar);

        overrideFonts(ll_parent_welcome, mContext);
    }
}
