package com.lakshyacareer.digitalcoaching.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.listener.SmsListener;
import com.lakshyacareer.digitalcoaching.models.ResendOTP;
import com.lakshyacareer.digitalcoaching.models.StatusMessages;
import com.lakshyacareer.digitalcoaching.models.User;
import com.lakshyacareer.digitalcoaching.models.UserData;
import com.lakshyacareer.digitalcoaching.receiver.SMSBroadcastReceiver;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class VerificationActivity extends BaseActivity {
    private static final String TAG = "VERIFICATION";
    VerificationActivity mContext;

    private String MobileNumber, userId, mtype;

    Button btn_verify, btn_resend_otp;

    TextView txt_mobile_numer;

    EditText edit_verification_code;

    TextView txt_topbar_title;

    LinearLayout ll_parent_verification;

    Button btn_back;

    private SmsRetrieverClient mSmsRetrieverClient;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_verify_mobile);

        mContext = this;

        MobileNumber = getIntent().getStringExtra("PHONE");
        userId = getIntent().getStringExtra("USER_ID");
        mtype = getIntent().getStringExtra("TYPE");

        mSmsRetrieverClient = SmsRetriever.getClient(this);
        startVerify();

        InitViews();
        setClickListener();

        SMSBroadcastReceiver.bindListener(new SmsListener() {
            @Override
            public void onMessageReceived(String messageText) {
                edit_verification_code.setText(messageText);
                Log.e(TAG, "messageText : " + messageText);
            }
        });
    }

    private void InitViews() {
        ll_parent_verification = findViewById(R.id.ll_parent_verification);
        overrideFonts(ll_parent_verification, mContext);
        btn_back = findViewById(R.id.btn_back);
        btn_back.setVisibility(View.GONE);

        txt_topbar_title = findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText(R.string.otp_verify);
        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));

        btn_resend_otp = findViewById(R.id.btn_resend_otp);
        btn_verify = findViewById(R.id.btn_verify);

        txt_mobile_numer = findViewById(R.id.txt_mobile_numer);

        edit_verification_code = findViewById(R.id.edit_verification_code);

        try {
            String mask = MobileNumber.replaceAll("\\w(?=\\w{4})", "*");
            txt_mobile_numer.setText(mask);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setClickListener() {
        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_verification_code.getText().toString().length() == 0) {
                    Toast.makeText(mContext, "Enter OTP code.", Toast.LENGTH_LONG).show();
                } else {
                    if (mtype.equalsIgnoreCase("forgot")) {
                        if (BaseActivity.CheckInternet(mContext)) {
                            sendForgotMatchOTPRequest(edit_verification_code.getText().toString());
                        } else {
                            Toast.makeText(mContext, getResources().getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (BaseActivity.CheckInternet(mContext)) {
                            sendMatchOTPRequest(edit_verification_code.getText().toString());
                        } else {
                            Toast.makeText(mContext, getResources().getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }


                }
            }
        });

        btn_resend_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (BaseActivity.CheckInternet(mContext)) {
                    sendResendOTPRequest();
                } else {
                    Toast.makeText(mContext, getResources().getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void startVerify() {
        // Make this a foreground service

        // Start SMS receiver code
        Task<Void> task = mSmsRetrieverClient.startSmsRetriever();
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.e(TAG, "SmsRetrievalResult start onSuccess.");
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "SmsRetrievalResult start failed.", e);
            }
        });
    }

    private void sendMatchOTPRequest(String code) {
        try {
//            String userId = SettingsPreferences.getConsumer(mContext).getIdUser();
            String islogin = "1";

            showWaitIndicator(true);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            Call<User> call = service.sendMatchOTPRequest(userId, code, islogin);

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {

                    showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {
                            Log.e(TAG, "RESPONSE : " + new Gson().toJson(response.body().getData()));

                            if (response.body().getStatus() == 1) {
                                setLogin(1);
                                UserData data = response.body().getData();
                                updateUser(data);

                                if (mtype.equalsIgnoreCase("signup")) {
                                    showSubscriptionDialog();
                                } else {
                                    startActivity(new Intent(mContext, DashBoardActivity.class));
                                    finish();
                                }

                                Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    showWaitIndicator(false);
                    Log.d(TAG, "onFailure" + t.toString());
                    Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendResendOTPRequest() {
        try {
            showWaitIndicator(true);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            Call<ResendOTP> call = service.sendResendOTPRequest(MobileNumber, mtype);

            call.enqueue(new Callback<ResendOTP>() {
                @Override
                public void onResponse(Call<ResendOTP> call, Response<ResendOTP> response) {

                    showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {

                            if (response.body().getStatus() == 1) {

                                Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResendOTP> call, Throwable t) {
                    showWaitIndicator(false);
                    Log.d(TAG, "onFailure" + t.toString());
                    Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendForgotMatchOTPRequest(String code) {
        try {

            showWaitIndicator(true);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            Call<StatusMessages> call = service.sendForgotMatchOTPRequest(MobileNumber, code);

            call.enqueue(new Callback<StatusMessages>() {
                @Override
                public void onResponse(Call<StatusMessages> call, Response<StatusMessages> response) {

                    showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {

                            if (response.body().getStatus() == 1) {


                                Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(mContext, LoginActivity.class));
                                finish();

                            } else {
                                Toast.makeText(mContext, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<StatusMessages> call, Throwable t) {
                    showWaitIndicator(false);
                    Log.d(TAG, "onFailure" + t.toString());
                    Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showSubscriptionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage("Choose your package now and start learning.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        BaseActivity.isFromSubject = 0;
                        startActivity(new Intent(mContext, SubscribeActivity.class));
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                        setLogin(1);
                        startActivity(new Intent(mContext, DashBoardActivity.class));
                        finish();
                    }
                })
                .show();
    }
}
