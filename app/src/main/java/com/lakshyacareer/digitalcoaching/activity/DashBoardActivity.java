package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.fragment.HomeFragment;
import com.lakshyacareer.digitalcoaching.fragment.MyAccountFragment;
import com.lakshyacareer.digitalcoaching.fragment.MyDownloadsFragment;
import com.lakshyacareer.digitalcoaching.fragment.SettingsFragment;
import com.lakshyacareer.digitalcoaching.fragment.WishListFragment;

public class DashBoardActivity extends BaseActivity {

    Context mContext;
    LinearLayout ll_parent_dashboard;
    Fragment currentFragment;
    BottomNavigationView mBottomNavView ;
    TextView txt_topbar_title;
    Button btn_back;

    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        mContext = this;
        InitViews();
        SetClickListner();

        String IntentTag = getIntent().getStringExtra("PendingIntent");
        Log.e("TESTAPP", "IntentTag :"+IntentTag);

        if(IntentTag != null){
            if(IntentTag.equalsIgnoreCase("PendingIntent")){
                if(getLogin() == 0){
                    startActivity(new Intent(mContext, LoginActivity.class));
                    finish();
                }
            }
        }


        changeFragment(new HomeFragment());
        txt_topbar_title.setText(getResources().getString(R.string.title_nav_home));
    }

    private void InitViews() {

        ll_parent_dashboard = findViewById(R.id.ll_parent_dashboard);
        txt_topbar_title = findViewById(R.id.txt_topbar_title);

        btn_back = findViewById(R.id.btn_back);
        btn_back.setVisibility(View.GONE);

        mBottomNavView = findViewById(R.id.bottom_navigation);
        overrideFonts(ll_parent_dashboard, mContext);
    }

    private void SetClickListner(){

        mBottomNavView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.navigation_home:

                                txt_topbar_title.setText(getResources().getString(R.string.title_nav_home));
                                changeFragment(new HomeFragment());
                                break;
                            case R.id.navigation_downloads:
                                txt_topbar_title.setText(getResources().getString(R.string.title_nav_downloads));
                                changeFragment(new MyDownloadsFragment());
                                break;
                            case R.id.navigation_wishlist:
                                txt_topbar_title.setText(getResources().getString(R.string.title_nav_wishlist));
                                changeFragment(new WishListFragment());
                                break;
                            case R.id.navigation_account:

                                txt_topbar_title.setText(getResources().getString(R.string.title_nav_account));
                                changeFragment(new MyAccountFragment());
                                break;
                            case R.id.navigation_settings:

                                txt_topbar_title.setText(getResources().getString(R.string.title_nav_settings));
                                changeFragment(new SettingsFragment());
                                break;
                        }
                        return true;
                    }
                });
    }


    public void changeFragment(Fragment targetFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, targetFragment, targetFragment.getClass().getName());
        transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.addToBackStack(targetFragment.getClass().getName());
        transaction.commitAllowingStateLoss();

        currentFragment = targetFragment;
    }

    @Override
    public void onBackPressed() {

        performBackTransaction();
    }

    public void performBackTransaction() {
        FragmentManager fm = getSupportFragmentManager();

        try {
            if (fm.getBackStackEntryCount() > 1) {
//                fm.popBackStackImmediate();

                String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
                Log.e("MainActivity", "Current fragment tag is: " + tag);

                findTheFragmentWhilePoping(tag);
            } else {
                Log.i("MainActivity", "Nothing in back stack call super");
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();

                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    System.exit(0);
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(mContext, "Please click BACK again to exit", Toast.LENGTH_LONG).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findTheFragmentWhilePoping(String tag) {
        FragmentManager fm = getSupportFragmentManager();

        if (tag.equalsIgnoreCase(HomeFragment.class.getName())) {

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                finish();
                System.exit(0);
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(mContext, "Please click BACK again to exit", Toast.LENGTH_LONG).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);

        }else if (tag.equalsIgnoreCase(MyDownloadsFragment.class.getName())) {

            mBottomNavView.setSelectedItemId(R.id.navigation_home);
            txt_topbar_title.setText(getResources().getString(R.string.title_nav_home));
            currentFragment = new HomeFragment();

        }else if (tag.equalsIgnoreCase(WishListFragment.class.getName())) {
            mBottomNavView.setSelectedItemId(R.id.navigation_home);
            txt_topbar_title.setText(getResources().getString(R.string.title_nav_home));
            currentFragment = new HomeFragment();

        }else if (tag.equalsIgnoreCase(MyAccountFragment.class.getName())) {
            mBottomNavView.setSelectedItemId(R.id.navigation_home);
            txt_topbar_title.setText(getResources().getString(R.string.title_nav_home));
            currentFragment = new HomeFragment();

        }else if (tag.equalsIgnoreCase(SettingsFragment.class.getName())) {
            mBottomNavView.setSelectedItemId(R.id.navigation_home);
            txt_topbar_title.setText(getResources().getString(R.string.title_nav_home));
            currentFragment = new HomeFragment();

        }

        changeFragment(currentFragment);

    }

}
