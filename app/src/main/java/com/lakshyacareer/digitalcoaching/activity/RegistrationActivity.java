package com.lakshyacareer.digitalcoaching.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.User;
import com.lakshyacareer.digitalcoaching.models.UserData;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegistrationActivity extends BaseActivity {

    private static final String TAG = "REGISTRATION";
    RegistrationActivity mContext;
    LinearLayout ll_parent_registration,layout_back;
    Button btn_subscribe;
    Spinner spinner_state;
    EditText edit_first_name_reg, edit_last_name_reg, edit_dob_reg, edit_email_reg, edit_mobile_reg, edit_pass_reg, edit_confirm_pass_reg,
            edit_city_reg;
    TextView txt_topbar_title;

    int selectedItem = 5;

    String StateName,IMEI_number;
    public static final int REQUEST_READ_PHONE_STATE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        mContext = this;
        InitViews();
        SetClickListners();
        setSpinnerAdapter();

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
        } else {
            //TODO
            IMEI_number = getIMEINumber();
//            IMEI_number = Static_IMEI;
            Log.e("IMEI_NUMBER" , IMEI_number);
        }
    }

    private void InitViews() {

        ll_parent_registration = findViewById(R.id.ll_parent_registration);
        layout_back = findViewById(R.id.layout_back);

        txt_topbar_title = findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText(R.string.registration_title);
        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));

        edit_first_name_reg = findViewById(R.id.edit_first_name_reg);
        edit_last_name_reg = findViewById(R.id.edit_last_name_reg);
        edit_email_reg = findViewById(R.id.edit_email_reg);
        edit_mobile_reg = findViewById(R.id.edit_mobile_reg);
        edit_pass_reg = findViewById(R.id.edit_pass_reg);
        edit_confirm_pass_reg = findViewById(R.id.edit_confirm_pass_reg);
        edit_city_reg = findViewById(R.id.edit_city_reg);
        edit_dob_reg = findViewById(R.id.edit_dob_reg);

        edit_dob_reg.addTextChangedListener(mTextWatcher);

        spinner_state = findViewById(R.id.spinner_state);

        btn_subscribe = findViewById(R.id.btn_subscribe);

        overrideFonts(ll_parent_registration, mContext);
    }

    private void SetClickListners() {

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        layout_back.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                View view = mContext.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                return false;
            }
        });

        ll_parent_registration.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                View view = mContext.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                return false;
            }
        });

        spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                StateName = adapterView.getItemAtPosition(i).toString();
                selectedItem = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String password = edit_pass_reg.getText().toString();
                String confirmPass = edit_confirm_pass_reg.getText().toString();


                if (edit_first_name_reg.getText().toString().length() == 0) {
                    edit_first_name_reg.setError("Please enter first name");
                } else if (edit_last_name_reg.getText().toString().length() == 0) {
                    edit_last_name_reg.setError("Please enter last name");
                } else if (edit_dob_reg.getText().toString().length() == 0) {
                    edit_dob_reg.setError("Please enter Date of Birth");
                } else if (edit_email_reg.getText().toString().length() == 0) {
                    edit_email_reg.setError("Please enter email");
                } else if (!isValidEmail(edit_email_reg.getText().toString())) {
                    edit_email_reg.setError("Please enter valid email");
                } else if (edit_mobile_reg.getText().toString().length() == 0) {
                    edit_mobile_reg.setError("Please enter mobile number");
                } else if (edit_mobile_reg.getText().toString().length() < 10) {
                    edit_mobile_reg.setError("Please enter valid mobile number");
                } else if (!isAlphaNumeric(password)) {
                    edit_pass_reg.setError("Please enter at alphanumeric password");
                } else if (password.length() == 0) {
                    edit_pass_reg.setError("Please enter valid password");
                } else if (password.length() < 6) {
                    edit_pass_reg.setError("Please enter at least six character password");
                } else if (confirmPass.length() == 0) {
                    edit_confirm_pass_reg.setError("Your password doesn't match");
                } else if (!confirmPass.equalsIgnoreCase(password)) {
                    edit_confirm_pass_reg.setError("Your password dose't match ");
                } else if (edit_city_reg.getText().toString().length() == 0) {
                    edit_city_reg.setError("Please define your city");
                } else {
                    if (BaseActivity.CheckInternet(mContext)) {
                        sendRegistrationRequest();
                    } else {
                        Toast.makeText(mContext, getResources().getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    TextWatcher mTextWatcher = new TextWatcher() {

        private String current = "";
        private String ddmmyyyy = "DDMMYYYY";
        private Calendar cal = Calendar.getInstance();

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!s.toString().equals(current)) {
                String clean = s.toString().replaceAll("[^\\d.]|\\.", "");
                String cleanC = current.replaceAll("[^\\d.]|\\.", "");

                int cl = clean.length();
                int sel = cl;
                for (int i = 2; i <= cl && i < 6; i += 2) {
                    sel++;
                }
                //Fix for pressing delete next to a forward slash
                if (clean.equals(cleanC)) sel--;

                if (clean.length() < 8) {
                    clean = clean + ddmmyyyy.substring(clean.length());
                } else {
                    //This part makes sure that when we finish entering numbers
                    //the date is correct, fixing it otherwise
                    int day = Integer.parseInt(clean.substring(0, 2));
                    int mon = Integer.parseInt(clean.substring(2, 4));
                    int year = Integer.parseInt(clean.substring(4, 8));

                    mon = mon < 1 ? 1 : mon > 12 ? 12 : mon;
                    cal.set(Calendar.MONTH, mon - 1);
                    year = (year < 1900) ? 1900 : (year > 2100) ? 2100 : year;
                    cal.set(Calendar.YEAR, year);
                    // ^ first set year for the line below to work correctly
                    //with leap years - otherwise, date e.g. 29/02/2012
                    //would be automatically corrected to 28/02/2012

                    day = (day > cal.getActualMaximum(Calendar.DATE)) ? cal.getActualMaximum(Calendar.DATE) : day;
                    clean = String.format("%02d%02d%02d", day, mon, year);
                }

                clean = String.format("%s-%s-%s", clean.substring(0, 2),
                        clean.substring(2, 4),
                        clean.substring(4, 8));

                sel = sel < 0 ? 0 : sel;
                current = clean;
                edit_dob_reg.setText(current);
                edit_dob_reg.setSelection(sel < current.length() ? sel : current.length());
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private void setSpinnerAdapter()
    {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, states) {

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent)
            {
                View v = null;
                v = super.getDropDownView(position, null, parent);
                // If this is the selected item position
                if (position == selectedItem) {
                    v.setBackgroundColor(Color.LTGRAY);
                }
                else {
                    // for other views
                    v.setBackgroundColor(Color.WHITE);

                }
                return v;
            }
        };

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        // attaching data adapter to spinner
        spinner_state.setAdapter(dataAdapter);
        spinner_state.setSelection(5,true);
    }

    private void sendRegistrationRequest()
    {
        try {
            String fName = edit_first_name_reg.getText().toString();
            String lName = edit_last_name_reg.getText().toString();
            String dob = edit_dob_reg.getText().toString();
            String city = edit_city_reg.getText().toString();
            StateName = spinner_state.getSelectedItem().toString();
            String email = edit_email_reg.getText().toString();
            String password = edit_pass_reg.getText().toString();
            String confirmPass = edit_confirm_pass_reg.getText().toString();
            final String mobile = edit_mobile_reg.getText().toString();

            showWaitIndicator(true);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            Call<User> call = service.sendRegisterRequest(fName, lName, dob, email, mobile, password, confirmPass, StateName, city, IMEI_number);
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {

                    showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {
                            Log.e(TAG, "RESPONSE : "+ new Gson().toJson(response.body().getData()));

                            if(response.body().getStatus() == 1)
                            {
                                UserData data =  response.body().getData();
                                updateUser(data);

                                Intent it = new Intent(mContext,VerificationActivity.class);
                                it.putExtra("PHONE",mobile);
                                it.putExtra("USER_ID",data.getIdUser());
                                it.putExtra("TYPE","signup");
                                startActivity(it);
                            }

                            Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    showWaitIndicator(false);
                    Log.d(TAG,"onFailure"+t.toString());
                    Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}