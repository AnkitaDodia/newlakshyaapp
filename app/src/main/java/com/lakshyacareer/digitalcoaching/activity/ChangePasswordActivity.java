package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.ChangePassword;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by abc on 02-11-2017.
 */

public class ChangePasswordActivity extends BaseActivity {

    private static final String TAG = "CHANGE_PASSWORD";
    Context mContext;

    LinearLayout layout_back;
    TextView txt_topbar_title;

    TextInputLayout input_current_pass, input_new_password, input_confirm_new_pass;
    EditText edit_current_pass, edit_new_pass,edit_confirm_new_pass;

    Button btn_change_password;


    String id_user;
    String password, new_password, confirm_new_password ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        mContext = this;


        getView();
        setTypeFace();
        setClickListener();


    }

    private void getView()
    {
        layout_back = (LinearLayout) findViewById(R.id.layout_back);
        txt_topbar_title = (TextView)findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText("Change Password");

        btn_change_password = (Button)findViewById(R.id.btn_change_password);

        input_current_pass = (TextInputLayout) findViewById(R.id.input_current_pass);
        input_new_password = (TextInputLayout) findViewById(R.id.input_new_password);
        input_confirm_new_pass = (TextInputLayout) findViewById(R.id.input_confirm_new_pass);

        edit_current_pass = (EditText) findViewById(R.id.edit_current_pass);
        edit_new_pass = (EditText) findViewById(R.id.edit_new_pass);
        edit_confirm_new_pass = (EditText) findViewById(R.id.edit_confirm_new_pass);

    }

    private void setTypeFace()
    {
        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));

        btn_change_password.setTypeface(getGujaratiFonts(mContext));

        input_current_pass.setTypeface(getGujaratiFonts(mContext));
        input_new_password.setTypeface(getGujaratiFonts(mContext));
        input_confirm_new_pass.setTypeface(getGujaratiFonts(mContext));

        edit_current_pass.setTypeface(getGujaratiFonts(mContext));
        edit_new_pass.setTypeface(getGujaratiFonts(mContext));
        edit_confirm_new_pass.setTypeface(getGujaratiFonts(mContext));


    }

    private void setClickListener() {

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        btn_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                password = edit_current_pass.getText().toString();
                new_password = edit_new_pass.getText().toString();
                confirm_new_password = edit_confirm_new_pass.getText().toString();

                if (edit_current_pass.getText().toString().length() == 0) {
                    edit_current_pass.setError("Please enter password");
//                    Toast.makeText(ctx,"Please enter first name",Toast.LENGTH_LONG).show();
                }
//                else if (!getLoginPwd().equals(password)) {
//                    edit_current_pass.setError("Enter Correct Password");
////                    Toast.makeText(ctx,"P" +getLoginPwd(),Toast.LENGTH_LONG).show();
//                }

                else if (edit_new_pass.getText().toString().length() == 0) {
                    edit_new_pass.setError("Please enter password");
//                    Toast.makeText(ctx,"Please enter first name",Toast.LENGTH_LONG).show();
                }else if (!isAlphaNumeric(new_password)) {
                    edit_new_pass.setError("Please enter at alphanumeric password");
//                    Toast.makeText(ctx,"Please enter at alphanumeric password",Toast.LENGTH_LONG).show();
                } else if (new_password.length() < 6) {
                    edit_new_pass.setError("Please enter at least six character password");
//                    Toast.makeText(ctx,"Please enter at least six character password",Toast.LENGTH_LONG).show();
                }

//                else if (edit_confirm_new_pass.getText().toString().length() == 0) {
//                    edit_confirm_new_pass.setError("Your password dose't match");
////                    Toast.makeText(ctx,"Please enter first name",Toast.LENGTH_LONG).show();
//                }
                else if (!confirm_new_password.equalsIgnoreCase(new_password)) {
                    edit_confirm_new_pass.setError("Your password dose't match");
//                    Toast.makeText(ctx,"Please enter at alphanumeric password",Toast.LENGTH_LONG).show();
                }
                else {
                    if (BaseActivity.CheckInternet(mContext)) {
                        sendChangePasswordRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }


    private void sendChangePasswordRequest()
    {
        try {
            id_user = SettingsPreferences.getConsumer(this).getIdUser();

            showWaitIndicator(true);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            Call<ChangePassword> call = service.sendChangePasswordRequest(id_user, password, new_password, confirm_new_password);
            call.enqueue(new Callback<ChangePassword>() {
                @Override
                public void onResponse(Call<ChangePassword> call, Response<ChangePassword> response) {

                    showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {
                            Log.e(TAG, "RESPONSE : "+ new Gson().toJson(response.body().getData()));

                            if(response.body().getStatus() == 1)
                            {
                                saveLoginPwd(new_password);

                                Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                finish();
                            }else {
                                Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                    } catch (Exception e) {
                        showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ChangePassword> call, Throwable t) {
                    showWaitIndicator(false);
                    Log.d(TAG,"onFailure"+t.toString());
                    Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    @Override
    public void onBackPressed() {
        finish();
    }
}