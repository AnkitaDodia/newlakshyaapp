package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.adapters.CurrentAffairsAdapter;
import com.lakshyacareer.digitalcoaching.adapters.LatestNewsAdapter;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.CurrentAffairs;
import com.lakshyacareer.digitalcoaching.models.CurrentAffairsData;
import com.lakshyacareer.digitalcoaching.models.LatestNews;
import com.lakshyacareer.digitalcoaching.models.LatestNewsData;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CurrentAffairActivity extends BaseActivity {

    CurrentAffairActivity mContext;
    RecyclerView rv_current_affairs;
    LinearLayout layout_back;
    TextView txt_topbar_title, txt_empty_current_affairs;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_affair);

        mContext = this;
        InitViews();
        SetClickListners();

        if (CheckInternet(mContext)) {
            sendCurrentAffairsRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void InitViews() {

        rv_current_affairs = findViewById(R.id.rv_current_affairs);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rv_current_affairs.setLayoutManager(mLayoutManager);
        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.rv_divider));
        rv_current_affairs.addItemDecoration(itemDecorator);

        layout_back = findViewById(R.id.layout_back);
        txt_topbar_title = findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText(getResources().getString(R.string.txt_label_current_affair));
        txt_empty_current_affairs = findViewById(R.id.txt_empty_current_affairs);
        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));
    }

    private void SetClickListners(){

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rv_current_affairs.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_current_affairs, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                BaseActivity.CURRENT_AFFAIRS_POSITION = position;
                startActivity(new Intent(mContext, DetailCurrentAffairsActivity.class));

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void sendCurrentAffairsRequest() {

        showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<CurrentAffairs> call = service.sendCurrentAffairsRequest();

        call.enqueue(new Callback<CurrentAffairs>() {
            @Override
            public void onResponse(Call<CurrentAffairs> call, Response<CurrentAffairs> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e("CURRENT_AFFAIRS", "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));

                        if(response.body().getStatus() == 1){

                            rv_current_affairs.setVisibility(View.VISIBLE);
                            txt_empty_current_affairs.setVisibility(View.GONE);

                            mCurrentAffairsDataList = response.body().getData();
                            Log.e("CURRENT_AFFAIRS", "Size of Array : "+mCurrentAffairsDataList.size());
                            setAdapter(mCurrentAffairsDataList);

                        }else {
                            rv_current_affairs.setVisibility(View.GONE);
                            txt_empty_current_affairs.setVisibility(View.VISIBLE);
                            txt_empty_current_affairs.setText("No Packages Available");
                        }
                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CurrentAffairs> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void setAdapter(ArrayList<CurrentAffairsData> mCurrentAffairsDataList){

        CurrentAffairsAdapter mCurrentAffairsAdapter = new CurrentAffairsAdapter(mContext, mCurrentAffairsDataList);
        rv_current_affairs.setAdapter(mCurrentAffairsAdapter);

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
