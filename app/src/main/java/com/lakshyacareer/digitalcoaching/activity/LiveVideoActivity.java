package com.lakshyacareer.digitalcoaching.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.adapters.YTPlayListAdapter;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.YTAllPlayList;
import com.lakshyacareer.digitalcoaching.models.YTPlaylist;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LiveVideoActivity extends BaseActivity {

    LiveVideoActivity mContext;
    RecyclerView rv_livevideo;
    TextView txt_topbar_title, txt_empty_livevideo;
    LinearLayout layout_back;

    public static ArrayList<YTAllPlayList> mYTAllPlayListData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_video);

        mContext = this;
        InitViews();
        SetClickListners();

        if (CheckInternet(mContext)) {
            GetYTPlayListRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void InitViews() {

        rv_livevideo = findViewById(R.id.rv_livevideo);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rv_livevideo.setLayoutManager(mLayoutManager);
//        rv_packagelist.addItemDecoration(new DividerItemDecoration(mContext,
//                DividerItemDecoration.VERTICAL));

        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.rv_divider));
        rv_livevideo.addItemDecoration(itemDecorator);

        layout_back = findViewById(R.id.layout_back);
        txt_topbar_title = findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText(getResources().getString(R.string.txt_label_playlist));
        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));
        txt_empty_livevideo = findViewById(R.id.txt_empty_livevideo);
    }

    private void SetClickListners(){

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rv_livevideo.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_livevideo, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                BaseActivity.PLAYLIST_ID = mYTAllPlayListData.get(position).getPlayListId();
                startActivity(new Intent(mContext, YTVideoListActivity.class));

//                Intent videoIntent = new Intent(mContext, VideoPlayerActivity.class);
//                startActivity(videoIntent);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }


    private void GetYTPlayListRequest() {

        showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.GOOGLE_API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        String key = "AIzaSyCZZ5Vu9X-ahW7k8Decyl3d9j00HeJr5zs";
        String channelId = "UCqUDtr7LxA_g2loE0GvMD4A";
        String part = "snippet,contentDetails";
        String maxResults = "50";

//         part=snippet,contentDetails&channelId=UCqUDtr7LxA_g2loE0GvMD4A&key=AIzaSyCZZ5Vu9X-ahW7k8Decyl3d9j00HeJr5zs&maxResults=50

        Call<YTPlaylist> call = service.GetYTPlayListRequest(key, channelId, part, maxResults);

        call.enqueue(new Callback<YTPlaylist>() {
            @Override
            public void onResponse(Call<YTPlaylist> call, Response<YTPlaylist> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e("YT_VIDEO", "FULL RESPONSE : "+ new Gson().toJson(response.body().getRegionCode()));
                        Log.e("YT_VIDEO", "FULL RESPONSE : "+ new Gson().toJson(response.body().getItems().get(0).getContentDetails().getItemCount()));

                        Log.e("YT_VIDEO", "FULL RESPONSE : "+ new Gson().toJson(response.body().getItems()));

                        for(int i=0; i <response.body().getItems().size(); i++){

                            YTAllPlayList mYTAllPlayList = new YTAllPlayList();

                            mYTAllPlayList.setPlayListId(response.body().getItems().get(i).getId());
                            mYTAllPlayList.setPlayListName(response.body().getItems().get(i).getSnippet().getTitle());
                            mYTAllPlayList.setPlayListThumbnail(response.body().getItems().get(i).getSnippet().getThumbnails().getHigh().getUrl());
                            mYTAllPlayList.setPlayListCount(response.body().getItems().get(i).getContentDetails().getItemCount());

                            mYTAllPlayListData.add(mYTAllPlayList);

                        }

                        setAdapter();
                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<YTPlaylist> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void setAdapter(){

        YTPlayListAdapter mYTPlayListAdapter = new YTPlayListAdapter(mContext, mYTAllPlayListData);
        rv_livevideo.setAdapter(mYTPlayListAdapter);

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
