package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.Sample;
import com.lakshyacareer.digitalcoaching.models.SampleData;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.AppSignatureHelper;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



public class SampleIntroActivity extends BaseActivity {

    Context mContext;
    private boolean isInternet;

    private LinearLayout dotsLayout;
    private TextView[] dots;
    private Button btnSkip, btnNext;
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;

    private int no_of_sample_tutorial = 3;


    ArrayList<SampleData> mSampleList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.activity_sampleintro);

        mContext = this;
        isInternet = new InternetStatus().isInternetOn(mContext);

        inItView();
        setTypeFace();
        setClickListener();

        AppSignatureHelper signatureHelper = new AppSignatureHelper(mContext);
        ArrayList<String> appSignatures = signatureHelper.getAppSignatures();
        Log.e("SampleIntroActivity",""+appSignatures.get(0));

        if (isInternet) {
            getSampleTutorialsRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }


    }

    private void inItView() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        btnSkip = (Button) findViewById(R.id.btn_skip);
        btnNext = (Button) findViewById(R.id.btn_next);
    }

    private void setTypeFace()
    {
        btnSkip.setTypeface(getGujaratiFonts(mContext));
        btnNext.setTypeface(getGujaratiFonts(mContext));

        // adding bottom dots
        addBottomDots(0);
    }


    private void setClickListener()
    {
        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(mContext, LoginActivity.class));
                finish();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // checking for last page
                // if last page home screen will be launched
                int current = getItem(+1);
                if (current < no_of_sample_tutorial) {
                    // move to next screen
                    viewPager.setCurrentItem(current);
                } else {

                    startActivity(new Intent(mContext, LoginActivity.class));
                    finish();
                }
            }
        });
    }

    private void setData()
    {
        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
    }

    //	viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == no_of_sample_tutorial - 1) {
                // last page. make button text to GOT IT
                btnNext.setText(getString(R.string.start));
                btnSkip.setVisibility(View.GONE);
            } else {
                // still pages are left
                btnNext.setText(getString(R.string.next));
                btnSkip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void addBottomDots(int currentPage) {
        dots = new TextView[no_of_sample_tutorial];

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(getResources().getColor(R.color.dot_dark));
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(getResources().getColor(R.color.dot_light));
    }


    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }


    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(R.layout.sample_intro_slide, container, false);

            //Update Views API DATA Here

            TextView txt_sampletopic_name = (TextView)view.findViewById(R.id.txt_sampletopic_name);
            TextView txt_sample_tag = (TextView)view.findViewById(R.id.txt_sample_tag);

            ImageView img_slider = (ImageView)view.findViewById(R.id.img_slider);
            ImageView img_play = (ImageView)view.findViewById(R.id.img_play);


            txt_sampletopic_name.setTypeface(getGujaratiFonts(mContext));
            txt_sample_tag.setTypeface(getGujaratiFonts(mContext));

            txt_sampletopic_name.setTextColor(getResources().getColor(R.color.colorAccent));
            txt_sample_tag.setTextColor(getResources().getColor(R.color.colorPrimaryDark));


            final SampleData mSample= mSampleList.get(position);

            txt_sampletopic_name.setText(mSample.getTitle());
            txt_sample_tag.setText(mSample.getDescription());

            Glide.with(SampleIntroActivity.this).load(mSample.getImageUrl())
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(img_slider);


            img_play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent videointent = new Intent(mContext, VideoPlayerActivity.class);
                    videointent.putExtra("video_path", mSample.getUrl());
                    startActivity(videointent);
                }
            });


            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return no_of_sample_tutorial;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    private void getSampleTutorialsRequest() {

        showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<Sample> call = service.getSampleTutorialsRequest();

        call.enqueue(new Callback<Sample>() {
            @Override
            public void onResponse(Call<Sample> call, Response<Sample> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        if (response.body().getStatus() == 1) {

                            mSampleList = response.body().getSampleListData();
                            setData();
                        }
                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Sample> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}
