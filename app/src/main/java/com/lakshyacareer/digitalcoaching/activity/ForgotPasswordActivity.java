package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.ForgotPassword;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForgotPasswordActivity extends BaseActivity {

    private static final String TAG = "FORGOT_PASSWORD" ;
    Context mContext;
    LinearLayout ll_parent_forgot_password, layout_back;
    TextView txt_topbar_title;
    Button btn_send;
    EditText edt_phone_forgot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        mContext = this;

        initView();
        setClickListener();
    }

    private void initView() {

        ll_parent_forgot_password = findViewById(R.id.ll_parent_forgot_password);
        txt_topbar_title = findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText(getResources().getString(R.string.txt_title_forgot_password));

        edt_phone_forgot = findViewById(R.id.edt_phone_forgot);
        layout_back = findViewById(R.id.layout_back);

        btn_send = findViewById(R.id.btn_send);

        overrideFonts(ll_parent_forgot_password, mContext);
    }

    private void setClickListener() {

        layout_back.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                View view = getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                return false;
            }
        });

        btn_send.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                View view = getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                return false;
            }
        });

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edt_phone_forgot.getText().toString().length() == 0)
                {
                    edt_phone_forgot.setError("Please enter mobile number");

                } else if(edt_phone_forgot.getText().toString().length() < 10){
                    edt_phone_forgot.setError("Please enter valid mobile number");
                }
                else
                {
                    if (BaseActivity.CheckInternet(mContext)) {
                        sendForgotPasswordRequest();
                    } else {
                        Toast.makeText(mContext, getResources().getString(R.string.msg_no_internet), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void sendForgotPasswordRequest()
    {
        try {
            final String phone = edt_phone_forgot.getText().toString();

            showWaitIndicator(true);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            Call<ForgotPassword> call = service.sendForgotPasswordRequest(phone);

            call.enqueue(new Callback<ForgotPassword>() {
                @Override
                public void onResponse(Call<ForgotPassword> call, Response<ForgotPassword> response) {

                    showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {
                            Log.e(TAG,"RESPONSE : "+response.body().getMessage());
                            Toast.makeText(mContext, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();


                            Intent it = new Intent(mContext,VerificationActivity.class);
                            it.putExtra("PHONE",phone);
                            it.putExtra("USER_ID",response.body().getData().getIdUser());
                            it.putExtra("TYPE","forgot");
                            startActivity(it);
                            finish();
                        }
                    } catch (Exception e) {
                        showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ForgotPassword> call, Throwable t) {
                    showWaitIndicator(false);
                    Log.d(TAG,"onFailure"+t.toString());
                    Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}