package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.fragment.MaterialsDownloadedFragment;
import com.lakshyacareer.digitalcoaching.models.Cart;
import com.lakshyacareer.digitalcoaching.utility.DatabaseHelper;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public class CartActivity extends BaseActivity
{
    CartActivity mContext;

    RecyclerView rv_cart_list;

    Button btn_check_out;
    LinearLayout layout_back;
    TextView txt_topbar_title, txt_empty_cart;

    DatabaseHelper mDbHelper, mDBAdapters;
    static int version_val = 2;

    ArrayList<Cart> mCartList = new ArrayList<>();
    private int mTotalAmount = 0;

    String mPackageId,mAmount,mDuration;

    StringBuilder sbPackageId,sbAmount,sbDuration;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        mContext = this;

        setupForDB();
        InitViews();
        setTypeFace();
        setClickListener();
        setDataFromDB();
    }

    private void InitViews()
    {
        layout_back = (LinearLayout) findViewById(R.id.layout_back);

        txt_topbar_title = (TextView) findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText("Cart");
        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));
        txt_empty_cart = (TextView) findViewById(R.id.txt_empty_cart);
        txt_empty_cart.setTypeface(getGujaratiFonts(mContext));
        rv_cart_list = findViewById(R.id.rv_cart_list);

        btn_check_out = findViewById(R.id.btn_check_out);
    }

    private void setTypeFace() {
        btn_check_out.setTypeface(getGujaratiFonts(mContext));
    }

    private void setClickListener() {

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mIntent = new Intent(mContext, SubscribeActivity.class);
                startActivity(mIntent);
                finish();

            }
        });

        btn_check_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCartList.size() == 0){
                    Toast.makeText(mContext,"Please select at least one package",Toast.LENGTH_LONG).show();
                }else{
                    CalculateTotalPrice();
                    Intent mIntent = new Intent(mContext, PaymentOptionActivity.class);
                    mIntent.putExtra("TOTAL_AMOUNT", mTotalAmount);
                    mIntent.putExtra("PACKAGE_ID", mPackageId);
                    mIntent.putExtra("AMOUNT", mAmount);

                    mIntent.putExtra("DURATION", mDuration);
                    startActivity(mIntent);
                }
            }
        });
    }

    private void setupForDB()
    {
        try {
            mDbHelper = new DatabaseHelper(mContext, "LakshyCareer.sqlite", null,version_val);
            mDBAdapters = DatabaseHelper.getDBAdapterInstance(mContext);
        } catch (IOException e) {
            e.printStackTrace();
        }


        try
        {
            mDBAdapters.createDataBase();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void setDataFromDB()
    {
        try
        {
            sbPackageId = new StringBuilder();
            sbAmount = new StringBuilder();
            sbDuration = new StringBuilder();

            mDBAdapters.openDataBase();
            mCartList = mDBAdapters.SelectPackage();
            mDBAdapters.close();

            for(Cart mCart : mCartList){
                if(sbPackageId.length() > 0){
                    sbPackageId.append(',');
                }
                sbPackageId.append(mCart.getId());

                if(sbAmount.length() > 0){
                    sbAmount.append(',');
                }
                String[] tempPrice = mCart.getPrice().split("₹");
                sbAmount.append(tempPrice[1]);

                if(sbDuration.length() > 0){
                    sbDuration.append(',');
                }
                String[] tempDuration = mCart.getDuration().split(" ");
                sbDuration.append(tempDuration[0]);
            }

            mPackageId = sbPackageId.toString();
            mAmount = sbAmount.toString();
            mDuration = sbDuration.toString();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        if(mCartList.size() > 0)
        {
            CalculateTotalPrice();
            SetAdapter();

            txt_empty_cart.setVisibility(View.GONE);
            rv_cart_list.setVisibility(View.VISIBLE);

            SetAdapter();
        }
        else
        {
            txt_empty_cart.setVisibility(View.VISIBLE);
            rv_cart_list.setVisibility(View.GONE);
            txt_empty_cart.setText("You haven't select any packages.");
        }

    }

    private void SetAdapter() {

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rv_cart_list.setLayoutManager(mLayoutManager);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.rv_divider));
        rv_cart_list.addItemDecoration(itemDecorator);

        CartAdapter mCartAdapter = new CartAdapter(mContext, mCartList);
        rv_cart_list.setAdapter(mCartAdapter);
    }

    private void CalculateTotalPrice(){
        mTotalAmount = 0;
        try {
            for(Cart value : mCartList)
            {
                String[] splited = value.getPrice().trim().split("\\s+");
                mTotalAmount = mTotalAmount + Integer.parseInt(splited[1]);
            }

            Log.e("TOTAL_PRICE", "mTotalAmount : "+ mTotalAmount);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder>
    {
        ArrayList<Cart> mCartList = new ArrayList<>();
        Context mContext;
        public CartAdapter(Context mContext, ArrayList<Cart> mList)
        {
            this.mContext = mContext;
            this.mCartList = mList;

        }

        @Override
        public CartAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_cart, null);

            return new CartAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final CartAdapter.MyViewHolder holder, final int position) {
            final Cart mCart = mCartList.get(position);

            LinearLayout.LayoutParams params = new
                    LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            holder.ll_parent_cart.setLayoutParams(params);


            holder.txt_row_package_title.setText(mCart.getPackageName());
            holder.txt_row_package_duration.setText(mCart.getDuration());
            holder.txt_row_package_price.setText(mCart.getPrice());

            holder.ll_delete_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        mDBAdapters.openDataBase();
                        mDBAdapters.DeletePackage(mCart.getId());
                        mDBAdapters.close();
                        Toast.makeText(mContext,"Item removed successfully ", Toast.LENGTH_SHORT).show();
                        notifyDataSetChanged();
                        setDataFromDB();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mCartList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder
        {
            LinearLayout ll_parent_cart, ll_delete_cart;
            TextView txt_row_package_title, txt_row_package_duration, txt_row_package_price;

            public MyViewHolder(View itemView)
            {
                super(itemView);

                ll_parent_cart = itemView.findViewById(R.id.ll_parent_cart);
                ll_delete_cart = itemView.findViewById(R.id.ll_delete_cart);

                txt_row_package_title = itemView.findViewById(R.id.txt_row_package_title);
                txt_row_package_duration = itemView.findViewById(R.id.txt_row_package_duration);
                txt_row_package_price = itemView.findViewById(R.id.txt_row_package_price);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent mIntent = new Intent(mContext, SubscribeActivity.class);
        startActivity(mIntent);
        finish();
    }
}
