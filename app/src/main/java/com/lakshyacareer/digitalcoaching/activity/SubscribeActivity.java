package com.lakshyacareer.digitalcoaching.activity;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.adapters.PackagesAdapter;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.Packages;
import com.lakshyacareer.digitalcoaching.models.PackagesData;
import com.lakshyacareer.digitalcoaching.models.PackagesInfo;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SubscribeActivity extends BaseActivity {

    SubscribeActivity mContext;
    RecyclerView rv_packagelist;
    TextView txt_empty_packagelist;
    Button btn_next;
    TextView txt_topbar_title;
    LinearLayout layout_back;

    ArrayList<PackagesData> mPackagesDataList = new ArrayList<>();
    ArrayList<PackagesInfo> mPackagesInfoList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe);

        mContext = this;

        InitViews();
        setTypeFace();
        setClickListener();

        if (BaseActivity.CheckInternet(mContext)) {
            sendPackagesListRequest();
        } else {
            Toast.makeText(mContext, "No Internet Connection Found.", Toast.LENGTH_SHORT).show();
        }
    }

    private void InitViews()
    {
        layout_back = (LinearLayout) findViewById(R.id.layout_back);

        btn_next = findViewById(R.id.btn_next);

        rv_packagelist = findViewById(R.id.rv_packagelist);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rv_packagelist.setLayoutManager(mLayoutManager);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.rv_divider));
        rv_packagelist.addItemDecoration(itemDecorator);

        txt_empty_packagelist = findViewById(R.id.txt_empty_packagelist);
    }

    private void setTypeFace()
    {
        txt_topbar_title = findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));

        txt_topbar_title.setText("Packages");
    }

    private void setClickListener()
    {
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (BaseActivity.isFromSubject){
                    case 0:
                        startActivity(new Intent(mContext,LoginActivity.class));
                        finish();
                        break;
                    case 1:
                        startActivity(new Intent(mContext,SubjectsListActivity.class));
                        finish();
                        break;
                    case 2:
                        startActivity(new Intent(mContext,DashBoardActivity.class));
                        finish();
                        break;
                }
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext,CartActivity.class));
            }
        });
    }

    private void sendPackagesListRequest() {

        showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<Packages> call = service.sendPackagesListRequest();

        call.enqueue(new Callback<Packages>() {
            @Override
            public void onResponse(Call<Packages> call, Response<Packages> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e("PACKAGES_LIST", "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));

                        if(response.body().getStatus() == 1){

                            rv_packagelist.setVisibility(View.VISIBLE);
                            txt_empty_packagelist.setVisibility(View.GONE);

                            mPackagesDataList = response.body().getData();

                            for(PackagesData mPackagesData : mPackagesDataList){

                                PackagesInfo mPackagesInfoData = new PackagesInfo();

                                mPackagesInfoData.setIdPackage(mPackagesData.getIdPackage());
                                mPackagesInfoData.setPackageName(mPackagesData.getPackageName());
                                mPackagesInfoData.setDescription(mPackagesData.getDescription());


                                ArrayList<String> sItemList = new ArrayList<String>();

                                for(int i=0; i < mPackagesData.getPackagePrice().size() ; i++){
                                    String mPrice = mContext.getResources().getString(R.string.ruppee, mPackagesData.getPackagePrice().get(i).getPrice());
                                    sItemList.add(mPackagesData.getPackagePrice().get(i).getDuration()+" Months : "+mPrice);
                                }

                                mPackagesInfoData.setPackagePriceDuration(sItemList);


                                mPackagesInfoList.add(mPackagesInfoData);
                            }

                            Log.e("PACKAGES_LIST", "Size of Array : "+mPackagesDataList.size());
                            setAdapter(mPackagesInfoList);

                        }else {
                            rv_packagelist.setVisibility(View.GONE);
                            txt_empty_packagelist.setVisibility(View.VISIBLE);
                            txt_empty_packagelist.setText("No Packages Available");
                        }
                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Packages> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void setAdapter(ArrayList<PackagesInfo> packagesinfolist){

        PackagesAdapter mPackagesAdapter = new PackagesAdapter(mContext, packagesinfolist);
        rv_packagelist.setAdapter(mPackagesAdapter);
    }


    @Override
    public void onBackPressed() {

        switch (BaseActivity.isFromSubject){
            case 0:
                startActivity(new Intent(mContext,LoginActivity.class));
                finish();
                break;
            case 1:
                startActivity(new Intent(mContext,SubjectsListActivity.class));
                finish();
                break;
            case 2:
                startActivity(new Intent(mContext,DashBoardActivity.class));
                finish();
                break;
        }
    }
}
