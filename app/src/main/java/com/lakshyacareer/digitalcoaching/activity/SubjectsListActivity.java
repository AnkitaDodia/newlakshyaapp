package com.lakshyacareer.digitalcoaching.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.adapters.SubjectsListAdapter;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.Subjects;
import com.lakshyacareer.digitalcoaching.models.SubjectsData;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SubjectsListActivity extends BaseActivity {

    private String TAG = "SubjectsList";
    SubjectsListActivity mContext;
    RecyclerView rv_subjects;
    TextView txt_topbar_title, txt_empty_subjects;
    LinearLayout layout_back;
    public ArrayList<SubjectsData> mSubjectsList = new ArrayList<>();
    int isLock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjectlist);

        mContext = this;
        InitViews();
        SetClickListners();

        if (CheckInternet(mContext)) {
            sendSubjectsListRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void InitViews() {

        rv_subjects = findViewById(R.id.rv_subjects);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.rv_divider));

        rv_subjects.setLayoutManager(mLayoutManager);
        rv_subjects.addItemDecoration(itemDecorator);

        layout_back = findViewById(R.id.layout_back);
        txt_topbar_title = findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText(getResources().getString(R.string.txt_label_subjects));
        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));
        txt_empty_subjects = findViewById(R.id.txt_empty_subjects);
    }

    private void SetClickListners(){

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(mContext, MaterialActivity.class));
                finish();
            }
        });

//        rv_subjects.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_subjects, new BaseActivity.ClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//
//                Toast.makeText(mContext,""+isLock,Toast.LENGTH_LONG).show();
//                if(isLock == 0) {
//                    showSubscriptionDialog();
//                }else
//                {
//                    BaseActivity.SUBJECT_ID =  mSubjectsList.get(position).getIdSubject();
//                    startActivity(new Intent(mContext, TopicsActivity.class));
//                }
//            }
//
//            @Override
//            public void onLongClick(View view, int position) {
//
//            }
//        }));
    }

    private void sendSubjectsListRequest()
    {
        try {
            showWaitIndicator(true);

            String id_user = SettingsPreferences.getConsumer(this).getIdUser();
            Log.e("id_user",""+id_user);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestInterface.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RestInterface service = retrofit.create(RestInterface.class);

            Call<Subjects> call = service.sendSubjectsListRequest(BaseActivity.PACKAGE_ID, id_user);

            call.enqueue(new Callback<Subjects>() {
                @Override
                public void onResponse(Call<Subjects> call, Response<Subjects> response) {

                    showWaitIndicator(false);

                    try {
                        if (response.code() == 200) {
                            Log.e(TAG, "RESPONSE : "+ new Gson().toJson(response.body().getData()));

                            if(response.body().getStatus() == 1){

                                rv_subjects.setVisibility(View.VISIBLE);
                                txt_empty_subjects.setVisibility(View.GONE);

                                mSubjectsList = response.body().getData();
                                isLock = response.body().getLock();
                                setAdapter(mSubjectsList, response.body().getLock());

                            }else {
                                rv_subjects.setVisibility(View.GONE);
                                txt_empty_subjects.setVisibility(View.VISIBLE);
                                txt_empty_subjects.setText("No Subjects Available");
                            }

                        }
                    } catch (Exception e) {
                        showWaitIndicator(false);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Subjects> call, Throwable t) {
                    showWaitIndicator(false);
                    Log.d(TAG,"onFailure"+t.toString());
                    Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setAdapter(ArrayList<SubjectsData> msubjectslist, int isLock){

        SubjectsListAdapter mSubjectsListAdapter = new SubjectsListAdapter(mContext, msubjectslist, isLock);
        rv_subjects.setAdapter(mSubjectsListAdapter);

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext, MaterialActivity.class));
        finish();
    }
}
