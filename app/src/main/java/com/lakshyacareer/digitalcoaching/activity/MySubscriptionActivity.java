package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.adapters.MySubscriptionAdapter;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.MySubscription;
import com.lakshyacareer.digitalcoaching.models.MySubscriptionData;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MySubscriptionActivity extends BaseActivity {

    private String TAG = "MYSUBSCRIPTION" ;
    Context mContext;
    RecyclerView rv_my_subscription;
    TextView txt_topbar_title, txt_empty_my_subscription;
    LinearLayout layout_back;

    private ArrayList<MySubscriptionData> mMySubscriptionList= new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_subscription);

        mContext = this;
        InitViews();
        SetClickListners();

        if (CheckInternet(mContext)) {
            sendMySubscriptionRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void InitViews() {

        rv_my_subscription = findViewById(R.id.rv_my_subscription);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rv_my_subscription.setLayoutManager(mLayoutManager);
//        rv_packagelist.addItemDecoration(new DividerItemDecoration(mContext,
//                DividerItemDecoration.VERTICAL));

        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.rv_divider));
        rv_my_subscription.addItemDecoration(itemDecorator);

        layout_back = findViewById(R.id.layout_back);
        txt_topbar_title = findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText(getResources().getString(R.string.txt_label_subscription));
        txt_empty_my_subscription = findViewById(R.id.txt_empty_my_subscription);
    }

    private void SetClickListners(){

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void sendMySubscriptionRequest() {

        showWaitIndicator(true);

        String id_user = SettingsPreferences.getConsumer(this).getIdUser();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<MySubscription> call = service.sendMySubscriptionRequest(id_user);

        call.enqueue(new Callback<MySubscription>() {
            @Override
            public void onResponse(Call<MySubscription> call, Response<MySubscription> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e(TAG, "FULL RESPONSE : "+ new Gson().toJson(response.body().getData()));

                        if(response.body().getStatus() == 1){

                            rv_my_subscription.setVisibility(View.VISIBLE);
                            txt_empty_my_subscription.setVisibility(View.GONE);

                            mMySubscriptionList = response.body().getData();
                            setAdapter();

                        }else {
                            rv_my_subscription.setVisibility(View.GONE);
                            txt_empty_my_subscription.setVisibility(View.VISIBLE);
                            txt_empty_my_subscription.setText("You have no package subscribed yet.");
                        }
                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MySubscription> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void setAdapter(){

        MySubscriptionAdapter mMySubscriptionAdapter = new MySubscriptionAdapter(mContext, mMySubscriptionList);
        rv_my_subscription.setAdapter(mMySubscriptionAdapter);

    }
}
