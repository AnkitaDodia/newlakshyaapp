package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.adapters.FAQAdapter;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.FAQ;
import com.lakshyacareer.digitalcoaching.models.FAQData;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FAQActivity extends BaseActivity {

    FAQActivity mContext;

    RecyclerView rv_faq;

    ArrayList<FAQData> mFAQList = new ArrayList<>();

    private FAQAdapter mFAQAdapter;

    TextView txt_topbar_title, txt_empty_faq;

    LinearLayout layout_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        mContext = this;

        initView();
        setTypeFace();
        setClickListener();

//        if(BaseActivity.mFAQitems.size() > 0)
//        {
//            mFAQList = mFAQitems;
//            setAdapter(mFAQList);
//        }
//        else
//        {
            if (BaseActivity.CheckInternet(mContext)) {
                sendFAQRequest();
            } else {
                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
//        }



    }

    private void initView() {
        layout_back = findViewById(R.id.layout_back);

        rv_faq = findViewById(R.id.rv_faq);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rv_faq.setLayoutManager(mLayoutManager);

        txt_empty_faq = findViewById(R.id.txt_empty_faq);
        txt_topbar_title = findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText("FAQs");
    }

    private void setTypeFace() {
        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));
    }

    private void setClickListener() {
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void sendFAQRequest() {

        showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<FAQ> call = service.sendFAQRequest();

        call.enqueue(new Callback<FAQ>() {
            @Override
            public void onResponse(Call<FAQ> call, Response<FAQ> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e("FAQ_LIST", "FULL RESPONSE : "+ new Gson().toJson(response.body().getFaqData()));
                        Log.e("FAQ_LIST", "STATUS RESPONSE : "+response.body().getStatus());

                        if(response.body().getStatus() == 1){

                            rv_faq.setVisibility(View.VISIBLE);
                            txt_empty_faq.setVisibility(View.GONE);

                            Log.e("FAQ_LIST", "STATUS RESPONSE : Inside ");

                            mFAQList = response.body().getFaqData();
                            setAdapter(mFAQList);

                        }else {
                            rv_faq.setVisibility(View.GONE);
                            txt_empty_faq.setVisibility(View.VISIBLE);
                            txt_empty_faq.setText("No FAQ Found");
                        }




                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<FAQ> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext," "+mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void setAdapter(ArrayList<FAQData> mFAQDataList)
    {
        Log.e("FAQ_LIST",""+mFAQDataList.size());
        mFAQAdapter = new FAQAdapter(mContext, mFAQDataList);
        rv_faq.setAdapter(mFAQAdapter);
    }


    @Override
    public void onBackPressed() {
        finish();
    }
}