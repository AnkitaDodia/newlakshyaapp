package com.lakshyacareer.digitalcoaching.activity;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;

import java.io.InputStream;

public class StaticContentActivity extends BaseActivity {

    Context ctx;

    TextView txt_topbar_title, txt_content;
    LinearLayout layout_back;

    WebView webview_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_content);

        ctx = this;

        initViews();
        setClickListener();

        SetContentData(BaseActivity.FROM_LEGAL);

    }

    private void initViews(){

        layout_back = (LinearLayout) findViewById(R.id.layout_back);
        txt_topbar_title = (TextView)findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText("PrivacyPolicy");
        txt_content = (TextView)findViewById(R.id.txt_content);

        webview_content = (WebView) findViewById(R.id.webview_content);
    }

    private void setClickListener(){

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }

    private void SetContentData(int i) {

        switch(i) {
            case 0 :
                txt_topbar_title.setText("PrivacyPolicy");
                PrivacyPolicyData();
                break;
            case 1 :
                txt_topbar_title.setText("Return Policy");
                ReturnPolicyData();
                break;
            case 2 :
                txt_topbar_title.setText("Terms Conditions");
                TermsConditionData();
                break;
            case 3 :
                txt_topbar_title.setText("IntellectualProperty");
                IntelectualPropertyData();
                break;
            case 4 :
                txt_topbar_title.setText("Disclaimer");
                DisclaimerData();
                break;
            case 5 :
                txt_topbar_title.setText("Process Flow");
                ProcessFlowData();
                break;
        }
    }

    private void ProcessFlowData() {

        try {
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.process_flow);
            byte[] b = new byte[in_s.available()];
            in_s.read(b);

            String text = "<html><body style=\"color:#484848;font-family:file:///android_asset//ProximaNovaSemibold.ttf;font-size:18px;\"'background-color:transparent' >" + "<p align=\"justify\">" + new String(b) + "</p>" + "</body></html>";

//            String all="<html><body>"+text.replace("\r\n", "<br/>")+"</body></html>";

            webview_content.loadData("<font>" + text + "</font>",
                    "text/html; charset=UTF-8", null);


        } catch (Exception e) {

//            Toast.makeText(ctx, ""+e, Toast.LENGTH_LONG).show();
        }
    }

    private void DisclaimerData() {

        try {
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.disclaimer);
            byte[] b = new byte[in_s.available()];
            in_s.read(b);

//            Toast.makeText(ctx, ""+new String(b), Toast.LENGTH_LONG).show();
            String text = "<html><body style=\"color:#484848;font-family:file:///android_asset//ProximaNovaSemibold.ttf;font-size:18px;\"'background-color:transparent' >" + "<p align=\"justify\">" + new String(b) + "</p>" + "</body></html>";

//            String all="<html><body>"+text.replace("\r\n", "<br/>")+"</body></html>";

            webview_content.loadData("<font>" + text + "</font>",
                    "text/html; charset=UTF-8", null);

        } catch (Exception e) {

            Toast.makeText(ctx, ""+e, Toast.LENGTH_LONG).show();

        }
    }

    private void IntelectualPropertyData() {

        try {
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.intellectual_property);
            byte[] b = new byte[in_s.available()];
            in_s.read(b);

            String text = "<html><body style=\"color:#484848;font-family:file:///android_asset//ProximaNovaSemibold.ttf;font-size:18px;\"'background-color:transparent' >" + "<p align=\"justify\">" + new String(b) + "</p>" + "</body></html>";

//            String all="<html><body>"+text.replace("\r\n", "<br/>")+"</body></html>";

            webview_content.loadData("<font>" + text + "</font>",
                    "text/html; charset=UTF-8", null);


        } catch (Exception e) {

//            Toast.makeText(ctx, ""+e, Toast.LENGTH_LONG).show();

        }
    }

    private void TermsConditionData() {

        try {
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.termscondition);
            byte[] b = new byte[in_s.available()];
            in_s.read(b);

            String text = "<html><body style=\"color:#484848;font-family:file:///android_asset//ProximaNovaSemibold.ttf;font-size:18px;\"'background-color:transparent' >" + "<p align=\"justify\">" + new String(b) + "</p>" + "</body></html>";

//            String all="<html><body>"+text.replace("\r\n", "<br/>")+"</body></html>";

            webview_content.loadData("<font>" + text + "</font>",
                    "text/html; charset=UTF-8", null);


        } catch (Exception e) {

//            Toast.makeText(ctx, ""+e, Toast.LENGTH_LONG).show();
        }

    }

    private void ReturnPolicyData() {

        try {
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.return_policy);
            byte[] b = new byte[in_s.available()];
            in_s.read(b);

            String text = "<html><body style=\"color:#484848;font-family:file:///android_asset//ProximaNovaSemibold.ttf;font-size:18px;\"'background-color:transparent' >" + "<p align=\"justify\">" + new String(b) + "</p>" + "</body></html>";

//            String all="<html><body>"+text.replace("\r\n", "<br/>")+"</body></html>";

            webview_content.loadData("<font>" + text + "</font>",
                    "text/html; charset=UTF-8", null);


        } catch (Exception e) {

//            Toast.makeText(ctx, ""+e, Toast.LENGTH_LONG).show();

        }

    }

    private void PrivacyPolicyData() {

        try {
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.privacy_policy);
            byte[] b = new byte[in_s.available()];
            in_s.read(b);

            String text = "<html><body style=\"color:#484848;font-family:file:///android_asset//ProximaNovaSemibold.ttf;font-size:18px;\"'background-color:transparent' >" + "<p align=\"justify\">" + new String(b) + "</p>" + "</body></html>";

            String newtext = "<html>  <head><style type=\\\"text/css\\\">  @font-face {  font-family: MyFont;      src: url(\\\"file:///android_asset/fonts/ProximaNovaSemibold.ttf\\\")  }    body { font-family: MyFont;  font-size: 18px;  text-align: justify;   }   </style> </head><body>";
//            String all="<html><body>"+text.replace("\r\n", "<br/>")+"</body></html>";


            String sourcefortest = "<style type=\"text/css\">" +
                    "@font-face {" +
                    "font-family: LakkiReddy;" +
                    "src: url('fonts/ProximaNovaSemibold.ttf')" +
                    "}" +
                    "p {" +
                    "font-family: LakkiReddy;" +
                    "font-size: medium;" +
                    "text-align: justify;" +
                    "}"+
                    "</style>"  +
                    "<p>It realy works for custom font</p>";

            webview_content.loadData("<font>" + text + "</font>",
                    "text/html; charset=UTF-8", null);



        } catch (Exception e) {

//            Toast.makeText(ctx, ""+e, Toast.LENGTH_LONG).show();
        }
    }


}