package com.lakshyacareer.digitalcoaching.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.adapters.MaterialsAdapter;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.Packages;
import com.lakshyacareer.digitalcoaching.models.PackagesData;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MaterialActivity extends BaseActivity {

    MaterialActivity mContext;
    String TAG = "MATERIALS";

    RecyclerView rv_materials;
    TextView txt_topbar_title, txt_empty_materials;
    LinearLayout layout_back;

    ArrayList<PackagesData> mPackagesDataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material);

        mContext = this;
        InitViews();
        SetClickListners();

        if (CheckInternet(mContext)) {
            getMaterialsRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void InitViews() {

        rv_materials = findViewById(R.id.rv_materials);
        rv_materials.setHasFixedSize(true);
        rv_materials.setLayoutManager(new GridLayoutManager(mContext, 3));

        DividerItemDecoration itemDecorator = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);

        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.rv_divider));
        rv_materials.addItemDecoration(itemDecorator);


        layout_back = findViewById(R.id.layout_back);
        txt_topbar_title = findViewById(R.id.txt_topbar_title);
        txt_topbar_title.setText(getResources().getString(R.string.txt_label_materials));
        txt_topbar_title.setTypeface(getGujaratiFonts(mContext));
        txt_empty_materials = findViewById(R.id.txt_empty_materials);
    }

    private void SetClickListners() {

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext,DashBoardActivity.class));
                finish();
            }
        });

        rv_materials.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_materials, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                PACKAGE_ID =  mPackagesDataList.get(position).getIdPackage();
                startActivity(new Intent(mContext, SubjectsListActivity.class));

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void getMaterialsRequest() {

        showWaitIndicator(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestInterface.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface service = retrofit.create(RestInterface.class);

        Call<Packages> call = service.sendPackagesListRequest();

        call.enqueue(new Callback<Packages>() {
            @Override
            public void onResponse(Call<Packages> call, Response<Packages> response) {

                showWaitIndicator(false);

                try {
                    if (response.code() == 200) {

                        Log.e("PACKAGES_LIST", "FULL RESPONSE : " + new Gson().toJson(response.body().getData()));

                        if (response.body().getStatus() == 1) {

                            rv_materials.setVisibility(View.VISIBLE);
                            txt_empty_materials.setVisibility(View.GONE);

                            mPackagesDataList = response.body().getData();
                            Log.e("PACKAGES_LIST", "Size of Array : " + mPackagesDataList.size());
                            setAdapter();

                        } else {

                            rv_materials.setVisibility(View.GONE);
                            txt_empty_materials.setVisibility(View.VISIBLE);
                            txt_empty_materials.setText("No Packages Available");

                        }
                    }

                } catch (Exception e) {
//                    Log.d("onResponse", "There is an error");
                    showWaitIndicator(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Packages> call, Throwable t) {
                showWaitIndicator(false);
                Log.d("onFailure", t.toString());
                Toast.makeText(mContext, " " + mContext.getString(R.string.network_calling_error_msg), Toast.LENGTH_SHORT).show();

            }
        });
    }


    private void setAdapter() {

        MaterialsAdapter mMaterialsAdapter = new MaterialsAdapter(mContext, mPackagesDataList);
        rv_materials.setAdapter(mMaterialsAdapter);

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext,DashBoardActivity.class));
        finish();
    }

}
