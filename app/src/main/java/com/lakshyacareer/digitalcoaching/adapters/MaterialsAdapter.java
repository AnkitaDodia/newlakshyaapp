package com.lakshyacareer.digitalcoaching.adapters;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.activity.MaterialActivity;
import com.lakshyacareer.digitalcoaching.models.PackagesData;

import java.util.ArrayList;


public class MaterialsAdapter extends RecyclerView.Adapter<MaterialsAdapter.MyViewHolder>
{
    ArrayList<PackagesData> mPackagesDataList= new ArrayList<>();
    MaterialActivity mContext;


    public MaterialsAdapter(MaterialActivity mContext, ArrayList<PackagesData> mList)
    {
        this.mContext = mContext;
        this.mPackagesDataList = mList;

    }

    @Override
    public MaterialsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_materials_grid, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MaterialsAdapter.MyViewHolder holder, final int position) {
        PackagesData mPackagesData = mPackagesDataList.get(position);

        holder.txt_material_name.setText(mPackagesData.getPackageName());

        if(position %2 == 1)
        {
            holder.item_card_material.setBackgroundColor(Color.parseColor("#EC8E48"));
            //  holder.imageView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        else
        {
            holder.item_card_material.setBackgroundColor(Color.parseColor("#009440"));
            //  holder.imageView.setBackgroundColor(Color.parseColor("#FFFAF8FD"));
        }
    }

    @Override
    public int getItemCount() {
        return mPackagesDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_material_name;
        CardView item_card_material;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            txt_material_name = itemView.findViewById(R.id.txt_material_name);
            item_card_material = itemView.findViewById(R.id.item_card_material);
            mContext.overrideFonts(txt_material_name, mContext);
        }

    }
}
