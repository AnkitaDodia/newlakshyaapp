package com.lakshyacareer.digitalcoaching.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.activity.TopicsActivity;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.FavouriteData;
import com.lakshyacareer.digitalcoaching.models.TopicsData;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class WishlistAdapter extends RecyclerView.Adapter<WishlistAdapter.MyViewHolder>
{

    Context mContext;
    private final BaseActivity.ClickListener listener;
    private ArrayList<FavouriteData> mFavouriteDataList = new ArrayList<>();


    public WishlistAdapter(Context mContext, ArrayList<FavouriteData> mList, BaseActivity.ClickListener listener)
    {
        this.mContext = mContext;
        this.mFavouriteDataList = mList;
        this.listener = listener;

    }

    @Override
    public WishlistAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_topics, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final WishlistAdapter.MyViewHolder holder, final int position) {
        FavouriteData mFavouriteData = mFavouriteDataList.get(position);

        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        holder.ll_parent_row_topics.setLayoutParams(params);

        int pos = position + 1;
        holder.txt_position.setText(String.valueOf(pos));

        holder.txt_subject_name.setText(mFavouriteData.getTitle());

        holder.txt_video_name.setText(mFavouriteData.getVideo());
        holder.txt_video_duration.setText(mFavouriteData.getDuration());
        holder.txt_video_size.setText(mFavouriteData.getVideoSize());


        holder.txt_document_name.setText(mFavouriteData.getMaterial());
        holder.txt_document_size.setText(mFavouriteData.getMaterialSize());

        if(mFavouriteData.getFavourite() == 1)
        {
            holder.img_favorite_topic.setImageResource(R.drawable.ic_favourite_active);
        }
        else
        {
            holder.img_favorite_topic.setImageResource(R.drawable.ic_favourite);
        }


        /*if(mFavouriteData.getVideo() == null){

            holder.ll_video_info.setVisibility(View.GONE);
        }else {
            holder.ll_video_info.setVisibility(View.VISIBLE);
        }

        if(mFavouriteData.getMaterial() == null){

            holder.ll_document_info.setVisibility(View.GONE);
        }else {
            holder.ll_document_info.setVisibility(View.VISIBLE);
        }*/

        if(mFavouriteData.getVideoLink().isEmpty()){

            holder.ll_video_info.setVisibility(View.GONE);
        }else {
            holder.ll_video_info.setVisibility(View.VISIBLE);
        }

        if(mFavouriteData.getMaterialLink().isEmpty()){
            holder.ll_document_info.setVisibility(View.GONE);
        }else {
            holder.ll_document_info.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return mFavouriteDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        LinearLayout ll_parent_row_topics, lay_topic_video, lay_document_file, lay_topic_favorite, ll_document_info, ll_video_info;
        TextView txt_position, txt_topic_name, txt_subject_name, txt_video_name, txt_video_duration, txt_video_size, txt_document_name, txt_document_size;

        ImageView img_favorite_topic;
        private WeakReference<BaseActivity.ClickListener> listenerRef;

        public MyViewHolder(View itemView)
        {
            super(itemView);

            listenerRef = new WeakReference<>(listener);

            ll_parent_row_topics = itemView.findViewById(R.id.ll_parent_row_topics);

            txt_position = itemView.findViewById(R.id.txt_position);
            txt_subject_name = itemView.findViewById(R.id.txt_subject_name);

            txt_video_name = itemView.findViewById(R.id.txt_video_name);
            txt_video_duration = itemView.findViewById(R.id.txt_video_duration);
            txt_video_size = itemView.findViewById(R.id.txt_video_size);

            txt_document_name = itemView.findViewById(R.id.txt_document_name);
            txt_document_size = itemView.findViewById(R.id.txt_document_size);

            ll_document_info = itemView.findViewById(R.id.ll_document_info);
            ll_video_info = itemView.findViewById(R.id.ll_video_info);

            img_favorite_topic = itemView.findViewById(R.id.img_favorite_topic);

            lay_topic_video = itemView.findViewById(R.id.lay_topic_video);
            lay_document_file = itemView.findViewById(R.id.lay_document_file);
            lay_topic_favorite = itemView.findViewById(R.id.lay_topic_favorite);

            ll_parent_row_topics.setOnClickListener(this);
            lay_document_file.setOnClickListener(this);
            lay_topic_video.setOnClickListener(this);
            lay_topic_favorite.setOnClickListener(this);

//            mContext.overrideFonts(ll_parent_row_topics, mContext);
        }

        // onClick Listener for view
        @Override
        public void onClick(View v) {

            listenerRef.get().onClick(v, getAdapterPosition());
        }

    }
}
