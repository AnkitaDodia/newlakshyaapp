package com.lakshyacareer.digitalcoaching.adapters;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.activity.DashBoardActivity;
import com.lakshyacareer.digitalcoaching.activity.SubjectsListActivity;
import com.lakshyacareer.digitalcoaching.activity.SubscribeActivity;
import com.lakshyacareer.digitalcoaching.activity.TopicsActivity;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.LiveVideoData;
import com.lakshyacareer.digitalcoaching.models.SubjectsData;

import java.util.ArrayList;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class SubjectsListAdapter extends RecyclerView.Adapter<SubjectsListAdapter.MyViewHolder> {

    SubjectsListActivity mContext;
    private ArrayList<SubjectsData> mSubjectsList = new ArrayList<>();
    private int mIsLock;


    public SubjectsListAdapter(SubjectsListActivity mContext, ArrayList<SubjectsData> mList, int isLcok) {
        this.mContext = mContext;
        this.mSubjectsList = mList;
        this.mIsLock = isLcok;

    }

    @Override
    public SubjectsListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_subjectlist, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SubjectsListAdapter.MyViewHolder holder, final int position) {
        SubjectsData mSubjectsData = mSubjectsList.get(position);

        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        holder.ll_row_parent_subject.setLayoutParams(params);

        Log.e("mIsLock",""+mIsLock);
        if (mIsLock == 0) {
            holder.img_lock.setImageResource(R.drawable.ic_lock);
        } else {
            holder.img_lock.setImageResource(R.drawable.ic_unlock);
        }

        int index = position + 1;
        holder.row_subject_title.setText(index+".  "+mSubjectsData.getSubjectName());
        holder.row_subject_details.setText(mSubjectsData.getSubjectDetail());

        holder.ll_row_parent_subject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsLock == 0) {
                    showSubscriptionDialog();
                } else {
                    BaseActivity.SUBJECT_ID =  mSubjectsList.get(position).getIdSubject();
                    mContext.startActivity(new Intent(mContext, TopicsActivity.class));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSubjectsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_row_parent_subject;
        TextView row_subject_title, row_subject_details;
        ImageView img_lock;

        public MyViewHolder(View itemView) {
            super(itemView);

            ll_row_parent_subject = itemView.findViewById(R.id.ll_row_parent_subject);
            row_subject_title = itemView.findViewById(R.id.row_subject_title);
            row_subject_details = itemView.findViewById(R.id.row_subject_details);
            img_lock = itemView.findViewById(R.id.img_lock);

            mContext.overrideFonts(ll_row_parent_subject, mContext);
        }

    }

    public void showSubscriptionDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        builder.setTitle(mContext.getResources().getString(R.string.app_name))
                .setMessage("Choose your package now and start learning.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        BaseActivity.isFromSubject = 2;
                        mContext.startActivity(new Intent(mContext, SubscribeActivity.class));
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .show();
    }
}
