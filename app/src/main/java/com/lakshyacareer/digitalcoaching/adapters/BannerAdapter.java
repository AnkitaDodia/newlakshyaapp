package com.lakshyacareer.digitalcoaching.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.activity.DashBoardActivity;
import com.lakshyacareer.digitalcoaching.models.BannerData;

import java.util.ArrayList;


public class BannerAdapter extends PagerAdapter {

    private DashBoardActivity mContext;
    private LayoutInflater layoutInflater;
    public ArrayList<BannerData> mBannerData = new ArrayList<>();

    public BannerAdapter(DashBoardActivity context, ArrayList<BannerData> mList) {
        this.mContext = context;
        this.mBannerData = mList;
    }

    @Override
    public int getCount() {
        return mBannerData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = layoutInflater.inflate(R.layout.item_banner, null);


        ImageView img_banner = itemView.findViewById(R.id.img_banner);
        final ProgressBar prgs_banner =  itemView.findViewById(R.id.prgs_banner);


        Glide.with(mContext).load(mBannerData.get(position).getImage())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        prgs_banner.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                            holder.img_vendor_services.setImageDrawable(resource);
                        prgs_banner.setVisibility(View.GONE);
                        return false;
                    }
                })
                .placeholder(R.drawable.image_placeholder)
                .into(img_banner);

        ViewPager vp = (ViewPager) container;
        vp.addView(itemView, 0);
        return itemView;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }
}