package com.lakshyacareer.digitalcoaching.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.activity.LiveVideoActivity;
import com.lakshyacareer.digitalcoaching.models.YTAllPlayList;

import java.util.ArrayList;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class YTPlayListAdapter extends RecyclerView.Adapter<YTPlayListAdapter.MyViewHolder>
{
    ArrayList<YTAllPlayList> mYTAllPlayList= new ArrayList<>();
    LiveVideoActivity mContext;


    public YTPlayListAdapter(LiveVideoActivity mContext, ArrayList<YTAllPlayList> mList)
    {
        this.mContext = mContext;
        this.mYTAllPlayList = mList;

    }

    @Override
    public YTPlayListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_ytplaylist, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final YTPlayListAdapter.MyViewHolder holder, final int position) {
        YTAllPlayList mYTAllPlayListData = mYTAllPlayList.get(position);

        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        holder.ll_row_parent_playlist.setLayoutParams(params);

        holder.row_txt_playlist_name.setText(mYTAllPlayListData.getPlayListName());
        holder.row_txt_playlist_count.setText(""+mYTAllPlayListData.getPlayListCount());


        holder.row_txt_playlist_count.setTypeface(mContext.getGujaratiFonts(mContext));

        Glide.with(mContext).load(mYTAllPlayListData.getPlayListThumbnail())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        holder.row_prgs_playlist.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                            holder.img_vendor_services.setImageDrawable(resource);
                        holder.row_prgs_playlist.setVisibility(View.GONE);
                        return false;
                    }
                })
                .placeholder(R.drawable.image_placeholder)
                .into(holder.img_playlist);
    }

    @Override
    public int getItemCount() {
        return mYTAllPlayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout ll_row_parent_playlist;
        TextView row_txt_playlist_name, row_txt_playlist_count;
        ProgressBar row_prgs_playlist;
        ImageView img_playlist;

        public MyViewHolder(View itemView)
        {
            super(itemView);

            ll_row_parent_playlist = itemView.findViewById(R.id.ll_row_parent_playlist);
            row_txt_playlist_name = itemView.findViewById(R.id.row_txt_playlist_name);
            row_txt_playlist_count = itemView.findViewById(R.id.row_txt_playlist_count);

            img_playlist = itemView.findViewById(R.id.img_playlist);
            row_prgs_playlist = itemView.findViewById(R.id.row_prgs_playlist);
        }

    }
}
