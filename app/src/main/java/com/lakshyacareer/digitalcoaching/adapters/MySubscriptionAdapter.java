package com.lakshyacareer.digitalcoaching.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.models.LiveVideoData;
import com.lakshyacareer.digitalcoaching.models.MySubscriptionData;

import java.util.ArrayList;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class MySubscriptionAdapter extends RecyclerView.Adapter<MySubscriptionAdapter.MyViewHolder>
{
    private ArrayList<MySubscriptionData> mMySubscriptionList= new ArrayList<>();
    Context mContext;


    public MySubscriptionAdapter(Context mContext, ArrayList<MySubscriptionData> mList)
    {
        this.mContext = mContext;
        this.mMySubscriptionList = mList;

    }

    @Override
    public MySubscriptionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_mysubcription, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MySubscriptionAdapter.MyViewHolder holder, final int position) {
        MySubscriptionData mMySubscriptionData = mMySubscriptionList.get(position);

        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        holder.ll_row_parent_subscription.setLayoutParams(params);


        int index = position + 1;
        holder.row_subscription_title.setText(index+".  "+mMySubscriptionData.getPackageName());
    }

    @Override
    public int getItemCount() {
        return mMySubscriptionList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout ll_row_parent_subscription;
        TextView row_subscription_title, row_subscription_description;

        public MyViewHolder(View itemView)
        {
            super(itemView);

            ll_row_parent_subscription = itemView.findViewById(R.id.ll_row_parent_subscription);
            row_subscription_title = itemView.findViewById(R.id.row_subscription_title);
            row_subscription_description = itemView.findViewById(R.id.row_subscription_description);
        }

    }
}
