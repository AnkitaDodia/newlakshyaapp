package com.lakshyacareer.digitalcoaching.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.activity.CurrentAffairActivity;
import com.lakshyacareer.digitalcoaching.models.CurrentAffairsData;
import com.lakshyacareer.digitalcoaching.models.LatestNewsData;

import java.util.ArrayList;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class CurrentAffairsAdapter extends RecyclerView.Adapter<CurrentAffairsAdapter.MyViewHolder>
{
    ArrayList<CurrentAffairsData> mCurrentAffairsDataList = new ArrayList<>();
    CurrentAffairActivity mContext;


    public CurrentAffairsAdapter(CurrentAffairActivity mContext, ArrayList<CurrentAffairsData> mList)
    {
        this.mContext = mContext;
        this.mCurrentAffairsDataList = mList;

    }

    @Override
    public CurrentAffairsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_latestnews, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CurrentAffairsAdapter.MyViewHolder holder, final int position) {
        CurrentAffairsData mCurrentAffairsData = mCurrentAffairsDataList.get(position);

        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        holder.ll_row_parent_latest_news.setLayoutParams(params);

        Glide.with(mContext).load(mCurrentAffairsData.getImage())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        holder.prgs_row_latest_news.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.prgs_row_latest_news.setVisibility(View.GONE);
                        return false;
                    }
                }).into(holder.img_row_latestnews);


        holder.txt_row_latest_news_title.setText(mCurrentAffairsData.getTitle());
        holder.txt_row_latest_news_discription.setText(mCurrentAffairsData.getDescription());
    }

    @Override
    public int getItemCount() {
        return mCurrentAffairsDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout ll_row_parent_latest_news;
        ImageView img_row_latestnews;
        TextView txt_row_latest_news_title, txt_row_latest_news_discription;
        ProgressBar prgs_row_latest_news;

        public MyViewHolder(View itemView)
        {
            super(itemView);


            ll_row_parent_latest_news = itemView.findViewById(R.id.ll_row_parent_latest_news);
            img_row_latestnews = itemView.findViewById(R.id.img_row_latestnews);
            txt_row_latest_news_title = itemView.findViewById(R.id.txt_row_latest_news_title);
            txt_row_latest_news_discription = itemView.findViewById(R.id.txt_row_latest_news_discription);
            prgs_row_latest_news = itemView.findViewById(R.id.prgs_row_latest_news);

            mContext.overrideFonts(ll_row_parent_latest_news, mContext);
        }

    }
}
