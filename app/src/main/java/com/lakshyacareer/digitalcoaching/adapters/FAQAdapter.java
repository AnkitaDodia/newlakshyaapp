package com.lakshyacareer.digitalcoaching.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.activity.FAQActivity;
import com.lakshyacareer.digitalcoaching.models.FAQData;

import java.util.ArrayList;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class FAQAdapter extends RecyclerView.Adapter<FAQAdapter.MyViewHolder>
{
    ArrayList<FAQData> mFAQDataList = new ArrayList<>();

    FAQActivity mContext;

    public FAQAdapter(FAQActivity mContext, ArrayList<FAQData> mList)
    {
        this.mContext = mContext;
        this.mFAQDataList = mList;
    }

    @Override
    public FAQAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_faq_list, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FAQAdapter.MyViewHolder holder, final int position) {
        FAQData mFAQData = mFAQDataList.get(position);

        holder.txt_faq_questions.setTypeface(mContext.getGujaratiFonts(mContext));
        holder.txt_faq_answer.setTypeface(mContext.getGujaratiFonts(mContext));

        holder.txt_faq_questions.setText(mFAQData.getQuestion());
        holder.txt_faq_answer.setText(mFAQData.getAnswer());

        Log.e("FAQ_LIST", "Que :"+mFAQData.getQuestion());

    }

    @Override
    public int getItemCount() {
        return mFAQDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {

        TextView txt_faq_questions, txt_faq_answer;


        public MyViewHolder(View itemView)
        {
            super(itemView);

            txt_faq_questions = itemView.findViewById(R.id.txt_faq_questions);
            txt_faq_answer = itemView.findViewById(R.id.txt_faq_answer);
        }
    }
}
