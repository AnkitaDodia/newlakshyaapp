package com.lakshyacareer.digitalcoaching.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.activity.PracticeActivity;
import com.lakshyacareer.digitalcoaching.common.BaseActivity;
import com.lakshyacareer.digitalcoaching.models.QuestionsData;

import java.util.ArrayList;


public class QuestionsAdapter extends PagerAdapter {

    private PracticeActivity mContext;
    private LayoutInflater layoutInflater;
    public ArrayList<QuestionsData> mQuestionsDataList = new ArrayList<>();

    public QuestionsAdapter(PracticeActivity context, ArrayList<QuestionsData> mList) {
        this.mContext = context;
        this.mQuestionsDataList = mList;
    }

    @Override
    public int getCount() {
        return mQuestionsDataList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = layoutInflater.inflate(R.layout.item_practice, null);

        final QuestionsData mQuestionsData = mQuestionsDataList.get(position);


        TextView txt_row_questions = itemView.findViewById(R.id.txt_row_questions);

        final RadioGroup rg_answers = itemView.findViewById(R.id.rg_answers);

        RadioButton rb_option_one = itemView.findViewById(R.id.rb_option_one);
        RadioButton rb_option_two = itemView.findViewById(R.id.rb_option_two);
        RadioButton rb_option_three = itemView.findViewById(R.id.rb_option_three);
        RadioButton rb_option_four = itemView.findViewById(R.id.rb_option_four);
        RadioButton rb_option_five = itemView.findViewById(R.id.rb_option_five);


        txt_row_questions.setTypeface(mContext.getGujaratiFonts(mContext));

        rb_option_one.setTypeface(mContext.getGujaratiFonts(mContext));
        rb_option_two.setTypeface(mContext.getGujaratiFonts(mContext));
        rb_option_three.setTypeface(mContext.getGujaratiFonts(mContext));
        rb_option_four.setTypeface(mContext.getGujaratiFonts(mContext));
        rb_option_five.setTypeface(mContext.getGujaratiFonts(mContext));

        txt_row_questions.setText(mQuestionsData.getQuestion());


        switch (mQuestionsData.getOptions().size()) {

            case 3:

                rb_option_one.setText(mQuestionsData.getOptions().get(0));
                rb_option_two.setText(mQuestionsData.getOptions().get(1));
                rb_option_three.setText(mQuestionsData.getOptions().get(2));

                rb_option_four.setVisibility(View.GONE);
                rb_option_five.setVisibility(View.GONE);

                break;

            case 4:

                rb_option_one.setText(mQuestionsData.getOptions().get(0));
                rb_option_two.setText(mQuestionsData.getOptions().get(1));
                rb_option_three.setText(mQuestionsData.getOptions().get(2));
                rb_option_four.setText(mQuestionsData.getOptions().get(3));

                rb_option_five.setVisibility(View.GONE);

                break;

            case 5:

                rb_option_one.setText(mQuestionsData.getOptions().get(0));
                rb_option_two.setText(mQuestionsData.getOptions().get(1));
                rb_option_three.setText(mQuestionsData.getOptions().get(2));
                rb_option_four.setText(mQuestionsData.getOptions().get(3));
                rb_option_five.setText(mQuestionsData.getOptions().get(4));

                break;
        }


//        Log.e("QUESTION" , "A Right : "+BaseActivity.RIGHT_ANSWERS +"  wrong : "+BaseActivity.WRONG_ANSWERS);


        rg_answers.clearCheck();
        rg_answers.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);

//                String currentString = rb.getText().toString();
//                String[] separated = currentString.split("-");
//
//                BaseActivity.SELECTED_ANSWER = separated[0];

                int idx = rg_answers.indexOfChild(rb);
//                Log.e("ANSWER", "id " + idx);

                switch (rg_answers.indexOfChild(rb)) {

                    case 0:
                        BaseActivity.SELECTED_ANSWER = "A";
                        break;
                    case 1:
                        BaseActivity.SELECTED_ANSWER = "B";
                        break;
                    case 2:
                        BaseActivity.SELECTED_ANSWER = "C";
                        break;
                    case 3:
                        BaseActivity.SELECTED_ANSWER = "D";
                        break;
                    case 4:
                        BaseActivity.SELECTED_ANSWER = "E";
                        break;
                }

            }
        });

        ViewPager vp = (ViewPager) container;
        vp.addView(itemView, 0);
        return itemView;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }
}