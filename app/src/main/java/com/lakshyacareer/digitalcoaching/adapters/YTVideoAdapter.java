package com.lakshyacareer.digitalcoaching.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.activity.LiveVideoActivity;
import com.lakshyacareer.digitalcoaching.activity.YTVideoListActivity;
import com.lakshyacareer.digitalcoaching.models.YTAllPlayList;
import com.lakshyacareer.digitalcoaching.models.YTAllVideo;

import java.util.ArrayList;

/**
 * Created by My 7 on 14-Aug-18.
 */

public class YTVideoAdapter extends RecyclerView.Adapter<YTVideoAdapter.MyViewHolder>
{
    ArrayList<YTAllVideo> mYTAllVideoList= new ArrayList<>();
    YTVideoListActivity mContext;


    public YTVideoAdapter(YTVideoListActivity mContext, ArrayList<YTAllVideo> mList)
    {
        this.mContext = mContext;
        this.mYTAllVideoList = mList;

    }

    @Override
    public YTVideoAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_ytvideos, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final YTVideoAdapter.MyViewHolder holder, final int position) {
        YTAllVideo mYTAllVideoData = mYTAllVideoList.get(position);

        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        holder.ll_row_parent_video.setLayoutParams(params);

        holder.row_txt_video_name.setText(mYTAllVideoData.getVideoName());


        holder.row_txt_video_name.setTypeface(mContext.getGujaratiFonts(mContext));

        Glide.with(mContext).load(mYTAllVideoData.getVideoThumbnail())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        holder.row_prgs_video.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                            holder.img_vendor_services.setImageDrawable(resource);
                        holder.row_prgs_video.setVisibility(View.GONE);
                        return false;
                    }
                })
                .placeholder(R.drawable.image_placeholder)
                .into(holder.img_video);
    }

    @Override
    public int getItemCount() {
        return mYTAllVideoList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout ll_row_parent_video;
        TextView row_txt_video_name;
        ProgressBar row_prgs_video;
        ImageView img_video;

        public MyViewHolder(View itemView)
        {
            super(itemView);

            ll_row_parent_video = itemView.findViewById(R.id.ll_row_parent_video);
            row_txt_video_name = itemView.findViewById(R.id.row_txt_video_name);

            img_video = itemView.findViewById(R.id.img_video);
            row_prgs_video = itemView.findViewById(R.id.row_prgs_video);
        }

    }
}
