package com.lakshyacareer.digitalcoaching.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.activity.SubscribeActivity;
import com.lakshyacareer.digitalcoaching.models.Cart;
import com.lakshyacareer.digitalcoaching.models.PackagePrice;
import com.lakshyacareer.digitalcoaching.models.PackagesData;
import com.lakshyacareer.digitalcoaching.models.PackagesInfo;
import com.lakshyacareer.digitalcoaching.utility.DatabaseHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static java.security.AccessController.getContext;

public class PackagesAdapter extends RecyclerView.Adapter<PackagesAdapter.MyViewHolder> {

    private SubscribeActivity mContext;
    private ArrayList<PackagesInfo> mPackagesInfoList = new ArrayList<PackagesInfo>();
    ArrayList<Cart> mCartList = new ArrayList<>();

    DatabaseHelper mDbHelper, dbAdapters;
    static int version_val = 2;
    String mPackageDuration,mPackagePrice;

    public PackagesAdapter(SubscribeActivity context, ArrayList<PackagesInfo> mpackagesinfolist) {
        mContext = context;
        this.mPackagesInfoList = mpackagesinfolist;
        setupForDB();
    }

    private void setupForDB()
    {
        try {
            mDbHelper = new DatabaseHelper(mContext, "LakshyCareer.sqlite", null,version_val);
            dbAdapters = DatabaseHelper.getDBAdapterInstance(mContext);
            dbAdapters.createDataBase();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        try
        {
            dbAdapters.openDataBase();
            mCartList = dbAdapters.SelectPackage();
            dbAdapters.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView row_package_title, row_package_discription;
        private Spinner sp_packages_options;
        private LinearLayout ll_add_to_cart;
        private AppCompatButton btn_add_to_cart;

        public MyViewHolder(View view) {
            super(view);
            row_package_title = view.findViewById(R.id.row_package_title);
            row_package_discription = view.findViewById(R.id.row_package_discription);

            ll_add_to_cart =  view.findViewById(R.id.ll_add_to_cart);

            sp_packages_options =  view.findViewById(R.id.sp_packages_options);
            btn_add_to_cart =  view.findViewById(R.id.btn_add_to_cart);
        }
    }


    @Override
    public PackagesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        System.out.println("PACKAGES_LIST: " + parent);
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_packageslist, parent, false);

        return new PackagesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PackagesAdapter.MyViewHolder holder, final int position) {

        final PackagesInfo mPackagesInfo = mPackagesInfoList.get(position);
        holder.row_package_title.setText(mPackagesInfo.getPackageName());
        holder.row_package_discription.setText(mPackagesInfo.getDescription());

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, mPackagesInfo.getPackagePriceDuration());
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.sp_packages_options.setAdapter(spinnerArrayAdapter);

        Log.e("Spinner_item",""+mPackagesInfo.getPackagePriceDuration());
        try {

            dbAdapters.openDataBase();
            String mId = dbAdapters.ValidateID(mPackagesInfoList.get(position).getIdPackage());
            String spinnerPosition = dbAdapters.getSpinnerPosition(mPackagesInfoList.get(position).getIdPackage());
            Log.e("spinnerPosition", "From_db : "+spinnerPosition);
            dbAdapters.close();

            if(!spinnerPosition.equalsIgnoreCase("-1")){
                holder.sp_packages_options.setSelection(Integer.parseInt(spinnerPosition));
            }

            if(mId.equalsIgnoreCase(mPackagesInfoList.get(position).getIdPackage()) ){
                Log.e("MaterialDatabade", "Inside If : "+mId);
                //If already in database then update
                holder.btn_add_to_cart.setBackgroundResource(R.drawable.ic_added_cart);

            }else {
                Log.e("MaterialDatabade", "Inside else : "+mId);
                //Intert new data
                holder.btn_add_to_cart.setBackgroundResource(R.drawable.ic_cart);

            }

        }catch (Exception e){
            e.printStackTrace();
        }

//        Log.e("PACKAGES_LIST", "Name : "+mPackagesData.getDescription());

//        Picasso.with(mContext).load(game.getFeatImgUrl()).resize(160, 200).into(holder.thumbnail);

//        Log.e("CHECK", "size : "+mCartList.size());
//        if(mCartList.contains(mPackagesData.getPackageName())){

//            for(Cart mcart : mCartList){
//                if(mcart.getPackageName().equalsIgnoreCase(mPackagesData.getPackageName())){
//                    holder.btn_add_to_cart.setBackgroundColor(mContext.getResources().getColor(R.color.red));
//                    holder.btn_add_to_cart.setClickable(false);
//
//                    Log.e("CHECK", "Contain : "+mPackagesData.getPackageName());
//                }else {
//                    holder.btn_add_to_cart.setClickable(true);
//                    holder.btn_add_to_cart.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryNew));
//
////                    Log.e("CHECK", "Contain : "+mPackagesData.getPackageName());
//                }
//
//            }


//        }else {
//            Log.e("CHECK", "Not Contain : "+mPackagesData.getPackageName());
//            holder.btn_add_to_cart.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryNew));
//        }

        holder.ll_add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    String[] temp = holder.sp_packages_options.getSelectedItem().toString().split(":");

                    mPackageDuration = temp[0];
                    mPackagePrice = temp[1];

//                    Log.e("tempPrice",""+mPackagePrice);
//                    Log.e("mPackageDuration",""+mPackageDuration);
                    Log.e("Spinner_position",""+holder.sp_packages_options.getSelectedItemPosition());

                    dbAdapters.openDataBase();

                    String mId = dbAdapters.ValidateID(mPackagesInfoList.get(position).getIdPackage());

//                    Log.e("MaterialDatabade", "Check DatabaseId : "+mId);
//                    Log.e("MaterialDatabade", "Check PackagesId : "+mPackagesInfoList.get(position).getIdPackage());
                    if(mId.equalsIgnoreCase(mPackagesInfoList.get(position).getIdPackage()) ){
                        Log.e("MaterialDatabade", "Inside If : "+mId);
                        //If already in database then update
                        dbAdapters.UpdatePackage(mId, mPackagesInfoList.get(position).getIdPackage(),
                                mPackagesInfoList.get(position).getPackageName(),
                                mPackageDuration,
                                mPackagePrice,
                                String.valueOf(holder.sp_packages_options.getSelectedItemPosition()));
                        dbAdapters.close();

                    }else {
                        Log.e("MaterialDatabade", "Inside else : "+mId);
                        //Intert new data
                        dbAdapters.InsertPackage(mPackagesInfoList.get(position).getIdPackage(),
                                mPackagesInfoList.get(position).getPackageName(),
                                mPackageDuration,
                                mPackagePrice,
                                String.valueOf(holder.sp_packages_options.getSelectedItemPosition()));
                        dbAdapters.close();

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
                notifyDataSetChanged();
                Toast.makeText(mContext, "Package is successfully added to your cart", Toast.LENGTH_SHORT).show();
            }
        });



//        holder.sp_packages_options.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
//
//                String[] temp = mPackagesInfo.getPackagePriceDuration().get(position).split(":");
//
//                mPackageDuration = temp[0];
//                mPackagePrice = temp[1];
//
//                Log.e("tempPrice",""+mPackagePrice);
//                Log.e("mPackageDuration",""+mPackageDuration);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
//        Log.e("PACKAGES_LIST", "ItemCount : "+mPackagesDataList.size());
        return mPackagesInfoList.size();
    }
}