package com.lakshyacareer.digitalcoaching.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class YTItem {

    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("etag")
    @Expose
    private String etag;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("snippet")
    @Expose
    private YTSnippet ytsnippet;
    @SerializedName("contentDetails")
    @Expose
    private YTContentDetails ytcontentDetails;


    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public String getId() {
        return id;
    }

    public void setId(String ytid) {
        this.id = ytid;
    }

    public YTSnippet getSnippet() {
        return ytsnippet;
    }

    public void setSnippet(YTSnippet ytsnippet) {
        this.ytsnippet = ytsnippet;
    }

    public YTContentDetails getContentDetails() {
        return ytcontentDetails;
    }

    public void setContentDetails(YTContentDetails contentDetails) {
        this.ytcontentDetails = contentDetails;
    }
}
