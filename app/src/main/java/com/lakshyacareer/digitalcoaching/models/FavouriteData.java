package com.lakshyacareer.digitalcoaching.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FavouriteData {
    @SerializedName("id_user")
    @Expose
    private String idUser;
    @SerializedName("id_material")
    @Expose
    private String idMaterial;
    @SerializedName("subject_name")
    @Expose
    private String subjectName;
    @SerializedName("favourite")
    @Expose
    private Integer favourite;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("material")
    @Expose
    private String material;
    @SerializedName("material_size")
    @Expose
    private String materialSize;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("video_size")
    @Expose
    private String videoSize;
    @SerializedName("video_link")
    @Expose
    private String videoLink;
    @SerializedName("material_link")
    @Expose
    private String materialLink;

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdMaterial() {
        return idMaterial;
    }

    public void setIdMaterial(String idMaterial) {
        this.idMaterial = idMaterial;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Integer getFavourite() {
        return favourite;
    }

    public void setFavourite(Integer favourite) {
        this.favourite = favourite;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getMaterialSize() {
        return materialSize;
    }

    public void setMaterialSize(String materialSize) {
        this.materialSize = materialSize;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getVideoSize() {
        return videoSize;
    }

    public void setVideoSize(String videoSize) {
        this.videoSize = videoSize;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public String getMaterialLink() {
        return materialLink;
    }

    public void setMaterialLink(String materialLink) {
        this.materialLink = materialLink;
    }
}
