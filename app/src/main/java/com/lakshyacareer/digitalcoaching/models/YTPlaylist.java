package com.lakshyacareer.digitalcoaching.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class YTPlaylist {

    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("etag")
    @Expose
    private String etag;
    @SerializedName("nextPageToken")
    @Expose
    private String nextPageToken;
    @SerializedName("regionCode")
    @Expose
    private String regionCode;
    @SerializedName("pageInfo")
    @Expose
    private YTPageInfo ytpageInfo;
    @SerializedName("items")
    @Expose
    private ArrayList<YTItem> ytitems = null;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public String getNextPageToken() {
        return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public YTPageInfo getPageInfo() {
        return ytpageInfo;
    }

    public void setPageInfo(YTPageInfo ytpageInfo) {
        this.ytpageInfo = ytpageInfo;
    }

    public ArrayList<YTItem> getItems() {
        return ytitems;
    }
    public void setItems(ArrayList<YTItem> ytitems) {
        this.ytitems = ytitems;
    }
}
