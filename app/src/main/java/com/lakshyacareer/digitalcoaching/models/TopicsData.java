package com.lakshyacareer.digitalcoaching.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopicsData {

    @SerializedName("id_subject")
    @Expose
    private String idSubject;
    @SerializedName("subject_name")
    @Expose
    private String subjectName;
    @SerializedName("video_name")
    @Expose
    private String videoName;
    @SerializedName("video_duration")
    @Expose
    private String videoDuration;
    @SerializedName("video_size")
    @Expose
    private String videoSize;
    @SerializedName("video_url")
    @Expose
    private String videoUrl;
    @SerializedName("id_material")
    @Expose
    private String idMaterial;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("document_name")
    @Expose
    private String documentName;
    @SerializedName("document_size")
    @Expose
    private String documentSize;
    @SerializedName("document_link")
    @Expose
    private String documentLink;
    @SerializedName("isdownload")
    @Expose
    private Integer isdownload;
    @SerializedName("isfavourite")
    @Expose
    private Integer isfavourite;

    public String getIdSubject() {
        return idSubject;
    }

    public void setIdSubject(String idSubject) {
        this.idSubject = idSubject;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(String videoDuration) {
        this.videoDuration = videoDuration;
    }

    public String getVideoSize() {
        return videoSize;
    }

    public void setVideoSize(String videoSize) {
        this.videoSize = videoSize;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getIdMaterial() {
        return idMaterial;
    }

    public void setIdMaterial(String idMaterial) {
        this.idMaterial = idMaterial;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentSize() {
        return documentSize;
    }

    public void setDocumentSize(String documentSize) {
        this.documentSize = documentSize;
    }

    public String getDocumentLink() {
        return documentLink;
    }

    public void setDocumentLink(String documentLink) {
        this.documentLink = documentLink;
    }

    public Integer getIsdownload() {
        return isdownload;
    }

    public void setIsdownload(Integer isdownload) {
        this.isdownload = isdownload;
    }

    public Integer getIsfavourite() {
        return isfavourite;
    }

    public void setIsfavourite(Integer isfavourite) {
        this.isfavourite = isfavourite;
    }

}
