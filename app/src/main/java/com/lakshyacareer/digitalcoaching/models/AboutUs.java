package com.lakshyacareer.digitalcoaching.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abc on 31-10-2017.
 */

public class AboutUs {

    @SerializedName("status")
    @Expose
    private Integer status;

    @SerializedName("aboutus_data")
    @Expose
    private String aboutus_data;


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return aboutus_data;
    }

    public void setMessage(String message) {
        this.aboutus_data = message;
    }

}
