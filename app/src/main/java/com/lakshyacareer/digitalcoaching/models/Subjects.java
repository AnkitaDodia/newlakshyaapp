package com.lakshyacareer.digitalcoaching.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Subjects {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("lock")
    @Expose
    private Integer lock;
    @SerializedName("data")
    @Expose
    private ArrayList<SubjectsData> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getLock() {
        return lock;
    }

    public void setLock(Integer lock) {
        this.lock = lock;
    }


    public ArrayList<SubjectsData> getData() {
        return data;
    }

    public void setData(ArrayList<SubjectsData> data) {
        this.data = data;
    }


}
