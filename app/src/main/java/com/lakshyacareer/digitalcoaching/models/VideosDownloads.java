package com.lakshyacareer.digitalcoaching.models;

import java.io.Serializable;

/**
 * Created by My 7 on 11/11/2017.
 */
public class VideosDownloads implements Serializable {

    String MaterialId;
    String VideoName;
    String MaterialTopicName;
    String VideoPath;
    String VideoDuraion;
    String VideoSize;
    String SubjectName;

    public String getMaterialId() {
        return MaterialId;
    }

    public void setMaterialId(String materialId) {
        MaterialId = materialId;
    }

    public String getVideoName() {
        return VideoName;
    }

    public void setVideoName(String videoName) {
        VideoName = videoName;
    }

    public String getMaterialTopicName() {
        return MaterialTopicName;
    }

    public void setMaterialTopicName(String materialTopicName) {
        MaterialTopicName = materialTopicName;
    }

    public String getVideoPath() {
        return VideoPath;
    }

    public void setVideoPath(String videoPath) {
        VideoPath = videoPath;
    }

    public String getVideoDuraion() {
        return VideoDuraion;
    }

    public void setVideoDuraion(String videoDuraion) {
        VideoDuraion = videoDuraion;
    }

    public String getVideoSize() {
        return VideoSize;
    }

    public void setVideoSize(String videoSize) {
        VideoSize = videoSize;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }



}
