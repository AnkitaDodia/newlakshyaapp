package com.lakshyacareer.digitalcoaching.models;

public class YTAllPlayList {

    private String PlayListId;
    private String PlayListName;
    private Integer PlayListCount;
    private String PlayListThumbnail;

    public String getPlayListId() {
        return PlayListId;
    }

    public void setPlayListId(String playListId) {
        PlayListId = playListId;
    }

    public String getPlayListName() {
        return PlayListName;
    }

    public void setPlayListName(String playListName) {
        PlayListName = playListName;
    }

    public Integer getPlayListCount() {
        return PlayListCount;
    }

    public void setPlayListCount(Integer playListCount) {
        PlayListCount = playListCount;
    }

    public String getPlayListThumbnail() {
        return PlayListThumbnail;
    }

    public void setPlayListThumbnail(String playListThumbnail) {
        PlayListThumbnail = playListThumbnail;
    }

}
