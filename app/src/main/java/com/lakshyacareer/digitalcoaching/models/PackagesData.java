package com.lakshyacareer.digitalcoaching.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PackagesData {

    @SerializedName("id_package")
    @Expose
    private String idPackage;
    @SerializedName("package_name")
    @Expose
    private String packageName;
    @SerializedName("subjects")
    @Expose
    private String subjects;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("activated")
    @Expose
    private String activated;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("package_price")
    @Expose
    private ArrayList<PackagePrice> packagePrice = null;

    public String getIdPackage() {
        return idPackage;
    }

    public void setIdPackage(String idPackage) {
        this.idPackage = idPackage;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getSubjects() {
        return subjects;
    }

    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getActivated() {
        return activated;
    }

    public void setActivated(String activated) {
        this.activated = activated;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public ArrayList<PackagePrice> getPackagePrice() {
        return packagePrice;
    }

    public void setPackagePrice(ArrayList<PackagePrice> packagePrice) {
        this.packagePrice = packagePrice;
    }
}
