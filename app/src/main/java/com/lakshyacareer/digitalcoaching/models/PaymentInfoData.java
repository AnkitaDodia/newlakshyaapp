package com.lakshyacareer.digitalcoaching.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abc on 25-11-2017.
 */

public class PaymentInfoData {

    @SerializedName("id_user")
    @Expose
    private String idUser;
    @SerializedName("payment_mode")
    @Expose
    private String paymentMode;
    @SerializedName("payed_amount")
    @Expose
    private String payedAmount;
    @SerializedName("payment_date")
    @Expose
    private String paymentDate;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("next_payment_date")
    @Expose
    private String nextPaymentDate;
    @SerializedName("expired")
    @Expose
    private Integer expired;

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPayedAmount() {
        return payedAmount;
    }

    public void setPayedAmount(String payedAmount) {
        this.payedAmount = payedAmount;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getNextPaymentDate() {
        return nextPaymentDate;
    }

    public void setNextPaymentDate(String nextPaymentDate) {
        this.nextPaymentDate = nextPaymentDate;
    }

    public Integer getExpired() {
        return expired;
    }

    public void setExpired(Integer expired) {
        this.expired = expired;
    }

}