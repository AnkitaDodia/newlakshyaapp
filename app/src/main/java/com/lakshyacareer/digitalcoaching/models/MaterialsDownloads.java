package com.lakshyacareer.digitalcoaching.models;

import java.io.Serializable;

/**
 * Created by My 7 on 11/11/2017.
 */
public class MaterialsDownloads implements Serializable {

    String MaterialId;
    String MaterialName;
    String MaterialTopicName;
    String MaterialPath;
    String MaterialSize;
    String SubjectName;

    public String getMaterialId() {
        return MaterialId;
    }

    public void setMaterialId(String materialId) {
        MaterialId = materialId;
    }

    public String getMaterialName() {
        return MaterialName;
    }

    public void setMaterialName(String materialName) {
        MaterialName = materialName;
    }

    public String getMaterialTopicName() {
        return MaterialTopicName;
    }

    public void setMaterialTopicName(String materialCategoryName) {
        MaterialTopicName = materialCategoryName;
    }

    public String getMaterialPath() {
        return MaterialPath;
    }

    public void setMaterialPath(String materialPath) {
        MaterialPath = materialPath;
    }

    public String getMaterialSize() {
        return MaterialSize;
    }

    public void setMaterialSize(String materialSize) {
        MaterialSize = materialSize;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }


}
