package com.lakshyacareer.digitalcoaching.models;

public class YTAllVideo {

    private String VideoId;
    private String VideoName;
    private String VideoThumbnail;

    public String getVideoId() {
        return VideoId;
    }

    public void setVideoId(String videoId) {
        VideoId = videoId;
    }

    public String getVideoName() {
        return VideoName;
    }

    public void setVideoName(String videoName) {
        VideoName = videoName;
    }

    public String getVideoThumbnail() {
        return VideoThumbnail;
    }

    public void setVideoThumbnail(String videoThumbnail) {
        VideoThumbnail = videoThumbnail;
    }
}
