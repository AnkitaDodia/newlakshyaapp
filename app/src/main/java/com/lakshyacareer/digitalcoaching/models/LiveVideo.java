package com.lakshyacareer.digitalcoaching.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LiveVideo {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private ArrayList<LiveVideoData> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ArrayList<LiveVideoData> getData() {
        return data;
    }

    public void setData(ArrayList<LiveVideoData> data) {
        this.data = data;
    }
}
