package com.lakshyacareer.digitalcoaching.models;

/**
 * Created by abc on 31-10-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FAQ {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("faq_data")
    @Expose
    private ArrayList<FAQData> faqData = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ArrayList<FAQData> getFaqData() {
        return faqData;
    }

    public void setFaqData(ArrayList<FAQData> faqData) {
        this.faqData = faqData;
    }
}
