package com.lakshyacareer.digitalcoaching.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PackagesInfo {

    private String idPackage;
    private String packageName;
    private String subjects;
    private String description;
    private ArrayList<String> packagespriceduration = null;

    public String getIdPackage() {
        return idPackage;
    }

    public void setIdPackage(String idPackage) {
        this.idPackage = idPackage;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getSubjects() {
        return subjects;
    }

    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public ArrayList<String> getPackagePriceDuration() {
        return packagespriceduration;
    }

    public void setPackagePriceDuration(ArrayList<String> packagePrice) {
        this.packagespriceduration = packagePrice;
    }
}
