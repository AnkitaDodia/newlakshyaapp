package com.lakshyacareer.digitalcoaching.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class YTThumbnails {

//    @SerializedName("default")
//    @Expose
//    private Default _default;
//    @SerializedName("medium")
//    @Expose
//    private Medium medium;
    @SerializedName("high")
    @Expose
    private YTHigh ythigh;

//    public Default getDefault() {
//        return _default;
//    }
//
//    public void setDefault(Default _default) {
//        this._default = _default;
//    }
//
//    public Medium getMedium() {
//        return medium;
//    }
//
//    public void setMedium(Medium medium) {
//        this.medium = medium;
//    }

    public YTHigh getHigh() {
        return ythigh;
    }

    public void setHigh(YTHigh ythigh) {
        this.ythigh = ythigh;
    }


}
